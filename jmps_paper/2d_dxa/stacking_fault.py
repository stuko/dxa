#!/usr/bin/env python

from sdia_2d import *

lattice_vectors = lattice_vector_set([[1,0],[0,1],[-1,0],[0,-1]])

crystal_size = 3
crystal = []
for i in range(-crystal_size,crystal_size):
    for j in range(-crystal_size,crystal_size):
        delta_x = 0.5 if j >= 0 else 0.0
        delta_y = 0.05 if j >= 0 else 0.0
        crystal.append([float(i) + delta_x, float(j) + 1e-7*i + delta_y])
        
#add_noise(crystal, 1e-3)

triang = compute_delaunay_tessellation(crystal, lattice_vectors)
#optimize_edge_vectors(triang, lattice_vectors)
burgers_vectors = compute_cell_burgers_vectors(triang, lattice_vectors)
plot_edge_vectors("output/stacking_fault_edges.pdf", triang)
plot_dislocation_cells("output/stacking_fault_cells.pdf", triang, lattice_vectors, burgers_vectors)
