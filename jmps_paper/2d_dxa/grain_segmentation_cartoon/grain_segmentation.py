#!/usr/bin/env python

from sdia_2d import *

crystal, atom_grains = create_polycrystal(4)
add_noise(crystal, 1e-1)
export_to_xyz_file(crystal, atom_grains, 'polycrystal.xyz')
tess = compute_delaunay_tessellation(crystal, atom_grains)
plot_cells('polycrystal_1.png', tess, False, False, False)
plot_cells('polycrystal_2.png', tess, False, False, True)
plot_cells('polycrystal_3.png', tess, True, False, True)
plot_cells('polycrystal_4.png', tess, True, True, True)
plot_cells('polycrystal_5.png', tess, True, True, True, True)
