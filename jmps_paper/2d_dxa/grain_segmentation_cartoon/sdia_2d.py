#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as colors
import matplotlib.transforms as mtransforms
import numpy as np
import math

_colors = ['cyan','green','blue','orange', 
	(0.4,0.3,0.4), 'magenta', (1.0,0.6,0.3), (0.0,0.6,1.0), 'red', (0.3,0.4,0.5),
	(0.6,0.2,0.3), 'cyan']

def create_polycrystal(num_grains, crystal_size_x = 13, crystal_size_y = 13, randomseed=5): 
	np.random.seed(randomseed)
	grain_centers = np.stack([np.random.random(num_grains) * crystal_size_x, np.random.random(num_grains) * crystal_size_y]).T
	print(grain_centers)
	xy = []
	atom_grains = []
	for gindex, gp in enumerate(grain_centers):
		angle = np.random.random(1) * math.pi * 2.0
		for i in range(-crystal_size_x,crystal_size_x):
			for j in range(-crystal_size_y,crystal_size_y):
				p = [float(i), float(j)]
				prot = [math.cos(angle) * p[0] - math.sin(angle) * p[1] + gp[0], math.cos(angle) * p[1] + math.sin(angle) * p[0] + gp[1]]
				if prot[0] < -2 or prot[0] > crystal_size_x+2 or prot[1] < -2 or prot[1] > crystal_size_y+2: continue
				dists = (grain_centers[:,0] - prot[0])**2 + (grain_centers[:,1] - prot[1])**2
				if np.argmin(dists) == gindex:
					xy.append(prot)	
					atom_grains.append(gindex)
	return xy, atom_grains

def add_noise(crystal, strength, randomseed=57): 
	np.random.seed(randomseed)
	for xy in crystal:
		xy[:] += (np.random.random_sample(np.shape(xy)) - 0.5) * strength

def export_to_xyz_file(atoms, grains, filename):
	f = open(filename, "w")
	f.write("%i\n" % len(atoms))
	f.write("Properties=pos:R:3:type:I:1\n")
	for a,g in zip(atoms, grains):
		f.write("%f %f 0 %i\n" % (a[0], a[1], g))
	f.close()

def compute_delaunay_tessellation(crystal, atom_grains):
	# Compute Delaunay tessellation
	xc = np.asarray(crystal)[:,0]
	yc = np.asarray(crystal)[:,1]	
	triang = tri.Triangulation(xc,yc)
	
	# Mask off unwanted triangles.
	#triang.set_mask(tri.TriAnalyzer(triang).get_flat_tri_mask(0.2))

	cell_grains = []
	for t in triang.triangles:
		vertex_grains = [atom_grains[t[0]], atom_grains[t[1]], atom_grains[t[2]]]
		cell_grains.append(np.argmax(np.bincount(vertex_grains)))
	triang.cell_grains = cell_grains
	triang.atom_grains = atom_grains
	return triang

def plot_cells(filename, triang, plot_tri, colored_elements, colored_atoms, no_lines = False):
	fig = plt.figure()
	plt.gca().set_aspect('equal')
	plt.axis('off')
	colormap = colors.ListedColormap(_colors)
	norm = colors.BoundaryNorm(range(len(_colors)), colormap.N)
	if not no_lines:
		if colored_atoms:
			plt.scatter(triang.x, triang.y, c=triang.atom_grains, zorder=2, cmap=colormap, norm=norm)
		else:
			plt.scatter(triang.x, triang.y, c='k', zorder=2)
	else:
			plt.scatter(triang.x, triang.y, c='k', alpha=0.0, zorder=2)

	if plot_tri:
		if not no_lines:
			if colored_elements:
				plt.tripcolor(triang, facecolors=np.asarray(triang.cell_grains), edgecolors='k', cmap=colormap, norm=norm, linewidth=1.0)
			else:
				plt.triplot(triang, color='k', linewidth=1.0)
		else:
			plt.tripcolor(triang, facecolors=np.asarray(triang.cell_grains), edgecolors='k', cmap=colormap, norm=norm, linewidth=0.3)
	fig.savefig(filename, format='PNG', bbox_inches='tight', dpi=200)
	#fig.savefig(filename, format='PDF', bbox_inches='tight')
	#fig.savefig(filename, format='PDF', bbox_inches=mtransforms.Bbox([[0.0, 0.0], [10, 10]]))

