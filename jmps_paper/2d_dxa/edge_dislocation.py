#!/usr/bin/env python

from sdia_2d import *

perfect_crystal = create_perfect_crystal(crystal_size_x = 4, crystal_size_y = 3)
export_to_dump_file(perfect_crystal, "output/perfect_crystal.xyz")


lattice_vectors = lattice_vector_set([[1,0],[0,1],[-1,0],[0,-1]])
plot_lattice_vector_legend('output/lattice_vectors.pdf', lattice_vectors)
crystal = create_dislocated_crystal(width = 1.0, npartials = 6, crystal_size_x = 4, crystal_size_y = 3)
export_to_dump_file(crystal, "output/edge_dislocation.xyz")
triang = compute_delaunay_tessellation(crystal, lattice_vectors)
optimize_edge_vectors(triang, lattice_vectors)
burgers_vectors = compute_cell_burgers_vectors(triang, lattice_vectors)
plot_edge_vectors("output/dislocation_edges.pdf", triang)
plot_dislocation_cells("output/dislocation_cells.pdf", triang, lattice_vectors, burgers_vectors)
