#!/usr/bin/env python

from sdia_2d import *

lattice_vectors = lattice_vector_set([[1,0],[0,1],[-1,0],[0,-1]])
#plot_lattice_vector_legend('output/lattice_vectors.pdf', lattice_vectors)
crystal = create_dislocated_crystal(b = 0.5, crystal_size_x = 5)
triang = compute_delaunay_tessellation(crystal, lattice_vectors)
optimize_edge_vectors(triang, lattice_vectors)
burgers_vectors = compute_cell_burgers_vectors(triang, lattice_vectors)
plot_edge_vectors("output/partial_dislocation_edges.pdf", triang)
plot_dislocation_cells("output/partial_dislocation_cells.pdf", triang, lattice_vectors, burgers_vectors)
