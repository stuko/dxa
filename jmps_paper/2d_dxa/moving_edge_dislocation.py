#!/usr/bin/env python

from sdia_2d import *
import copy

lattice_vectors = lattice_vector_set([[0,0],[1,0],[0,1],[-1,0],[0,-1]], 2)
crystal = create_perfect_crystal(crystal_size_x = 6, crystal_size_y = 3)

crystal1 = copy.deepcopy(crystal)
crystal2 = copy.deepcopy(crystal)
#apply_dislocation_field(crystal1, disloc = np.asarray([-2.5, -0.5]))
#apply_dislocation_field(crystal2, disloc = np.asarray([2.5, -0.5]))
apply_dislocation_field(crystal2, disloc = np.asarray([-2.5, -0.5]), b = -1.0)
apply_dislocation_field(crystal2, disloc = np.asarray([2.5, -0.5]), b = 1.0)

triang1 = compute_delaunay_tessellation(crystal1, lattice_vectors)
triang2 = compute_delaunay_tessellation(crystal2, lattice_vectors)
#optimize_edge_vectors(triang1, lattice_vectors)
#optimize_edge_vectors(triang2, lattice_vectors)
burgers_vectors1 = compute_cell_burgers_vectors(triang1, lattice_vectors)
burgers_vectors2 = compute_cell_burgers_vectors(triang2, lattice_vectors)

plot_edge_vectors("output/moving_dislocation_edges.pdf", triang1)
plot_dislocation_cells("output/moving_dislocation_cells1.pdf", triang1, lattice_vectors, burgers_vectors1)
plot_dislocation_cells("output/moving_dislocation_cells2.pdf", triang2, lattice_vectors, burgers_vectors2)

triang1.x = np.asarray(crystal2)[:,0]
triang1.y = np.asarray(crystal2)[:,1]
assign_edge_vectors(triang1, lattice_vectors)
#optimize_edge_vectors(triang1, lattice_vectors)
burgers_vectors1 = compute_cell_burgers_vectors(triang1, lattice_vectors)
plot_edge_vectors("output/moving_dislocation_edges1b.pdf", triang1)
plot_dislocation_cells("output/moving_dislocation_cells1b.pdf", triang1, lattice_vectors, burgers_vectors1)
