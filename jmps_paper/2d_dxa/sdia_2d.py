#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as colors
import numpy as np
import math

_lattice_vector_colors = ['white',(0.1, 0.1, 1.0), (0.6,0.9,0.0), (0.1, 0.7, 1.0), 'green', 
	(0.4,0.3,0.4), 'magenta', (1.0,0.6,0.3), (0.0,0.6,1.0), 'red', (0.3,0.4,0.5),
	(0.6,0.2,0.3), 'cyan']

def lattice_vector_set(primitive_vectors, niter = 1):
	primitive_vectors = np.asarray(primitive_vectors)
	lattice_vectors = np.copy(primitive_vectors)
	for i in range(niter):
		for v1 in primitive_vectors:
			for v2 in primitive_vectors:
				sum = v1 + v2
				if not np.any(np.sum((lattice_vectors - sum)**2, axis=1) < 1e-9):
					lattice_vectors = np.append(lattice_vectors, [sum], axis=0)
		primitive_vectors = np.copy(lattice_vectors)
	return lattice_vectors

def find_best_lattice_vector_index(lattice_vectors, v):
	return np.argmin(np.sum((lattice_vectors - v)**2, axis=1))

def find_exact_lattice_vector_index(lattice_vectors, v):
	return np.argmin(np.sum((lattice_vectors - v)**2, axis=1))

def plot_lattice_vector_legend(filename, lattice_vectors):
	fig = plt.figure()
	plt.gca().set_aspect('equal')
	plt.xlim(-1.3,1.3)
	plt.ylim(-1.3,1.3)
	plt.axis('off')
	lineargs = { 'color':'gray', 'linestyle':'--', 'linewidth':2, 'zorder':0}
	plt.axhline(y=-1,xmin=-1.3, xmax=1.3, **lineargs)
	plt.axhline(y=0,xmin=-1.3, xmax=1.3, **lineargs)
	plt.axhline(y=1,xmin=-1.3, xmax=1.3, **lineargs)
	plt.axvline(x=-1,ymin=-1.3, ymax=1.3, **lineargs)
	plt.axvline(x=0,ymin=-1.3, ymax=1.3, **lineargs)
	plt.axvline(x=1,ymin=-1.3, ymax=1.3, **lineargs)
	plt.scatter([-1,-1,-1,0,0,0,1,1,1], [-1,0,1,-1,0,1,-1,0,1], c='k', zorder=3, s=750)
	for lv,color in zip(lattice_vectors, _lattice_vector_colors):
		if lv[0] == 0 and lv[1] == 0: continue
		if abs(lv[0]) >= 2 or abs(lv[1]) >= 2: continue
		offset = 1.0 - 0.12 / math.sqrt(lv[0]**2 + lv[1]**2)
		plt.arrow(0.0, 0.0, lv[0]*offset, lv[1]*offset, width=0.05, head_width=0.05*3.5,
			head_length=0.05*2.0, color=color, length_includes_head=True, zorder=1)	
	fig.savefig(filename, format='PDF', bbox_inches='tight')

def dislocation_displacement_field(loc, b, nu = 0.0):
	x,y = loc
	ux = b*(math.atan2(y,x) + x*y/(2*(1-nu)*(x**2 + y**2)))/(2*math.pi);
	uy = b*((1-2*nu)/(4*(1-nu)) + x*y/(2*(1-nu)*(x**2 + y**2)))/(2*math.pi);
	return [ux,uy]

def create_dislocated_crystal(disloc = np.asarray([-0.5, -0.5]), b = 1.0, rand = 0.0, width = 1.0, npartials = 1, crystal_size_x = 3, crystal_size_y = 3): 
	xy = []
	for i in range(-crystal_size_x,crystal_size_x):
		for j in range(-crystal_size_y,crystal_size_y):
			p = [float(i), float(j)] - disloc
			u = np.zeros(2)
			if npartials > 1:
				for k in np.linspace(-width, width, npartials):
					c = np.array([k,0.0])
					u += dislocation_displacement_field(p + c, b / npartials)
			else:
				u += dislocation_displacement_field(p, b)
			p += u
			if i == -crystal_size_x and j < 0: continue
			xy.append(p)
	return xy

def apply_dislocation_field(crystal, disloc = np.asarray([-0.5, -0.5]), b = 1.0, rand = 0.0, width = 1.0, npartials = 1): 
	for i in range(len(crystal)):	
		p = crystal[i] - disloc
		u = np.zeros(2)
		if npartials > 1:
			for k in np.linspace(-width, width, npartials):
				c = np.array([k,0.0])
				u += dislocation_displacement_field(p + c, b / npartials)
		else:
			u += dislocation_displacement_field(p, b)
		crystal[i] += u

def create_perfect_crystal(crystal_size_x = 3, crystal_size_y = 3): 
	xy = []
	for i in range(-crystal_size_x,crystal_size_x):
		for j in range(-crystal_size_y,crystal_size_y):
			xy.append([float(i), float(j)])
	return xy

def add_noise(crystal, strength, randomseed=57): 
	np.random.seed(randomseed)
	for xy in crystal:
		xy += (np.random.random_sample(np.shape(xy)) - 0.5) * strength

def export_to_dump_file(atoms, filename):
	f = open(filename, "w")
	f.write("%i\n" % len(atoms))
	f.write("Properties=pos:R:3\n")
	for a in atoms:
		f.write("%f %f 0\n" % (a[0], a[1]))
	f.close()

def assign_edge_vectors(triang, lattice_vectors):
	edge_vectors = {}
	for t in triang.triangles:
		for e in range(3):
			a = t[(e+1)%3]
			b = t[e]
			v0 = np.asarray([triang.x[a], triang.y[a]])
			v1 = np.asarray([triang.x[b], triang.y[b]])
			v = v1 - v0
			edge_vectors[(a,b)] = find_best_lattice_vector_index(lattice_vectors,  v)
			edge_vectors[(b,a)] = find_best_lattice_vector_index(lattice_vectors, -v)
	
	triang.egde_vectors = edge_vectors

def compute_delaunay_tessellation(crystal, lattice_vectors):
	# Compute Delaunay tessellation
	xc = np.asarray(crystal)[:,0]
	yc = np.asarray(crystal)[:,1]	
	triang = tri.Triangulation(xc,yc)
	
	# Mask off unwanted triangles.
	triang.set_mask(tri.TriAnalyzer(triang).get_flat_tri_mask(0.2))
	
	# Assign initial edge vectors
	assign_edge_vectors(triang, lattice_vectors)

	return triang

def compute_cell_burgers_vectors(triang, lattice_vectors):
	burgers_vectors = []
	
	for t in triang.triangles:
		b = np.zeros(2)
		for e in range(3):
			v0 = t[(e+1)%3]
			v1 = t[e]
			b += lattice_vectors[triang.egde_vectors[(v0,v1)]]
		burgers_vectors.append(find_exact_lattice_vector_index(lattice_vectors, b))
		
	return burgers_vectors
	
def optimize_edge_vectors(triang, lattice_vectors):
	for a,b in triang.egde_vectors:
		if a > b: continue
		c = []
		for t in triang.triangles:
			if a in t and b in t:
				for i in range(3):
					if t[i] != a and t[i] != b:
						c.append(t[i])
		votes = { triang.egde_vectors[(a,b)] : 1 } 		
		for vo in c:
			edgev = lattice_vectors[triang.egde_vectors[(a,vo)]] + lattice_vectors[triang.egde_vectors[(vo,b)]]
			lv = find_exact_lattice_vector_index(lattice_vectors, edgev)
			if lv in votes: votes[lv] += 1
			else: votes[lv] = 1
		max_lv = -1
		max_count = 0;
		for vo in votes:
			if votes[vo] > max_count: 
				max_count = votes[vo]
				max_lv = vo
		triang.egde_vectors[(a,b)] = max_lv
		triang.egde_vectors[(b,a)] = find_exact_lattice_vector_index(lattice_vectors, -lattice_vectors[max_lv])
		
def plot_edge_vectors(filename, triang, offset=-0.025, width=0.05, head_width=3.5*0.05, head_length=2.0*0.05):
	fig = plt.figure()
	plt.gca().set_aspect('equal')
	plt.axis('off')
	plt.scatter(triang.x, triang.y, c='k', zorder=2)
	plt.triplot(triang, '-', color='k')
	
	for t,mask in zip(triang.triangles, triang.mask):
		if mask: continue
		for e in range(3):
			a = t[(e+1)%3]			
			b = t[e]
			v0 = np.asarray([triang.x[a], triang.y[a]])
			v1 = np.asarray([triang.x[b], triang.y[b]])
			v = v1 - v0
			lv = triang.egde_vectors[(a,b)]
			n = np.asarray([-v[1],v[0]]) / np.linalg.norm(v) * offset
			n += v*0.2
			color = _lattice_vector_colors[lv] if lv < len(_lattice_vector_colors) else (0.5,0.5,0.5)
			plt.arrow(v0[0] + n[0], v0[1] + n[1], v[0]*0.6, v[1]*0.6, width=width, head_width=head_width,
				head_length=head_length, color=color, 
				shape='left', length_includes_head=True)
	
	fig.savefig(filename, format='PDF', bbox_inches='tight', pad_inches=0)

def plot_dislocation_cells(filename, triang, lattice_vectors, burgers_vectors):
	fig = plt.figure()
	plt.gca().set_aspect('equal')
	plt.axis('off')
	plt.scatter(triang.x, triang.y, c='k', zorder=2)
	colormap = colors.ListedColormap(_lattice_vector_colors)
	norm = colors.BoundaryNorm(range(len(_lattice_vector_colors)), colormap.N)
	plt.tripcolor(triang, facecolors=np.asarray(burgers_vectors), edgecolors='k', cmap=colormap, norm=norm, linewidth=1.4)
	
	np.random.seed(2)
	for t,mask,bv in zip(triang.triangles, triang.mask, burgers_vectors):
		if bv == 0 or mask: continue
		xmid = triang.x[t].mean()
		ymid = triang.y[t].mean()
		b = lattice_vectors[bv] * 0.5
		ymid += np.random.random_sample() * 0.2 - 0.1
		plt.arrow(xmid - b[0]*0.5, ymid - b[1]*0.5, b[0], b[1], width=0.05, head_width=0.05*3.0,
			head_length=0.05*2.0, color=(0.8,0.2,0.2), length_includes_head=True, zorder=3)
	
	fig.savefig(filename, format='PDF', bbox_inches='tight')
