from ovito.linalg import *

v1 = Vector3(2,1,-3)
v2 = Vector3(1,1,1)
v3 = Vector3(4,-5,1)
m1 = Matrix3(v1.normalized(), v2.normalized(), v3.normalized())
mr1 = m1.inverse()

lv = [
      Vector3(0.5,0.5,0.0),
      Vector3(0.5,0.0,0.5),
      Vector3(0.0,0.5,0.5),
      Vector3(0.5,-0.5,0.0),
      Vector3(0.5,0.0,-0.5),
      Vector3(0.0,-0.5,0.5)
]

for l in lv:
    v = mr * l
    print(v.x, v.y, v.z)
    print(-v.x, v.y, v.z)
    v = mr * -l
    print(v.x, v.y, v.z)
    print(-v.x, v.y, v.z)

#a = 4.053
#print(a)
#for l in lv:
#    v = mr * l * a
#    print(v.x, v.y, v.z, v.length)
#    v = mr * -l * a
#    print(v.x, v.y, v.z, v.length)
