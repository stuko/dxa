///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (2014) Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
//
//  This file is part of the program code 'Simple Dislocation Identification Algorithm'
//  that accompanies the paper:
//
//  A. Stukowski,
//  "A triangulation-based method to identify dislocations in atomistic models"
//  Journal of the Mechanics and Physics of Solids 70 (2014), 314–319
//  DOI: 10.1016/j.jmps.2014.06.009
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
///////////////////////////////////////////////////////////////////////////////

#include "SDIA.h"

#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/zip_iterator.hpp>

/// List of the six edges of a tetrahedron.
/// Each edge is defined in terms of the indices of its two vertices.
const int SDIA::edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

/// List of Burgers circuits on a tetrahedron, each defined by a sequence of three edges.
const int SDIA::burgersCircuitEdges[4][3] = {{3,5,4},{1,5,2},{0,4,2},{0,3,1}};

/**
 * Algorithm step 0:
 *
 * Precompute some stuff such as the rotated lattice vectors prior to the actual analysis.
 */
void SDIA::prepare(double latticeParameter, double orientation[3][3])
{
	// Check if coordinate axes are orthogonal.
	if(orientation[0][0]*orientation[1][0] + orientation[0][1]*orientation[1][1] + orientation[0][2]*orientation[1][2] ||
		orientation[1][0]*orientation[2][0] + orientation[1][1]*orientation[2][1] + orientation[1][2]*orientation[2][2] ||
		orientation[0][0]*orientation[2][0] + orientation[0][1]*orientation[2][1] + orientation[0][2]*orientation[2][2])
		throw runtime_error("Lattice orient vectors are not orthogonal.");

	// Check righthandedness of orientation vectors. x cross y must be in same direction as z.
	int xy0 = orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1];
	int xy1 = orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2];
	int xy2 = orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0];
	if(xy0*orientation[2][0] + xy1*orientation[2][1] + xy2*orientation[2][2] <= 0)
		throw runtime_error("Lattice orient vectors are not right-handed.");

	// Normalize orient vectors.
	for(int i = 0; i < 3; i++) {
		double len = sqrt(orientation[i][0]*orientation[i][0] + orientation[i][1]*orientation[i][1] + orientation[i][2]*orientation[i][2]);
		for(int j = 0; j < 3; j++) orientation[i][j] /= len;
	}

	// Prepare lattice orientation matrix.
	AffineTransformation rotation(
			orientation[0][0], orientation[1][0], orientation[2][0],
			orientation[0][1], orientation[1][1], orientation[2][1],
			orientation[0][2], orientation[1][2], orientation[2][2]);
	AffineTransformation inv_rotation = rotation.inverse();

	// Put null vector at index 0 of vector table.
	latticeVectors.insert(latticeVectors.begin(), CGAL::NULL_VECTOR);

	// Compute linear combinations of existing lattice vectors to generate additional lattice vectors.
	for(int depth = 0; depth < 2; depth++) {
		for(size_t i = 0, lvcount = latticeVectors.size(); i < lvcount; i++) {
			for(size_t j = 0; j <= i; j++) {
				Vector3 v = latticeVectors[i] + latticeVectors[j];
				if(latticeVectors.find(v) == -1) latticeVectors.push_back(v);
			}
		}
	}
	cout << "Final number of lattice vectors: " << latticeVectors.size() << endl;

	// Transform lattice vectors and scale by the user-specified lattice constant.
	scaledLatticeVectors.resize(latticeVectors.size());
	transform(latticeVectors.begin(), latticeVectors.end(), scaledLatticeVectors.begin(), [inv_rotation,latticeParameter](const Vector3& v) {
		return inv_rotation(v * latticeParameter);
	});

	// Generate lookup table for reverse vectors.
	reverseLatticeVectors.resize(latticeVectors.size());
	transform(latticeVectors.begin(), latticeVectors.end(), reverseLatticeVectors.begin(), [this](const Vector3& v) {
		return latticeVectors.find(-v);
	});

	// Build lookup table for vector sums.
	for(size_t i = 0; i < latticeVectors.size(); i++) {
		for(size_t j = 0; j <= i; j++) {
			int sum = latticeVectors.find(latticeVectors[i] + latticeVectors[j]);
			if(sum != -1) latticeVectorSums.insert(make_pair(make_pair(j,i), sum));
		}
	}

	// Generate a mapping table from Burgers vectors b to dislocation types.
	// The program supports two dislocation classification schemes:

	// (1) In the first scheme, dislocations with Burgers vector +b and -b are mapped
	//     to the same type. Here, b refers to a unique lattice vector. Thus, there will be
	//     half as many dislocation types as there are lattice vectors.
	//     This classification scheme is useful if you want to analyze dislocation junctions
	//     in detail.
	// (2) In the second scheme, all dislocation with a Burgers vector of length |b| are
	//     mapped to one type. For example, all dislocation from the 1/2<110>
	//     Burgers vector family are treated as one class of dislocations.
	//     This classification scheme is useful if you just want to discriminate between
	//     different dislocation types such as full and partial dislocations.

#if 1
	// First classification scheme:
	burgersVectorTypes.resize(latticeVectors.size());
	transform(latticeVectors.begin(), latticeVectors.end(), burgersVectorTypes.begin(), [this](const Vector3& v) {
		return min(latticeVectors.find(v), latticeVectors.find(-v));
	});
#else
	// Second classification scheme:
	burgersVectorTypes.resize(latticeVectors.size());
	// First, generate a sorted list of lattice vector magnitudes.
	struct LengthCompare { bool operator()(double a, double b) const { return a < b - epsilon; } };
	map<double,int,LengthCompare> vectorLengths;
	for(const Vector3& v : latticeVectors)
		vectorLengths.insert(make_pair(v.squared_length(), 0));
	int index = 0;
	for(auto& e : vectorLengths)
		e.second = index++;
	// Second, generate mapping table.
	transform(latticeVectors.begin(), latticeVectors.end(), burgersVectorTypes.begin(), [&vectorLengths](const Vector3& v) {
		return vectorLengths[v.squared_length()];
	});
#endif

#if 0	// Enable this code section to let the program print the list of defined dislocation types.
	vector<int> sortedTypes = burgersVectorTypes;
	sort(sortedTypes.begin(), sortedTypes.end());
	sortedTypes.erase(unique(sortedTypes.begin(), sortedTypes.end()), sortedTypes.end());
	for(int t : sortedTypes)
		cout << "Dislocation type " << t << ": " << latticeVectors[t] << endl;
#endif
}

/**
 * Algorithm step 1:
 *
 * Calls the CGAL library to compute the Delaunay tessellation.
 * The atomic coordinates serve as input vertices for the tessellation.
 */
void SDIA::generateTessellation()
{
	tessellation.insert(boost::make_zip_iterator(boost::make_tuple(atoms.begin(), boost::counting_iterator<size_t>(0))),
					    boost::make_zip_iterator(boost::make_tuple(atoms.end(), boost::counting_iterator<size_t>(atoms.size()))));

	cout << "Tessellation consists of " << tessellation.number_of_finite_cells() << " cells." << endl;
}

/**
 * Algorithm step 2:
 *
 * Assigns ideal lattice vectors to edges of the tessellation.
 */
void SDIA::assignLatticeVectors()
{
	cout << "Assigning lattice vectors." << endl;

	// Maximum deviation threshold:
	double skipEdgeThreshold = scaledLatticeVectors[1].squared_length() / 2.0;
	// Threshold used to filter out elements at the surface.
	double alpha = 4.0 * scaledLatticeVectors[1].squared_length();

	size_t numEdges = 0;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {

		// Perform alpha shape test to filter out sliver elements at the solid's surface.
		if(tessellation.geom_traits().compare_squared_radius_3_object()(cell->vertex(0)->point(), cell->vertex(1)->point(), cell->vertex(2)->point(), cell->vertex(3)->point(), alpha) == CGAL::POSITIVE) {
			cell->info().markInvalid();
			continue;
		}

		for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
			DT::Vertex_handle v1 = cell->vertex(edgeVertices[edgeIndex][0]);
			DT::Vertex_handle v2 = cell->vertex(edgeVertices[edgeIndex][1]);
			Edge* edge = findEdge(v1, v2);
			if(!edge) {
				int lv = scaledLatticeVectors.closestVector(v1->point() - v2->point(), skipEdgeThreshold);
				if(lv != -1) {
					edge = edgePool.construct(lv, v2);
					Edge* oppositeEdge = edgePool.construct(reverseLatticeVectors[lv], v1);
					edge->nextEdge = v1->info().firstEdge;
					v1->info().firstEdge = edge;
					oppositeEdge->nextEdge = v2->info().firstEdge;
					v2->info().firstEdge = oppositeEdge;
					oppositeEdge->oppositeEdge = edge;
					edge->oppositeEdge = oppositeEdge;
					numEdges++;
				}
				else {
					// Mark cell as invalid.
					cell->info().markInvalid();
					break;
				}
			}
			cell->info().edges[edgeIndex] = edge;
		}
	}

	cout << "Number of tessellation edges: " << numEdges << endl;
}

/**
 * Algorithm step 2b:
 *
 * Optimizes the lattice vectors assigned to tessellation edges
 * by eliminating as many dislocated triangles as possible.
 */
void SDIA::optimizeEdges()
{
	cout << "Optimizing edges." << endl;

	bool done;
	do {
		done = true;
		map<int,int> vectorCounts;
		for(auto vertex = tessellation.vertices_begin(); vertex != tessellation.vertices_end(); ++vertex) {
			for(Edge* edge = vertex->info().firstEdge; edge != nullptr; edge = edge->nextEdge) {
				if(edge->oppositeEdge > edge || !edge->isFlagged) continue;
				edge->isFlagged = false;
				vectorCounts.insert(make_pair(edge->initialLatticeVector, 1));
				for(Edge* edge1 = vertex->info().firstEdge; edge1 != nullptr; edge1 = edge1->nextEdge) {
					for(Edge* edge2 = edge->v2->info().firstEdge; edge2 != nullptr; edge2 = edge2->nextEdge) {
						if(edge2->v2 == edge1->v2) {
							int lv = vectorSum(edge1->latticeVector, reverseLatticeVectors[edge2->latticeVector]);
							if(lv != -1) {
								if(lv != edge->latticeVector) {
									edge1->flagEdge();
									edge2->flagEdge();
								}
								vectorCounts[lv]++;
							}
							break;
						}
					}
				}
				int newLatticeVector = max_element(vectorCounts.begin(), vectorCounts.end(),
						[](const pair<int,int>& a, const pair<int,int>&b) { return a.second < b.second; })->first;
				if(edge->latticeVector != newLatticeVector) {
					done = false;
					edge->latticeVector = newLatticeVector;
					edge->oppositeEdge->latticeVector = reverseLatticeVectors[newLatticeVector];
				}
				vectorCounts.clear();
			}
		}
	}
	while(!done);
}

/**
 * Algorithm step 3:
 *
 * Performs the Burgers test on every tetrahedral cell.
 */
void SDIA::markDislocatedCells()
{
	cout << "Finding dislocated tessellation cells." << endl;

	// Mark cells that are intersected by a dislocation.
	numDislocatedCells = 0;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		cell->info().burgersVector = 0;

		// Skip invalid cells.
		if(!cell->info().isValid()) continue;

		// Gather lattice vectors assigned to the six edges of the tetrahedron.
		int edgeVectors[6];
		for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++)
			edgeVectors[edgeIndex] = cell->info().edges[edgeIndex]->latticeVector;

		// Perform Burgers test on three of the four tetrahedron facets.
		for(int face = 0; face < 3; face++) {
			Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[face][0]]] +
						latticeVectors[edgeVectors[burgersCircuitEdges[face][1]]] -
						latticeVectors[edgeVectors[burgersCircuitEdges[face][2]]];
			if(b.squared_length() > epsilon) {
				int bv = latticeVectors.find(b);
				if(bv != -1) {
					if(face & 1)
						bv = reverseLatticeVectors[bv];
					cell->info().burgersVector = burgersVectorTypes[bv];
					numDislocatedCells++;
					break;
				}
			}
		}
	}

	cout << "Found " << numDislocatedCells << " dislocated cells." << endl;

#if 1
	// Export edges of all dislocated cells to a VTK file for debugging purposes.
	writeEdgesOfDislocatedCellsToFile("edges.vtk");
	// Export facets of all dislocated cells to a VTK file for debugging purposes.
	writeFacetsOfDislocatedCellsToFile("facets.vtk");
	// Export simulation cell geometry for debugging purposes.
	writeSimulationCellFile("cell.vtk");
	// Export dislocation lines for debugging purposes.
	writeDislocationFile("dislocations.vtk");
#endif
}

