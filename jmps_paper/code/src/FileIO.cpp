///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (2014) Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
//
//  This file is part of the program code 'Simple Dislocation Identification Algorithm'
//  that accompanies the paper:
//
//  A. Stukowski,
//  "A triangulation-based method to identify dislocations in atomistic models"
//  Journal of the Mechanics and Physics of Solids 70 (2014), 314–319
//  DOI: 10.1016/j.jmps.2014.06.009
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
///////////////////////////////////////////////////////////////////////////////

#include "SDIA.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <random>

/**
 * Parses the atomic coordinates from the input LAMMPS dump file.
 */
void SDIA::parseDumpFile(const string& filename)
{
	// Read atomic coordinates from LAMMPS dump file.
	ifstream inputStream(filename.c_str());
	if(!inputStream)
		throw runtime_error("Failed to open input file");

	// Check first line.
	string line;
	getline(inputStream, line);
	if(line != "ITEM: TIMESTEP")
		throw runtime_error("Input file is not a LAMMPS dump file.");

	// Parse header.
	int numAtoms = 0;
	for(;;) {
		getline(inputStream, line);
		if(inputStream.eof()) throw runtime_error("Unexpected end of input file.");
		if(line == "ITEM: NUMBER OF ATOMS")
			inputStream >> numAtoms;
		else if(line.find("ITEM: BOX BOUNDS") == 0) {
			// Parse optional boundary condition flags.
			istringstream ss(line.substr(strlen("ITEM: BOX BOUNDS")));
			string pbcx, pbcy, pbcz;
			ss >> pbcx >> pbcy >> pbcz;
			if(pbcx.length() == 2 && pbcy.length() == 2 && pbcz.length() == 2) {
				pbc[0] = (pbcx == "pp");
				pbc[1] = (pbcy == "pp");
				pbc[2] = (pbcz == "pp");
			}

			// Parse orthogonal simulation box size.
			double simBox[2][3];
			for(size_t k = 0; k < 3; k++) {
				getline(inputStream, line);
				if(sscanf(line.c_str(), "%lg %lg", &simBox[0][k], &simBox[1][k]) != 2)
					throw runtime_error("LAMMPS dump file parsing error. Invalid box size.");
			}
			cellOrigin = Vector3(simBox[0][0], simBox[0][1], simBox[0][2]);
			cellVectors[0] = Vector3(simBox[1][0] - simBox[0][0], 0, 0);
			cellVectors[1] = Vector3(0, simBox[1][1] - simBox[0][1], 0);
			cellVectors[2] = Vector3(0, 0, simBox[1][2] - simBox[0][2]);
		}
		else if(line.compare(0, 11, "ITEM: ATOMS") == 0)
			break;
	}
	if(numAtoms <= 0)
		throw runtime_error("Invalid number of atoms.");

	// Read file column names.
	int xyz_columns[3] = {-1,-1,-1};
	istringstream iss(line.substr(12));
	int columnIndex = 0;
	for(istream_iterator<string> t(iss); t != istream_iterator<string>(); ++t, columnIndex++) {
		if(*t == "x") xyz_columns[0] = columnIndex;
		else if(*t == "y") xyz_columns[1] = columnIndex;
		else if(*t == "z") xyz_columns[2] = columnIndex;
	}
	if(xyz_columns[0] == -1 || xyz_columns[1] == -1 || xyz_columns[2] == -1)
		throw runtime_error("Dump file does not contain xyz coordinates.");

	// Allocate memory for atoms.
	atoms.resize(numAtoms);

#if 0	// Enable this if you want to apply artificial noise to the input data:
	default_random_engine rng;
	double displacementMagnitude = 0.2;
	uniform_real_distribution<double> displacementDistribution(-displacementMagnitude, +displacementMagnitude);
#endif

	// Parse coordinates.
	for(Point3& pos : atoms) {
		getline(inputStream, line);
		const char* token = line.c_str();
		const char* tokenEnd;
		double coords[3] = {0,0,0};
		for(int columnIndex = 0; ; columnIndex++, token = tokenEnd) {
			// Skip whitespace.
			while(*token == ' ' || *token == '\t') ++token;
			if(!*token) break;
			tokenEnd = token + 1;
			while(*tokenEnd != '\0' && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
			int cindex;
			if(columnIndex == xyz_columns[0]) cindex = 0;
			else if(columnIndex == xyz_columns[1]) cindex = 1;
			else if(columnIndex == xyz_columns[2]) cindex = 2;
			else continue;
			if(sscanf(token, "%lg", &coords[cindex]) != 1)
				throw runtime_error("LAMMPS dump file parsing error. Invalid atomic coordinate.");
		}
#if 0	// Enable this if you want to apply artificial noise to the atomic coordinates
		coords[0] += displacementDistribution(rng);
		coords[1] += displacementDistribution(rng);
		coords[2] += displacementDistribution(rng);
#endif
		pos = Point3(coords[0], coords[1], coords[2]);
	}

	cout << "Loaded " << numAtoms << " atoms from input file " << filename << endl;
}

/**
 * Parses the list of ideal lattice vectors from a text file.
 *
 * All lines that do not contain exactly three number are ignored.
 */
void SDIA::parseLatticeVectors(const string& filename)
{
	ifstream inputStream(filename.c_str());
	if(!inputStream)
		throw runtime_error("Failed to open lattice vector file");

	string line;
	while(inputStream) {
		getline(inputStream, line);
		double x,y,z;
		if(sscanf(line.c_str(), "%lg %lg %lg", &x, &y, &z) == 3)
			latticeVectors.push_back(Vector3(x,y,z));
	}

	cout << "Loaded " << latticeVectors.size() << " lattice vectors from file " << filename << endl;
	if(latticeVectors.empty())
		throw runtime_error("No lattice vectors have been defined.");
}

/**
 * Writes the analysis results to a file.
 *
 * Only dislocated tessellation cells are written to the file.
 */
void SDIA::writeVTKFile(const string& filename)
{
	// Gather vertices that need to be written to the output file.
	set<DT::Vertex_handle> outputVertices;
	int numCells = 0;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector > 0) {
			for(int i = 0; i < 4; i++)
				outputVertices.insert(cell->vertex(i));
			numCells++;
		}
	}

	// Write VTK output file.
	ofstream outputStream(filename);
	outputStream << "# vtk DataFile Version 3.0" << endl;
	outputStream << "# Dislocation output" << endl;
	outputStream << "ASCII" << endl;
	outputStream << "DATASET UNSTRUCTURED_GRID" << endl;
	outputStream << "POINTS " << outputVertices.size() << " double" << endl;
	int index = 0;
	for(DT::Vertex_handle v : outputVertices) {
		outputStream << v->point().x() << " " << v->point().y() << " " << v->point().z() << "\n";
		v->info().index = index++;
	}
	outputStream << endl << "CELLS " << numCells << " " << (numCells * 5) << endl;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector > 0) {
			outputStream << "4";
			for(int i = 0; i < 4; i++) {
				outputStream << " " << cell->vertex(i)->info().index;
			}
			outputStream << "\n";
		}
	}
	outputStream << endl << "CELL_TYPES " << numCells << "\n";
	for(size_t i = 0; i < numCells; i++)
		outputStream << "10" << "\n";
	outputStream << endl << "CELL_DATA " << numCells << "\n";
	outputStream << endl << "SCALARS burgers_vector_type int" << "\n";
	outputStream << "LOOKUP_TABLE default" << "\n";
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector > 0)
			outputStream << cell->info().burgersVector << "\n";
	}

	cout << "Wrote output to " << filename << endl;
}

/**
 * This function is for debugging purposes only.
 *
 * Writes all tessellation edges that belong to dislocated cells to a file.
 */
void SDIA::writeEdgesOfDislocatedCellsToFile(const string& filename)
{
	// Export all edges to a VTK file for debugging purposes.
	set<Edge*> exportEdges;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector > 0) {
			for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
				pair<DT::Vertex_handle,DT::Vertex_handle> vpair(cell->vertex(edgeVertices[edgeIndex][0]), cell->vertex(edgeVertices[edgeIndex][1]));
				if(vpair.first > vpair.second) swap(vpair.first, vpair.second);
				exportEdges.insert(findEdge(vpair.first, vpair.second));
			}
		}
	}
	writeEdgeFile(filename, exportEdges);
}

/**
 * This function is for debugging purposes only.
 *
 * Writes a set of tessellation edges to a VTK file.
 * This can be used to check the assignment of ideal vectors to edges.
 */
void SDIA::writeEdgeFile(const string& filename, const set<Edge*>& edges)
{
	// Gather vertices that need to be written to the output file.
	set<DT::Vertex_handle> outputVertices;
	for(Edge* edge : edges) {
		outputVertices.insert(edge->v2);
		outputVertices.insert(edge->oppositeEdge->v2);
	}

	// Write VTK output file.
	ofstream outputStream(filename);
	outputStream << "# vtk DataFile Version 3.0" << endl;
	outputStream << "# Edges" << endl;
	outputStream << "ASCII" << endl;
	outputStream << "DATASET UNSTRUCTURED_GRID" << endl;
	outputStream << "POINTS " << outputVertices.size() << " double" << endl;
	size_t index = 0;
	for(DT::Vertex_handle v : outputVertices) {
		outputStream << v->point().x() << " " << v->point().y() << " " << v->point().z() << endl;
		v->info().index = index++;
	}
	auto numCells = edges.size();
	outputStream << endl << "CELLS " << numCells << " " << (numCells * 3) << endl;
	for(Edge* edge : edges)
		outputStream << "2 " << edge->oppositeEdge->v2->info().index << "  " << edge->v2->info().index << endl;
	outputStream << endl << "CELL_TYPES " << numCells << endl;
	for(size_t i = 0; i < numCells; i++)
		outputStream << "3" << endl;
	outputStream << endl << "CELL_DATA " << numCells << endl;
	outputStream << endl << "VECTORS lattice_vec double" << endl;
	for(Edge* edge : edges) {
		outputStream << latticeVectors[edge->latticeVector] << endl;
	}
	outputStream << endl << "SCALARS vertex1 int" << endl;
	outputStream << "LOOKUP_TABLE default" << "\n";
	for(Edge* edge : edges) {
		outputStream << edge->oppositeEdge->v2->info().atomIndex << endl;
	}
	outputStream << endl << "SCALARS vertex2 int" << endl;
	outputStream << "LOOKUP_TABLE default" << "\n";
	for(Edge* edge : edges) {
		outputStream << edge->v2->info().atomIndex << endl;
	}
}

/**
 * This function is for debugging purposes only.
 *
 * Writes all tessellation facets that belong to dislocated cells to a file.
 */
void SDIA::writeFacetsOfDislocatedCellsToFile(const string& filename)
{
	vector<pair<array<DT::Vertex_handle,3>,int>> exportFacets;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(!cell->info().isValid()) continue;

		// Gather lattice vectors assigned to the six edges of the tetrahedron.
		int edgeVectors[6];
		for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++)
			edgeVectors[edgeIndex] = cell->info().edges[edgeIndex]->latticeVector;

		// Perform Burgers test on three of the four tetrahedron facets.
		for(int face = 0; face < 4; face++) {
			Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[face][0]]] +
						latticeVectors[edgeVectors[burgersCircuitEdges[face][1]]] -
						latticeVectors[edgeVectors[burgersCircuitEdges[face][2]]];
			if(b.squared_length() > epsilon) {
				int bv = latticeVectors.find(b);
				if(bv != -1) {
					array<DT::Vertex_handle,3> verts;
					verts[0] = cell->info().edges[burgersCircuitEdges[face][0]]->v2;
					verts[1] = cell->info().edges[burgersCircuitEdges[face][1]]->v2;
					verts[2] = cell->info().edges[burgersCircuitEdges[face][0]]->oppositeEdge->v2;
					exportFacets.push_back(make_pair(verts, bv));
				}
			}
		}
	}
	writeFacetFile(filename, exportFacets);
}

/**
 * This function is for debugging purposes only.
 *
 * Writes a set of tessellation facets to a VTK file.
 * This can be used to check which triangular facets are intersected by a dislocation.
 */
void SDIA::writeFacetFile(const string& filename, const vector<pair<array<DT::Vertex_handle,3>,int>>& facets)
{
	// Gather vertices that need to be written to the output file.
	set<DT::Vertex_handle> outputVertices;
	for(const auto& facet : facets) {
		for(int i = 0; i < 3; i++)
			outputVertices.insert(facet.first[i]);
	}

	// Write VTK output file.
	ofstream outputStream(filename);
	outputStream << "# vtk DataFile Version 3.0" << endl;
	outputStream << "# Facets" << endl;
	outputStream << "ASCII" << endl;
	outputStream << "DATASET UNSTRUCTURED_GRID" << endl;
	outputStream << "POINTS " << outputVertices.size() << " double" << endl;
	size_t index = 0;
	for(DT::Vertex_handle v : outputVertices) {
		outputStream << v->point().x() << " " << v->point().y() << " " << v->point().z() << endl;
		v->info().index = index++;
	}
	auto numCells = facets.size();
	outputStream << endl << "CELLS " << numCells << " " << (numCells * 4) << endl;
	for(const auto& facet : facets)
		outputStream << "3 " << facet.first[0]->info().index << "  " << facet.first[1]->info().index << "  " << facet.first[2]->info().index << endl;
	outputStream << endl << "CELL_TYPES " << numCells << endl;
	for(size_t i = 0; i < numCells; i++)
		outputStream << "5" << endl;
	outputStream << endl << "CELL_DATA " << numCells << endl;
	outputStream << endl << "VECTORS lattice_vec double" << endl;
	for(const auto& facet : facets) {
		outputStream << latticeVectors[facet.second] << endl;
	}
	outputStream << endl << "SCALARS burgers_vector_type int" << endl;
	outputStream << endl << "LOOKUP_TABLE default" << endl;
	for(const auto& facet : facets) {
		outputStream << burgersVectorTypes[facet.second] << endl;
	}
}

/**
 * This function writes the simulation cell geometry to a VTK file for visualozation.
 */
void SDIA::writeSimulationCellFile(const string& filename)
{
	Vector3 corners[8];
	corners[0] = cellOrigin;
	corners[1] = cellOrigin + cellVectors[0];
	corners[2] = cellOrigin + cellVectors[0] + cellVectors[1];
	corners[3] = cellOrigin + cellVectors[1];
	corners[4] = cellOrigin + cellVectors[2];
	corners[5] = cellOrigin + cellVectors[0] + cellVectors[2];
	corners[6] = cellOrigin + cellVectors[0] + cellVectors[1] + cellVectors[2];
	corners[7] = cellOrigin + cellVectors[1] + cellVectors[2];

	ofstream stream(filename);
	stream << "# vtk DataFile Version 3.0" << endl;
	stream << "# Simulation cell" << endl;
	stream << "ASCII" << endl;
	stream << "DATASET UNSTRUCTURED_GRID" << endl;
	stream << "POINTS 8 double" << endl;
	for(int i = 0; i < 8; i++)
		stream << corners[i].x() << " " << corners[i].y() << " " << corners[i].z() << endl;

	stream << endl << "CELLS 1 9" << endl;
	stream << "8 0 1 2 3 4 5 6 7" << endl;

	stream << endl << "CELL_TYPES 1" << endl;
	stream << "12" << endl;  // Hexahedron
}

/**
 * This function is for debugging purposes only.
 *
 * Writes the dislocation segments to a file for visualization.
 */
void SDIA::writeDislocationFile(const string& filename)
{
	std::vector<Vector3> points;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector > 0) {
			cell->info().index = points.size();
			Vector3 centroid(
				0.25 * (cell->vertex(0)->point().x() + cell->vertex(1)->point().x() + cell->vertex(2)->point().x() + cell->vertex(3)->point().x()),
				0.25 * (cell->vertex(0)->point().y() + cell->vertex(1)->point().y() + cell->vertex(2)->point().y() + cell->vertex(3)->point().y()),
				0.25 * (cell->vertex(0)->point().z() + cell->vertex(1)->point().z() + cell->vertex(2)->point().z() + cell->vertex(3)->point().z()));
			points.push_back(centroid);
		}
	}

	// Write VTK output file.
	ofstream outputStream(filename);
	outputStream << "# vtk DataFile Version 3.0" << endl;
	outputStream << "# Dislocations" << endl;
	outputStream << "ASCII" << endl;
	outputStream << "DATASET UNSTRUCTURED_GRID" << endl;
	outputStream << "POINTS " << points.size() << " double" << endl;
	for(const auto& p : points) {
		outputStream << p.x() << " " << p.y() << " " << p.z() << endl;
	}

	std::vector<std::tuple<int,int,int>> segments;
	for(auto cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
		if(cell->info().burgersVector <= 0) continue;

		// Gather lattice vectors assigned to the six edges of the tetrahedron.
		int edgeVectors[6];
		for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++)
			edgeVectors[edgeIndex] = cell->info().edges[edgeIndex]->latticeVector;

		// Perform Burgers test on the four tetrahedron facets.
		for(int face = 0; face < 4; face++) {
			Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[face][0]]] +
						latticeVectors[edgeVectors[burgersCircuitEdges[face][1]]] -
						latticeVectors[edgeVectors[burgersCircuitEdges[face][2]]];
			if(b.squared_length() > epsilon) {
				auto neighbor = cell->neighbor(face);
				if(neighbor->info().burgersVector <= 0) continue;
				if(neighbor->info().index < cell->info().index) continue;
				int v1 = cell->info().index;
				int v2 = neighbor->info().index;
				int bv = latticeVectors.find(b);
				if(bv != -1) {
					if(face & 1)
						bv = reverseLatticeVectors[bv];
					if(points[v1].y() > points[v2].y()) {
						std::swap(v1, v2);
						bv = reverseLatticeVectors[bv];
					}
					segments.push_back(std::make_tuple(v1, v2, bv));
				}
			}
		}
	}

	int numSegments = segments.size();
	outputStream << endl << "CELLS " << numSegments << " " << (numSegments * 3) << endl;
	for(const auto& seg : segments)
		outputStream << "2 " << std::get<0>(seg) << "  " << std::get<1>(seg) << endl;
	outputStream << endl << "CELL_TYPES " << numSegments << endl;
	for(size_t i = 0; i < numSegments; i++)
		outputStream << "3" << endl;

	outputStream << endl << "CELL_DATA " << numSegments << endl;
	outputStream << endl << "VECTORS burgers_vector double" << endl;
	for(const auto& seg : segments)
		outputStream << latticeVectors[std::get<2>(seg)] << endl;
}

