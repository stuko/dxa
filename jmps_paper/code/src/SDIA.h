///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (2014) Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
//
//  This file is part of the program code 'Simple Dislocation Identification Algorithm'
//  that accompanies the paper:
//
//  A. Stukowski,
//  "A triangulation-based method to identify dislocations in atomistic models"
//  Journal of the Mechanics and Physics of Solids 70 (2014), 314–319
//  DOI: 10.1016/j.jmps.2014.06.009
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __SDIA_H
#define __SDIA_H

#include <string>
#include <stdexcept>
#include <limits>
#include <iterator>
#include <vector>
#include <utility>
using namespace std;

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>

#if CGAL_VERSION_NR < CGAL_VERSION_NUMBER(4,0,0)
	#error "This program requires CGAL 4.0 or higher."
#endif


#include <boost/pool/object_pool.hpp>

class SDIA
{
public:

	/*************** Dislocation identification functions **************/

	/// Precomputes some stuff prior to the actual analysis.
	void prepare(double latticeParameter, double orientation[3][3]);

	/// Calls the CGAL library to compute the Delaunay tessellation.
	/// The atomic coordinates serve as input vertices for the tessellation.
	void generateTessellation();

	/// Assigns ideal lattice vectors to edges of the tessellation.
	void assignLatticeVectors();

	/// Optimizes the assigned lattice vectors of tessellation edges.
	void optimizeEdges();

	/// Performs the Burgers test on every tetrahedral cell.
	void markDislocatedCells();

	/************************ File I/O function ************************/

	/// Parses the atomic coordinates from a LAMMPS dump file.
	void parseDumpFile(const string& filename);

	/// Parses the list of ideal lattice vectors.
	void parseLatticeVectors(const string& filename);

	/// Writes all dislocated tessellation cells to a file.
	void writeVTKFile(const string& filename);

private:

	typedef CGAL::Exact_predicates_inexact_constructions_kernel	Kernel;
	typedef CGAL::Point_3<Kernel> 								Point3;
	typedef CGAL::Vector_3<Kernel> 								Vector3;
	typedef CGAL::Aff_transformation_3<Kernel>					AffineTransformation;

	struct Edge;

	// Data structure for vertices of the tessellation.
	struct Vertex {
		Vertex() : firstEdge(nullptr), index(-1), atomIndex(-1) {}
		Vertex(size_t index) : firstEdge(nullptr), atomIndex(index) {}
		Edge* firstEdge;
		size_t atomIndex;
		size_t index;
	};

	struct Cell {
		array<Edge*,6> edges;
		int burgersVector;
		int index;
		bool isValid() const { return edges.front() != nullptr; }
		void markInvalid() { edges.fill(nullptr); }
	};

	typedef CGAL::Triangulation_cell_base_with_info_3<Cell,Kernel>		CbDS;
	typedef CGAL::Triangulation_vertex_base_with_info_3<Vertex,Kernel>	VbDS;
	typedef CGAL::Triangulation_data_structure_3<VbDS, CbDS>			TDS;
	typedef CGAL::Delaunay_triangulation_3<Kernel, TDS>					DT;

	// Data structure for edges of the tessellation.
	struct Edge {
		Edge(int lv, DT::Vertex_handle v) : latticeVector(lv), initialLatticeVector(lv), v2(v), isFlagged(true) {}
		int latticeVector;
		int initialLatticeVector;
		DT::Vertex_handle v2;
		Edge* nextEdge;
		Edge* oppositeEdge;
		bool isFlagged;
		void flagEdge() { oppositeEdge->isFlagged = this->isFlagged = true; }
	};

	/// Small helper class that stores a list of vectors.
	class VectorList : public vector<Vector3>
	{
	public:

		/// Returns the vector from this list that is closest to the given vector.
		/// Returns -1 if no vector in the list is closer to the given vector than the threshold 'maxDeviation'.
		int closestVector(const Vector3& v, double maxDeviation) const {
			int result = -1;
			for(size_t i = 0; i < size(); i++) {
				double d = (v - (*this)[i]).squared_length();
				if(d < maxDeviation) {
					maxDeviation = d;
					result = i;
				}
			}
			return result;
		}

		/// Finds the index of a certain vector in this list.
		/// Returns -1 if no such vector exists in the list.
		int find(const Vector3& v) const {
			for(int i = 0; i < size(); i++) {
				if((v - (*this)[i]).squared_length() <= epsilon)
					return i;
			}
			return -1;
		}
	};

	/// List of six edges of a tetrahedron.
	static const int edgeVertices[6][2];

	/// List of Burgers circuits on a tetrahedron, each defined by a sequence of three edges.
	static const int burgersCircuitEdges[4][3];

	/// A small number.
	static constexpr double epsilon = 1e-6;

	/// The list of ideal lattice vectors.
	VectorList latticeVectors;

	/// List of lattice vectors, which have been scaled by the lattice constant.
	VectorList scaledLatticeVectors;

	/// The lookup map for reverse vectors.
	vector<int> reverseLatticeVectors;

	/// The list of Burgers vector types.
	vector<int> burgersVectorTypes;

	/// Lookup table for vector sums.
	map<pair<int,int>,int> latticeVectorSums;

	/// The atomic coordinates loaded from the input file.
	vector<Point3> atoms;

	/// The Delaunay tessellation.
	DT tessellation;

	/// Memory pool used for fast allocation of Edge structures.
	boost::object_pool<Edge> edgePool;

	/// The number of dislocated tessellation cells.
	size_t numDislocatedCells = 0;

	/// The periodic boundary flags of the simulation cell.
	std::array<bool, 3> pbc = std::array<bool, 3>{{ false, false, false }};

	/// The simulation cell vectors.
	Vector3 cellVectors[3] = { Vector3(0,0,0), Vector3(0,0,0), Vector3(0,0,0) };

	/// The simulation cell origin.
	Vector3 cellOrigin = Vector3(0,0,0);

	/// Returns the edge connecting two adjacent vertices.
	Edge* findEdge(DT::Vertex_handle v1, DT::Vertex_handle v2) const {
		for(Edge* e = v1->info().firstEdge; e != nullptr; e = e->nextEdge)
			if(e->v2 == v2) return e;
		return nullptr;
	}

	/// Computes the sum of two lattice vectors.
	int vectorSum(int v1, int v2) const {
		if(v1 > v2) swap(v1, v2);
		auto iter = latticeVectorSums.find(make_pair(v1,v2));
		if(iter == latticeVectorSums.end()) return -1;
		return iter->second;
	}

	/// Writes all tessellation edges that belong to dislocated cells to a file (only for debugging purposes).
	void writeEdgesOfDislocatedCellsToFile(const string& filename);

	/// Writes a set of tessellation edges to a VTK file (only for debugging purposes).
	void writeEdgeFile(const string& filename, const set<Edge*>& edges);

	/// Writes all tessellation facets that belong to dislocated cells to a file (only for debugging purposes).
	void writeFacetsOfDislocatedCellsToFile(const string& filename);

	/// Writes a set of tessellation facets to a VTK file (only for debugging purposes).
	void writeFacetFile(const string& filename, const vector<pair<array<DT::Vertex_handle,3>,int>>& facets);

	/// This function writes the simulation cell geometry to a VTK file for visualozation.
	void writeSimulationCellFile(const string& filename);

	/// Writes the dislocation segments to a file for visualization.
	void writeDislocationFile(const string& filename);
};

#endif
