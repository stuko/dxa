#ifndef __CAREADER_H
#define __CAREADER_H

#include <istream>
#include "../dislocation_network/SimulationCell.h"
#include "../dislocation_network/DislocationNetwork.h"
#include "../interface_mesh/PartitionMesh.h"
#include "../crystal_cluster/ClusterGraph.h"

/**
 * This class reads data from a CA file.
 */
class CAReader
{
public:

	/// Constructor.
	CAReader(SimulationCell& cell, ClusterGraph& clusterGraph) : _cell(cell), _clusterGraph(clusterGraph) {}

	/// Returns the simulation cell.
	SimulationCell& cell() const { return _cell; }

	/// Returns the simulation timestep when the dislocations have been extracted.
	int simulationTimestep() const { return _simulationTimestep; }

	/// Returns a const-reference to the cluster graph.
	ClusterGraph& clusterGraph() const { return _clusterGraph; }

	/// Reads the given data objects from a CA file stream.
	void readCAFile(std::istream& stream, DislocationNetwork* dislocationNetwork, HalfEdgeMesh<>* defectMesh, PartitionMesh* partitionMesh);

private:

	/// The simulation cell.
	SimulationCell& _cell;

	/// The time of the simulation at which the dislocations have been extracted.
	int _simulationTimestep = -1;

	/// The associated cluster graph.
	ClusterGraph& _clusterGraph;
	
	/// List of Burgers vector families defined in the CA file.
	std::vector<std::pair<std::string, Vector3>> _burgersFamilies;
};

#endif // __CAREADER_H
