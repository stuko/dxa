#include <stdexcept>
#include <sstream>
#include <string>
#include <iostream>
#include <istream>
#include <algorithm>
#include "ParserStream.h"

using namespace std;

/******************************************************************************
* Constructor.
******************************************************************************/
ParserStream::ParserStream(const char* filename) : ifstream(filename), _filename(filename)
{
	if(!is_open())
		throw runtime_error("Failed to open input file for reading.");
}

/******************************************************************************
* Reads one line from the input stream and compares it to the expected string.
******************************************************************************/
void ParserStream::expectLine(const char* line)
{
	string s;
	readLine(s);
	const char* l = line;
	for(auto c : s) {
		if(*l == '\0') break;
		if(c != *l) {
			ostringstream msg;
			msg << "Invalid input file '" << _filename << "'. Expected literal '" << line << "' but found '" << s << "'.";
			throw runtime_error(msg.str());
		}
		l++;
	}
	if(*l != '\0') {
		ostringstream msg;
		msg << "Invalid input file '" << _filename << "'. Expected literal '" << line << "' but found end of file.";
		throw runtime_error(msg.str());
	}
}

/******************************************************************************
* Reads one line from the input stream.
******************************************************************************/
void ParserStream::readLine(string& line)
{
	if(fail())
		throw runtime_error("Failed to parse input file. Input stream error.");

	// Left-trim white space and read line.
	std::getline(*this >> ws, line);

	// Right-trim whitespace.
	line.erase(find_if(line.rbegin(), line.rend(), not1(ptr_fun<int,int>(std::isspace))).base(), line.end());
};
