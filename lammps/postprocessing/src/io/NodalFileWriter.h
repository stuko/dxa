#ifndef __NODAL_FILE_WRITER_H
#define __NODAL_FILE_WRITER_H

#include <ostream>
#include "../dislocation_network/SimulationCell.h"
#include "../dislocation_network/DislocationNetwork2.h"
#include "../crystal_cluster/ClusterGraph.h"

/**
 * This class writes data to a nodal data file.
 */
class NodalFileWriter
{
public:

	/// Constructor.
	NodalFileWriter(const SimulationCell& cell, const ClusterGraph& clusterGraph, int simulationTimestep = -1) : _cell(cell), _clusterGraph(clusterGraph), _simulationTimestep(simulationTimestep) {}

	/// Returns the simulation cell.
	const SimulationCell& cell() const { return _cell; }

	/// Returns the simulation timestep when the dislocations have been extracted.
	int simulationTimestep() const { return _simulationTimestep; }

	/// Returns a const-reference to the cluster graph.
	const ClusterGraph& clusterGraph() const { return _clusterGraph; }

	/// Writes the given data objects to a file stream.
	void writeNodalFile(std::ostream& stream, DislocationNetwork2* dislocationNetwork);

private:

	/// The simulation cell.
	const SimulationCell& _cell;

	/// The time of the simulation at which the dislocations have been extracted.
	int _simulationTimestep;

	/// The associated cluster graph.
	const ClusterGraph& _clusterGraph;
};

#endif // __NODAL_FILE_WRITER_H
