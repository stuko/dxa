#ifndef __PARSER_STREAM_H
#define __PARSER_STREAM_H

#include <fstream>

/**
 * This helper class provides functions to read data from a text file.
 */
class ParserStream : public std::ifstream
{
public:

	/// Constructor.
	ParserStream(const char* filename);

	/// Reads one line from the input stream and compares it to the expected string.
	void expectLine(const char* line);

	/// Reads one line from the input stream.
	void readLine(std::string& line);

private:

	std::string _filename;
};

#endif // __PARSER_STREAM_H
