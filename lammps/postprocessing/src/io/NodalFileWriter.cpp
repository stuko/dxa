#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include "NodalFileWriter.h"
#include "../dislocation_network/DislocationNetwork2.h"
#include "../CodeVersion.h"

/******************************************************************************
* Writes the given data objects to a file stream.
******************************************************************************/
void NodalFileWriter::writeNodalFile(std::ostream& stream, DislocationNetwork2* dislocationNetwork)
{
	// Write simulation cell geometry.
	stream << "SIMULATION_CELL_ORIGIN\n"
			<< cell().matrix().column(3).x() << " " << cell().matrix().column(3).y() << " " << cell().matrix().column(3).z() << "\n";
	stream << "SIMULATION_CELL_MATRIX\n"
			<< cell().matrix().column(0).x() << " " << cell().matrix().column(1).x() << " " << cell().matrix().column(2).x() << "\n"
			<< cell().matrix().column(0).y() << " " << cell().matrix().column(1).y() << " " << cell().matrix().column(2).y() << "\n"
			<< cell().matrix().column(0).z() << " " << cell().matrix().column(1).z() << " " << cell().matrix().column(2).z() << "\n";
	stream << "PBC_FLAGS\n"
			<< (int)cell().pbcFlags()[0] << " "
			<< (int)cell().pbcFlags()[1] << " "
			<< (int)cell().pbcFlags()[2] << "\n";
	stream << "NUMBER_OF_NODES\n" << dislocationNetwork->nodes().size() << "\n";

	stream << "NODES\n";
	for(DislocationNetwork2::Node* node : dislocationNetwork->nodes()) {
		int numArms = 0;
		for(DislocationNetwork2::Segment* seg = node->segments; seg != nullptr; seg = seg->nextNodeSegment)
			numArms++;
		stream << (node->index+1) << " " << node->pos.x() << " " << node->pos.y() << " " << node->pos.z() << " " << numArms << "\n";
		for(DislocationNetwork2::Segment* seg = node->segments; seg != nullptr; seg = seg->nextNodeSegment) {
			stream << (seg->node2->index+1) << " " << seg->burgersVector.localVec().x() << " " << seg->burgersVector.localVec().y() << " " << seg->burgersVector.localVec().z() << "\n";
		}
	}
}

