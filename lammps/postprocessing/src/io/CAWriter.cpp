#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include "CAWriter.h"
#include "../dislocation_network/DislocationNetwork.h"
#include "../dislocation_network/DislocationNetwork2.h"
#include "../CodeVersion.h"

/******************************************************************************
* Writes the given data objects to a CA file stream.
******************************************************************************/
void CAWriter::writeCAFile(std::ostream& stream, DislocationNetwork* dislocationNetwork, HalfEdgeMesh<>* defectMesh, PartitionMesh* partitionMesh, DislocationNetwork2* dislocationNetwork2)
{
	// Write file header.
	stream << "CA_FILE_VERSION 7\n";
	stream << "CA_LIB_VERSION " << TRACE_TOOL_VERSION << "\n";
	if(simulationTimestep() >= 0)
		stream << "METADATA SIMULATION_TIMESTEP " << simulationTimestep() << "\n";

	// Gather structure types.
	std::map<int, std::string> structureTypes;
	for(Cluster* cluster : clusterGraph().clusters()) {
		if(cluster->structure != 0)
			structureTypes[cluster->structure] = cluster->structureName;
	}

	// Write list of structure types.
	stream << "STRUCTURE_TYPES " << structureTypes.size() << "\n";
	for(const auto& s : structureTypes) {
		stream << "STRUCTURE_TYPE " << s.first << "\n";
		stream << "NAME " << s.second << "\n";
		if(s.second == "bcc") {
			stream << "BURGERS_VECTOR_FAMILIES 3\n";
			stream << "BURGERS_VECTOR_FAMILY ID 1\n1/2<111>\n0.5 0.5 0.5\n0 1 0\n";
			stream << "BURGERS_VECTOR_FAMILY ID 2\n<100>\n1.0 0.0 0.0\n1 0.3 0.8\n";
			stream << "BURGERS_VECTOR_FAMILY ID 3\n<110>\n1.0 1.0 0.0\n0.2 0.5 1.0\n";
		}
		else if(s.second == "fcc") {
			stream << "BURGERS_VECTOR_FAMILIES 3\n";
			stream << "BURGERS_VECTOR_FAMILY ID 1\n1/2<110> (Perfect)\n0.5 0.5 0.0\n0.2 0.2 1.0\n";
			stream << "BURGERS_VECTOR_FAMILY ID 2\n1/6<112> (Shockley)\n0.1666667 0.1666667 0.3333334\n0.0 1.0 0.0\n";
			stream << "BURGERS_VECTOR_FAMILY ID 3\n1/6<110> (Stair-rod)\n0.1666667 0.1666667 0.0\n1.0 0.0 1.0\n";
			stream << "BURGERS_VECTOR_FAMILY ID 4\n1/3<001> (Hirth)\n0.3333334 0.0 0.0\n1.0 1.0 0.0\n";
			stream << "BURGERS_VECTOR_FAMILY ID 5\n1/3<111> (Frank)\n0.3333334 0.3333334 0.3333334\n0.0 1.0 1.0\n";
		}
		stream << "END_STRUCTURE_TYPE\n";
	}

	// Write simulation cell geometry.
	stream << "SIMULATION_CELL_ORIGIN "
			<< cell().matrix().column(3).x() << " " << cell().matrix().column(3).y() << " " << cell().matrix().column(3).z() << "\n";
	stream << "SIMULATION_CELL_MATRIX" << "\n"
			<< cell().matrix().column(0).x() << " " << cell().matrix().column(1).x() << " " << cell().matrix().column(2).x() << "\n"
			<< cell().matrix().column(0).y() << " " << cell().matrix().column(1).y() << " " << cell().matrix().column(2).y() << "\n"
			<< cell().matrix().column(0).z() << " " << cell().matrix().column(1).z() << " " << cell().matrix().column(2).z() << "\n";
	stream << "PBC_FLAGS "
			<< (int)cell().pbcFlags()[0] << " "
			<< (int)cell().pbcFlags()[1] << " "
			<< (int)cell().pbcFlags()[2] << "\n";

	// Write list of clusters.
	assert(clusterGraph().clusters().size() >= 1);
	stream << "CLUSTERS " << (clusterGraph().clusters().size() - 1) << "\n";
	for(Cluster* cluster : clusterGraph().clusters()) {
		if(cluster->id == 0) continue;
		stream << "CLUSTER " << cluster->id << "\n";
		stream << "CLUSTER_STRUCTURE " << cluster->structure << "\n";
		stream << "CLUSTER_ORIENTATION\n";
		for(size_t row = 0; row < 3; row++)
			stream << cluster->orientation(row,0) << " " << cluster->orientation(row,1) << " " << cluster->orientation(row,2) << "\n";
		stream << "CLUSTER_COLOR " << cluster->color.x() << " " << cluster->color.y() << " " << cluster->color.z() << "\n";
		stream << "CLUSTER_SIZE " << cluster->atomCount << "\n";
		stream << "END_CLUSTER\n";
	}

	if(dislocationNetwork != nullptr) {
		// Write list of dislocation segments.
		stream << "DISLOCATIONS " << dislocationNetwork->segments().size() << "\n";
		for(DislocationSegment* segment : dislocationNetwork->segments()) {

			// Make sure consecutive identifiers have been assigned to segments.
			assert(segment->id >= 0 && segment->id < dislocationNetwork->segments().size());
			assert(dislocationNetwork->segments()[segment->id] == segment);

			stream << segment->id << "\n";

			stream << segment->burgersVector.localVec().x() << " " << segment->burgersVector.localVec().y() << " " << segment->burgersVector.localVec().z() << "\n";
			stream << "1\n";	// Always cluster 1 (i.e. the first cluster)

			// Write polyline.
			stream << segment->line.size() << "\n";
			if(segment->coreSize.empty()) {
				for(const Point3& p : segment->line) {
					stream << p.x() << " " << p.y() << " " << p.z() << "\n";
				}
			}
			else {
				assert(segment->coreSize.size() == segment->line.size());
				auto cs = segment->coreSize.cbegin();
				for(const Point3& p : segment->line) {
					stream << p.x() << " " << p.y() << " " << p.z() << " " << (*cs++) << "\n";
				}
			}
		}

		// Write dislocation connectivity information.
		stream << "DISLOCATION_JUNCTIONS\n";
		int index = 0;
		for(DislocationSegment* segment : dislocationNetwork->segments()) {
			assert(segment->forwardNode().junctionRing->segment->id < dislocationNetwork->segments().size());
			assert(segment->backwardNode().junctionRing->segment->id < dislocationNetwork->segments().size());

			for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
				DislocationNode* otherNode = segment->nodes[nodeIndex]->junctionRing;
				stream << (int)otherNode->isForwardNode() << " " << otherNode->segment->id << "\n";
			}
			index++;
		}
	}

	// Output defect mesh.
	if(defectMesh != nullptr) {
		// Write list of vertices of defect mesh.
		stream << "DEFECT_MESH_VERTICES " << defectMesh->vertexCount() << "\n";
		for(size_t vertexIdx = 0; vertexIdx < defectMesh->vertexCount(); vertexIdx++) {
			HalfEdgeMesh<>::Vertex* vertex = defectMesh->vertex(vertexIdx);

			// Make sure indices have been assigned to vertices.
			assert(defectMesh->vertex(vertex->index()) == vertex);

			stream << vertex->pos().x() << " " << vertex->pos().y() << " " << vertex->pos().z() << "\n";
		}

		// Write list of facets of defect mesh.
		stream << "DEFECT_MESH_FACETS " << defectMesh->faceCount() << "\n";
		for(size_t facetIdx = 0; facetIdx < defectMesh->faceCount(); facetIdx++) {
			HalfEdgeMesh<>::Face* facet = defectMesh->face(facetIdx);
			HalfEdgeMesh<>::Edge* edge = facet->edges();
			for(int v = 0; v < 3; v++, edge = edge->nextFaceEdge())
				stream << edge->vertex1()->index() << " ";
			stream << "\n";
		}

		// Write facet adjacency information.
		for(size_t facetIdx = 0; facetIdx < defectMesh->faceCount(); facetIdx++) {
			HalfEdgeMesh<>::Face* facet = defectMesh->face(facetIdx);
			HalfEdgeMesh<>::Edge* edge = facet->edges();
			for(int v = 0; v < 3; v++, edge = edge->nextFaceEdge()) {
				stream << edge->oppositeEdge()->face()->index() << " ";
			}
			stream << "\n";
		}
	}

	if(partitionMesh != nullptr) {
		// Serialize list of vertices.
		stream << "PARTITION_MESH_VERTICES " << partitionMesh->vertexCount() << "\n";
		for(size_t vertexIdx = 0; vertexIdx < partitionMesh->vertexCount(); vertexIdx++) {
			PartitionMesh::Vertex* vertex = partitionMesh->vertex(vertexIdx);

			// Make sure indices have been assigned to vertices.
			assert(vertex->index() >= 0 && vertex->index() < partitionMesh->vertexCount());

			stream << vertex->pos().x() << " " << vertex->pos().y() << " " << vertex->pos().z() << "\n";
		}

		// Serialize list of facets.
		stream << "PARTITION_MESH_FACETS " << partitionMesh->faceCount() << "\n";
		for(size_t facetIdx = 0; facetIdx < partitionMesh->faceCount(); facetIdx++) {
			PartitionMesh::Face* facet = partitionMesh->face(facetIdx);
			stream << facet->region << " ";
			PartitionMesh::Edge* e = facet->edges();
			do {
				stream << e->vertex1()->index() << " ";
				e = e->nextFaceEdge();
			}
			while(e != facet->edges());
			stream << "\n";
		}

		// Serialize facet adjacency information.
		for(size_t facetIdx = 0; facetIdx < partitionMesh->faceCount(); facetIdx++) {
			PartitionMesh::Face* facet = partitionMesh->face(facetIdx);
			assert(facet->oppositeFace != nullptr);
			stream << facet->oppositeFace->index() << " ";
			PartitionMesh::Edge* e = facet->edges();
			do {
				assert(facet->oppositeFace->findEdge(e->nextManifoldEdge->vertex1(), e->nextManifoldEdge->vertex2()) != nullptr);
				stream << e->oppositeEdge()->face()->index() << " " << e->nextManifoldEdge->vertex1()->index() << " " << e->nextManifoldEdge->vertex2()->index() << " ";
				e = e->nextFaceEdge();
			}
			while(e != facet->edges());
			stream << "\n";
		}
	}

	if(dislocationNetwork2 != nullptr) {
		// Serialize list of vertices.
		stream << "SLIP_SURFACE_VERTICES " << dislocationNetwork2->nodes().size() << "\n";
		for(DislocationNetwork2::Node* node : dislocationNetwork2->nodes()) {

			// Make sure indices have been assigned to nodes.
			assert(node->index >= 0 && node->index < dislocationNetwork2->nodes().size());

			stream << node->pos.x() << " " << node->pos.y() << " " << node->pos.z() << "\n";
		}

		// Serialize list of slip surface facets.
		stream << "SLIP_SURFACE_FACETS " << dislocationNetwork2->slipFaces().size() << "\n";
		for(DislocationNetwork2::SlipFace* facet : dislocationNetwork2->slipFaces()) {
			stream << facet->slipVector.localVec().x() << " " << facet->slipVector.localVec().y() << " " << facet->slipVector.localVec().z();
			stream << " 1 ";	// Always cluster 1 (i.e. the first cluster)
			stream << facet->nodes[0]->index << " " << facet->nodes[1]->index << " " << facet->nodes[2]->index;
			stream << "\n";
		}

		// Serialize list of vertices.
		stream << "STACKING_FAULT_VERTICES " << dislocationNetwork2->nodes().size() << "\n";
		for(DislocationNetwork2::Node* node : dislocationNetwork2->nodes()) {

			// Make sure indices have been assigned to nodes.
			assert(node->index >= 0 && node->index < dislocationNetwork2->nodes().size());

			stream << node->pos.x() << " " << node->pos.y() << " " << node->pos.z() << "\n";
		}

		// Serialize list of stacking fault facets.
		stream << "STACKING_FAULT_FACETS " << dislocationNetwork2->stackingFaultFaces().size() << "\n";
		for(DislocationNetwork2::StackingFaultFace* facet : dislocationNetwork2->stackingFaultFaces()) {
			stream << facet->slipVector.localVec().x() << " " << facet->slipVector.localVec().y() << " " << facet->slipVector.localVec().z();
			stream << " 1 ";	// Always cluster 1 (i.e. the first cluster)
			stream << facet->nodes[0]->index << " " << facet->nodes[1]->index << " " << facet->nodes[2]->index;
			stream << "\n";
		}		
	}
}

