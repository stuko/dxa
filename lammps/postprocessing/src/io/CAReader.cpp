#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include "CAReader.h"
#include "../dislocation_network/DislocationNetwork.h"
#include "../CodeVersion.h"

/******************************************************************************
* Reads the given data objects from a CA file stream.
******************************************************************************/
void CAReader::readCAFile(std::istream& stream, DislocationNetwork* dislocationNetwork, HalfEdgeMesh<>* defectMesh, PartitionMesh* partitionMesh)
{
	using namespace std;

	auto readLine = [&stream](string& line) {
		if(stream.fail())
			throw runtime_error("Failed to parse CA file. Input stream error.");
		// Left-trim white space and read line.
		getline(stream >> ws, line);
		// Right-trim whitespace.
		line.erase(find_if(line.rbegin(), line.rend(), not1(ptr_fun<int,int>(std::isspace))).base(), line.end());
	};

	auto startsWith = [](const string& line, const char* s) {
		for(auto l = line.begin(); *s && l != line.end(); ++s, ++l)
			if(*l != *s) return false;
		return true;
	};

	// Read file header.
	string line;
	readLine(line);
	if(line != "CA_FILE_VERSION 5" && line != "CA_FILE_VERSION 6" && line != "CA_FILE_VERSION 7")
		throw runtime_error("Failed to parse file. This is not a valid CA file.");

	AffineTransformation cellMatrix = AffineTransformation::Zero();
	int pbcFlags[3] = { 1, 1, 1 };

	// Parse keyword sections.
	while(!stream.eof()) {
		if(!stream)
			throw runtime_error("Error while parsing CA file.");
		readLine(line);
		if(startsWith(line, "SIMULATION_CELL_ORIGIN ")) {
			if(sscanf(line.c_str(), "SIMULATION_CELL_ORIGIN " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING, &cellMatrix(0,3), &cellMatrix(1,3), &cellMatrix(2,3)) != 3)
				throw runtime_error("Failed to parse CA file. Invalid cell origin.");
		}
		else if(startsWith(line, "SIMULATION_CELL_MATRIX")) {
			for(size_t row = 0; row < 3; row++) {
				readLine(line);
				if(sscanf(line.c_str(), FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING,
						&cellMatrix(row,0), &cellMatrix(row,1), &cellMatrix(row,2)) != 3)
					throw runtime_error("Failed to parse CA file. Invalid cell matrix.");
			}
		}
		else if(startsWith(line, "METADATA SIMULATION_TIMESTEP ")) {
			if(sscanf(line.c_str(), "METADATA SIMULATION_TIMESTEP %i", &_simulationTimestep) != 1)
				throw runtime_error("Failed to parse CA file. Invalid simulation timestep.");
		}
		else if(startsWith(line, "BURGERS_VECTOR_FAMILY")) {
			string familyName;
			readLine(familyName);
			readLine(line);
			Vector3 b;
				if(sscanf(line.c_str(), FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING,
						&b.x(), &b.y(), &b.z()) != 3)
					throw runtime_error("Failed to parse CA file. Invalid Burgers vectpr.");
			_burgersFamilies.push_back(std::make_pair(familyName, b));
		}
		else if(startsWith(line, "PBC_FLAGS ")) {
			if(sscanf(line.c_str(), "PBC_FLAGS %i %i %i", &pbcFlags[0], &pbcFlags[1], &pbcFlags[2]) != 3)
				throw runtime_error("Failed to parse CA file. Invalid PBC flags.");
		}
		else if(startsWith(line, "CLUSTERS ")) {
			int numClusters;
			if(sscanf(line.c_str(), "CLUSTERS %i", &numClusters) != 1)
				throw runtime_error("Failed to parse CA file. Invalid number of clusters.");
			for(int index = 0; index < numClusters; index++) {
				int clusterId = 0, patternId = 0;
				Matrix3 orientation = Matrix3::Identity();
				Vector3 color(1,1,1);
				size_t atomCount = 0;
				while(!stream.eof()) {
					readLine(line);
					if(startsWith(line, "CLUSTER ")) {
						if(sscanf(line.c_str(), "CLUSTER %i", &clusterId) != 1)
							throw runtime_error("Failed to parse CA file. Invalid cluster ID.");
					}
					else if(startsWith(line, "CLUSTER_ORIENTATION")) {
						for(size_t row = 0; row < 3; row++) {
							readLine(line);
							if(sscanf(line.c_str(), FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING,
									&orientation(row,0), &orientation(row,1), &orientation(row,2)) != 3)
								throw runtime_error("Failed to parse CA file. Invalid cluster orientation matrix.");
						}
					}
					else if(startsWith(line, "CLUSTER_STRUCTURE ")) {
						if(sscanf(line.c_str(), "CLUSTER_STRUCTURE %i", &patternId) != 1)
							throw runtime_error("Failed to parse file. Invalid cluster structure type.");
					}
					else if(startsWith(line, "CLUSTER_SIZE ")) {
						if(sscanf(line.c_str(), "CLUSTER_SIZE %zu", &atomCount) != 1)
							throw runtime_error("Failed to parse file. Invalid cluster size.");
					}
					else if(startsWith(line, "CLUSTER_COLOR ")) {
						if(sscanf(line.c_str(), "CLUSTER_COLOR " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING, &color.x(), &color.y(), &color.z()) != 3)
							throw runtime_error("Failed to parse file. Invalid cluster color.");
					}
					else if(startsWith(line, "END_CLUSTER"))
						break;
				}
				Cluster* cluster = clusterGraph().createCluster(patternId);
				if(cluster->id != clusterId)
					throw runtime_error("Failed to parse CA file. Inconsistent cluster ID.");
				cluster->orientation = orientation;
				cluster->atomCount = atomCount;
				cluster->color = color;
			}
			stream >> ws;
		}
		else if(startsWith(line, "DISLOCATIONS ") && dislocationNetwork != nullptr) {
			assert(dislocationNetwork->segments().empty());

			int numDislocations;
			if(sscanf(line.c_str(), "DISLOCATIONS %i", &numDislocations) != 1)
				throw runtime_error("Failed to parse CA file. Invalid number of dislocation lines.");
			for(int index = 0; index < numDislocations; index++) {

				int segmentId = -1;
				int clusterId = -1;
				Vector3 burgersVector;

				stream >> segmentId;
				stream >> burgersVector.x() >> burgersVector.y() >> burgersVector.z();
				stream >> clusterId;

				Cluster* cluster = clusterGraph().findCluster(clusterId);
				if(!cluster)
					throw runtime_error("Failed to parse CA file. Invalid cluster ID in dislocation line record.");
				DislocationSegment* segment = dislocationNetwork->createSegment(ClusterVector(burgersVector, cluster));

				// Read polyline.
				int numPoints;
				stream >> numPoints;
				stream >> ws;
				if(numPoints <= 1)
					throw runtime_error("Failed to parse CA file. Invalid number of dislocation points.");
				segment->line.resize(numPoints);
				for(Point3& p : segment->line) {
					int coreSize = 0;
					readLine(line);
					if(sscanf(line.c_str(), FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " %i", &p.x(), &p.y(), &p.z(), &coreSize) < 3)
						throw runtime_error("Failed to parse CA file. Invalid dislocation point.");
					if(coreSize > 0)
						segment->coreSize.push_back(coreSize);
				}
			}
			stream >> ws;
		}
		else if(startsWith(line, "DISLOCATION_JUNCTIONS") && dislocationNetwork != nullptr) {
			for(DislocationSegment* segment : dislocationNetwork->segments()) {
				for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
					int isForward, otherSegmentIndex;
					stream >> isForward >> otherSegmentIndex;
					if(otherSegmentIndex < 0 || otherSegmentIndex >= dislocationNetwork->segments().size())
						throw runtime_error("Failed to parse CA file. Invalid dislocation connectivity.");
					segment->nodes[nodeIndex]->junctionRing = dislocationNetwork->segments()[otherSegmentIndex]->nodes[isForward ? 0 : 1];
				}
			}
			stream >> ws;
		}
		else if(startsWith(line, "PARTITION_MESH_VERTICES ") && partitionMesh != nullptr) {
			// Read partition mesh vertices.
			int numVertices;
			if(sscanf(line.c_str(), "PARTITION_MESH_VERTICES %i", &numVertices) != 1)
				throw runtime_error("Failed to parse file. Invalid number of mesh vertices.");
			//partitionMesh->reserveVertices(numVertices);
			for(int index = 0; index < numVertices; index++) {
				Point3 p;
				readLine(line);
				if(sscanf(line.c_str(), FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING " " FLOATTYPE_SCANF_STRING, &p.x(), &p.y(), &p.z()) != 3)
					throw runtime_error("Failed to parse file. Invalid partition mesh vertex.");
				partitionMesh->createVertex(p);
			}
		}
		else if(startsWith(line, "PARTITION_MESH_FACETS ") && partitionMesh != nullptr) {
			// Read partition mesh facets.
			int numFacets;
			if(sscanf(line.c_str(), "PARTITION_MESH_FACETS %i", &numFacets) != 1)
				throw runtime_error("Failed to parse file. Invalid number of mesh facets.");
			//partitionMesh->reserveFaces(numFacets);
			for(int index = 0; index < numFacets; index++) {
				int v[3];
				int region;
				readLine(line);
				if(sscanf(line.c_str(), "%i %i %i %i", &region, &v[0], &v[1], &v[2]) != 4)
					throw runtime_error("Failed to parse file. Invalid triangle facet.");
				PartitionMesh::Face* face = partitionMesh->createFace({ partitionMesh->vertex(v[0]), partitionMesh->vertex(v[1]), partitionMesh->vertex(v[2]) });
				face->region = region;
			}

			// Read facet adjacency information.
			for(int index = 0; index < numFacets; index++) {
				int v[3];
				int mfe[3][2];
				int oppositeFaceIndex;
				readLine(line);
				if(sscanf(line.c_str(), "%i %i %i %i %i %i %i %i %i %i", &oppositeFaceIndex,
						&v[0], &mfe[0][0], &mfe[0][1],
						&v[1], &mfe[1][0], &mfe[1][1],
						&v[2], &mfe[2][0], &mfe[2][1]) != 10)
					throw runtime_error("Failed to parse file. Invalid triangle adjacency info.");
				PartitionMesh::Face* oppositeFace = partitionMesh->face(oppositeFaceIndex);
				partitionMesh->face(index)->oppositeFace = oppositeFace;
				PartitionMesh::Edge* edge = partitionMesh->face(index)->edges();
				for(int i = 0; i < 3; i++, edge = edge->nextFaceEdge()) {
					PartitionMesh::Edge* manifoldEdge = oppositeFace->findEdge(partitionMesh->vertex(mfe[i][0]), partitionMesh->vertex(mfe[i][1]));
					assert(manifoldEdge != nullptr);
					edge->nextManifoldEdge = manifoldEdge;
					if(edge->oppositeEdge() != nullptr) continue;
					PartitionMesh::Face* adjacentFace = partitionMesh->face(v[i]);
					PartitionMesh::Edge* oppositeEdge = adjacentFace->findEdge(edge->vertex2(), edge->vertex1());
					assert(oppositeEdge != nullptr);
					edge->linkToOppositeEdge(oppositeEdge);
				}
			}
		}

	}

	cell().setMatrix(cellMatrix);
	cell().setPbcFlags((bool)pbcFlags[0], (bool)pbcFlags[1], (bool)pbcFlags[2]);
}

