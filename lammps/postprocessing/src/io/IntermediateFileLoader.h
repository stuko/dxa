#ifndef __INTERMEDIATE_FILE_LOADER_H
#define __INTERMEDIATE_FILE_LOADER_H

#include <cstddef>
#include <string>
#include <vector>

#include "../dislocation_network/SimulationCell.h"
#include "ParserStream.h"

/**
 * Parses the header of an intermediate file.
 */
class IntermediateFileLoader
{
public:

	/// Reads the simulation cell and timestep info from the intermediate file header.
	void loadIntermediateFileHeader(ParserStream& stream);

	/// Returns the simulation cell geometry.
	const SimulationCell& cell() const { return _cell; }

	/// Returns the simulation cell geometry.
	SimulationCell& cell() { return _cell; }

	/// Returns the time of the simulation when the intermediate files were written was created.
	int simulationTimestep() const { return _simulationTimestep; }

	/// Sets the time of the simulation.
	void setSimulationTimestep(int timestep) { _simulationTimestep = timestep; }

	/// Writes the cell geometry to the given file in VTK format.
	void writeCellVTKFile(const std::string& filename) const;

	/// Return the crystal structure type.
	const std::string& latticeStructureName() const { return _latticeStructureName; }

	/// The list of ideal lattice vectors.
	const std::vector<Vector3>& latticeVectors() const { return _latticeVectors; }

	/// The list of transformed ideal lattice vectors.
	const std::vector<Vector3>& transformedLatticeVectors() const { return _transformedLatticeVectors; }

	/// Determines the orientation matrix of the single crystal.
	Matrix3 crystalOrientation();

	/// Returns the total number of intermediate file pieces.
	int numPieces() const { return _numPieces; }

	/// Returns the number of intermediate file pieces loaded so far.
	int numPiecesLoaded() const { return _numPiecesLoaded; }

	/// Returns the size of the processor grid that produced the intermediate files.
	const Vector3I processorGrid() const { return _processorGrid; }

private:

	/// Stores the simulation cell geometry.
	SimulationCell _cell;

	/// The time of the simulation when the interface mesh was created.
	int _simulationTimestep;

	/// The number of partial mesh pieces.
	int _numPieces = 0;

	/// The number of mesh pieces from different processors that have been loaded so far.
	int _numPiecesLoaded = 0;

	/// The size of the processor grid that produced the interface mesh.
	Vector3I _processorGrid;

	/// The type of crystal.
	std::string _latticeStructureName;

	/// The list of ideal lattice vectors.
	std::vector<Vector3> _latticeVectors;

	/// The list of transformed ideal lattice vectors.
	std::vector<Vector3> _transformedLatticeVectors;
};

#endif // __INTERMEDIATE_FILE_LOADER_H
