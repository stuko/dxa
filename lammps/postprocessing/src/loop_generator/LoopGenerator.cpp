#include <map>
#include <iostream>
#include <fstream>
#include "LoopGenerator.h"

//#include <Eigen/Dense>

/******************************************************************************
* Computes the closed loop dislocation network.
******************************************************************************/
void LoopGenerator::compute()
{
	assert(_nodes.empty());
	assert(_segments.empty());

	if(inputNetwork().segments().empty())
		return;

	buildInputNodesAndSegments();

	writeDislocationsVTKFile("temp.dislocations.vtk");
	inputNetwork().writeCellVTKFile("temp.cell.vtk");

	buildTessellation();
	writeDislocationsVTKFile("temp.subdivided_dislocations.vtk");

	// Verify dislocation network.
	for(Node* node : nodes()) {
		for(Segment* s = node->segments; s != nullptr; s = s->nextNodeSegment) {
			assert(s->reverseSegment->reverseSegment == s);
			assert(s->reverseSegment->node2 == node);
			assert(s->node2 != node);
		}
	}

	assignJumpVectors();
	writeCutFacesVTKFile("temp.cutfaces.vtk");
}

/******************************************************************************
* Builds the internal representation of the input dislocation network.
******************************************************************************/
void LoopGenerator::buildInputNodesAndSegments()
{
	std::map<DislocationNode*, Node*> nodeMap;

	for(DislocationSegment* inputSegment : inputNetwork().segments()) {
		// Create nodes for the two end points of the dislocation line.
		Node* endNodes[2];
		for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
			DislocationNode* headNode = inputSegment->nodes[nodeIndex];
			for(DislocationNode* node = headNode->junctionRing; node != inputSegment->nodes[nodeIndex]; node = node->junctionRing) {
				if(node->segment->id < headNode->segment->id || (node->segment == headNode->segment && node->isForwardNode()))
					headNode = node;
			}
			auto entry = nodeMap.find(headNode);
			if(entry == nodeMap.end()) {
				Node* newNode = createNode(headNode->position());
				entry = nodeMap.insert(std::make_pair(headNode, newNode)).first;
			}
			endNodes[nodeIndex] = entry->second;
		}

		// Create segments and intermediate nodes.
		Node* node1 = endNodes[0];
		if(inputSegment->line.size() > 2) {
			for(auto p = inputSegment->line.cbegin() + 1; p != inputSegment->line.cend() - 1; ++p) {
				Node* node2 = createNode(*p);
				createSegmentPair(node1, node2, inputSegment->burgersVector);
				node1 = node2;
			}
		}
		createSegmentPair(node1, endNodes[1], inputSegment->burgersVector);
	}

	std::cerr << "Number of nodes: " << _nodes.size() << std::endl;
	std::cerr << "Number of segments: " << _segments.size() << std::endl;
}

/******************************************************************************
* Computes the Delaunay tetrahedrilazation.
******************************************************************************/
void LoopGenerator::buildTessellation()
{
	// Build list of input points for Delaunay tessellation.
	std::vector<std::pair<DT::Point,Node*>> points;
	points.reserve(nodes().size());
	for(Node* node : nodes())
		points.push_back(std::make_pair(DT::Point(node->pos.x(), node->pos.y(), node->pos.z()), node));

	// Compute Delaunay.
	_dt.insert(std::begin(points), std::end(points));
	std::cerr << "Number of tetrahedra: " << _dt.number_of_vertices() << std::endl;

	// Link dislocation nodes with tessellation vertices.
	for(auto v = _dt.finite_vertices_begin(); v != _dt.finite_vertices_end(); ++v) {
		v->info()->vertex = v;
	}

	writeTessellationVTKFile("temp.tessellation.vtk");

	// Make sure there is a Delaunay edge for every input dislocation segment.
	bool done;
	do {
		done = true;
		for(Segment* segment : segments()) {
			VertexHandle vertex1 = segment->node1()->vertex;
			VertexHandle vertex2 = segment->node2->vertex;
			CellHandle c;
			int i, j;
			if(!_dt.is_edge(vertex1, vertex2, c, i, j)) {
				// Subdivide segment.
				Vector3 delta = inputNetwork().cell().wrapVector(segment->node2->pos - segment->node1()->pos);
				Node* newNode = createNode(segment->node2->pos - 0.5*delta);
				std::cerr << "Inserting extra node at " << newNode->pos << std::endl;
				createSegmentPair(newNode, segment->node2, segment->burgersVector);
				if(segment->node2->segments == segment->reverseSegment) {
					segment->node2->segments = segment->node2->segments->nextNodeSegment;
				}
				else {
					for(Segment* s = segment->node2->segments; s != nullptr; s = s->nextNodeSegment) {
						if(s->nextNodeSegment == segment->reverseSegment) {
							s->nextNodeSegment = s->nextNodeSegment->nextNodeSegment;
							break;
						}
						assert(s->nextNodeSegment != nullptr);
					}
				}
				segment->reverseSegment->nextNodeSegment = newNode->segments;
				newNode->segments = segment->reverseSegment;
				segment->node2 = newNode;
				VertexHandle newVertex = _dt.insert(DT::Point(newNode->pos.x(), newNode->pos.y(), newNode->pos.z()), vertex1);
				newVertex->info() = newNode;
				newNode->vertex = newVertex;
				done = false;
				break;
			}
		}
	}
	while(!done);

	writeTessellationVTKFile("temp.subdivided_tessellation.vtk");
}

/******************************************************************************
* Computes the jump vectors of tessellation faces.
******************************************************************************/
void LoopGenerator::assignJumpVectors()
{
	size_t nedges = _dt.number_of_finite_edges();
	std::cerr << "Number of edges: " << nedges << std::endl;

	std::map<DT::Facet, int> facetMap;
	for(DT::Finite_facets_iterator facet = _dt.finite_facets_begin(); facet != _dt.finite_facets_end(); ++facet) {
		facetMap.insert(std::make_pair(*facet, (int)facetMap.size()));
	}

	size_t nfacets = facetMap.size();
	std::cerr << "Number of facets: " << nfacets << std::endl;

	Eigen::VectorXd bvec = Eigen::VectorXd::Zero(nedges * 3);
	Eigen::MatrixXd amat = Eigen::MatrixXd::Zero(nedges * 3, nfacets * 3);

	size_t edgeIndex = 0;
	for(DT::Finite_edges_iterator edge = _dt.finite_edges_begin(); edge != _dt.finite_edges_end(); ++edge) {
		DT::Vertex_handle vertex1 = edge->first->vertex(edge->second);
		DT::Vertex_handle vertex2 = edge->first->vertex(edge->third);

		// Look up Burgers vector.
		Vector3 b = Vector3::Zero();
		for(Segment* segment = vertex1->info()->segments; segment != nullptr; segment = segment->nextNodeSegment) {
			if(segment->node2 == vertex2->info()) {
				b = segment->burgersVector.localVec();
				break;
			}
		}

		bvec(edgeIndex * 3 + 0) = -b.x();
		bvec(edgeIndex * 3 + 1) = -b.y();
		bvec(edgeIndex * 3 + 2) = -b.z();

		FacetCirculator facetStart = _dt.incident_facets(*edge);
		FacetCirculator facet = facetStart;
		do {
			if(!_dt.is_infinite(*facet)) {
				auto entry = facetMap.find(*facet);
				double coefficient;
				if(entry == facetMap.end()) {
					entry = facetMap.find(_dt.tds().mirror_facet(*facet));
					coefficient = -1;
				}
				else {
					coefficient = 1;
				}
				assert(entry != facetMap.end());

				int facetIndex = entry->second;
				amat(edgeIndex * 3 + 0, facetIndex * 3 + 0) = coefficient;
				amat(edgeIndex * 3 + 1, facetIndex * 3 + 1) = coefficient;
				amat(edgeIndex * 3 + 2, facetIndex * 3 + 2) = coefficient;
			}
			++facet;
		}
		while(facet != facetStart);

		edgeIndex++;
	}

	//std::cerr << bvec << std::endl;
	//std::cerr << amat << std::endl;

	Eigen::FullPivLU<Eigen::MatrixXd> lu_decomp(amat);
	Eigen::VectorXd jvec = lu_decomp.solve(bvec);
	std::cerr << jvec << std::endl;

	double relative_error = (amat*jvec - bvec).norm() / bvec.norm();
	std::cerr << "The relative error is: " << relative_error << std::endl;
	std::cerr << "The rank of A is " << lu_decomp.rank() << std::endl;

	// Assign jump vectors to facets.
	for(DT::Finite_facets_iterator facet = _dt.finite_facets_begin(); facet != _dt.finite_facets_end(); ++facet) {
		int facetIndex = facetMap[*facet];
		Vector3 jv(jvec(facetIndex * 3 + 0), jvec(facetIndex * 3 + 1), jvec(facetIndex * 3 + 2));

		facet->first->info().visitedFacets[facet->second] = true;
		facet->first->info().jumpVecs[facet->second] = jv;
		DT::Facet mirrorFacet = _dt.tds().mirror_facet(*facet);
		mirrorFacet.first->info().visitedFacets[mirrorFacet.second] = true;
		mirrorFacet.first->info().jumpVecs[mirrorFacet.second] = -jv;
	}
}

/******************************************************************************
* Computes the solid angle formed by a triangle as seen from a point p.
******************************************************************************/
static double solidAngleTriangle(const Vector3& s1, const Vector3& s2, const Vector3& s3)
{
    double RR1 = s1.length();
    double RR2 = s2.length();
    double RR3 = s3.length();
    double numer = s1.x()*s2.y()*s3.z() + s1.y()*s2.z()*s3.x() + s1.z()*s2.x()*s3.y()
           -s1.x()*s2.z()*s3.y() - s1.y()*s2.x()*s3.z() - s1.z()*s2.y()*s3.x();
    double dotR1R2 = s1.dot(s2);
    double dotR2R3 = s2.dot(s3);
    double dotR3R1 = s3.dot(s1);
    double denom = RR1*RR2*RR3 + dotR1R2*RR3 + dotR2R3*RR1 + dotR3R1*RR2;
    return -2.0 * atan2(numer, denom);
}

static Vector3 get_f(const Vector3& A, const Vector3& B, const Vector3& burgers)
{
    Vector3 t_AB = (B - A).normalized();
    double norm_A = A.length();
    double norm_B = B.length();
    Vector3 lambda_A = A / norm_A;
    Vector3 lambda_B = B / norm_B;
    double coeff_f = log(norm_B / norm_A * (1.0 + lambda_B.dot(t_AB)) / (1.0 + lambda_A.dot(t_AB)));
    return burgers.cross(t_AB) * coeff_f;
}

static inline Vector3 get_g(const Vector3& A, const Vector3& B, const Vector3& burgers)
{
    double norm_A = A.length();
    double norm_B = B.length();
    Vector3 lambda_A = A / norm_A;
    Vector3 lambda_B = B / norm_B;
    Vector3 cross_AB = lambda_A.cross(lambda_B);
    double coeff_g = burgers.dot(cross_AB) / (1.0 + lambda_A.dot(lambda_B));
    return (lambda_A + lambda_B) * coeff_g;
}

/******************************************************************************
* Computes the displacements vector at the given position.
******************************************************************************/
Vector3 LoopGenerator::computeDisplacement(const Point3& p0)
{
	const double nu = 0.0;

	Vector3 u = Vector3::Zero();
	for(DT::Finite_facets_iterator facet = _dt.finite_facets_begin(); facet != _dt.finite_facets_end(); ++facet) {
		const Vector3& b = facet->first->info().jumpVecs[facet->second];
		if(b.isZero()) continue;

		// Get the three vertices of the triangle.
		Point3 vertices[3];
		for(int v = 0; v < 3; v++) {
			DT::Vertex_handle vh = facet->first->vertex(CGAL::Triangulation_utils_3::vertex_triple_index(facet->second, v));
			vertices[v].x() = vh->point().x();
			vertices[v].y() = vh->point().y();
			vertices[v].z() = vh->point().z();
		}

	    Vector3 s1 = vertices[0] - p0;
	    Vector3 s2 = vertices[1] - p0;
	    Vector3 s3 = vertices[2] - p0;

		double omega = solidAngleTriangle(s1, s2, s3);

		Vector3 f = get_f(s1,s2,b) + get_f(s2,s3,b) + get_f(s3,s1,b);
		Vector3 g = get_g(s1,s2,b) + get_g(s2,s3,b) + get_g(s3,s1,b);
		Vector3 fvec = f * ((1.0 - 2.0 * nu) / (8.0 * FLOATTYPE_PI * (1.0 - nu)));
		Vector3 gvec = g / (8.0 * FLOATTYPE_PI * (1.0 - nu));
		u += b * (omega / 4.0 / FLOATTYPE_PI) - fvec + gvec;
	}

	return u;
}

/******************************************************************************
* Creates a new dislocation node.
******************************************************************************/
LoopGenerator::Node* LoopGenerator::createNode(const Point3& pos)
{
	Node* node = _nodePool.construct();
	node->pos = pos;
	_nodes.push_back(node);
	return node;
}

/******************************************************************************
* Creates a dislocation segment and a reverse segment between two dislocation nodes.
******************************************************************************/
LoopGenerator::Segment* LoopGenerator::createSegmentPair(Node* node1, Node* node2, const ClusterVector& burgersVector)
{
	Segment* segment1 = _segmentPool.construct();
	Segment* segment2 = _segmentPool.construct();
	_segments.push_back(segment1);
	_segments.push_back(segment2);
	segment1->burgersVector = burgersVector;
	segment2->burgersVector = -burgersVector;
	segment1->node2 = node2;
	segment2->node2 = node1;
	segment1->reverseSegment = segment2;
	segment2->reverseSegment = segment1;
	segment1->nextNodeSegment = node1->segments;
	node1->segments = segment1;
	segment2->nextNodeSegment = node2->segments;
	node2->segments = segment2;
	return segment1;
}

/******************************************************************************
* Dumps the dislocation segments to a VTK file for visualization purposes.
******************************************************************************/
void LoopGenerator::writeDislocationsVTKFile(const char* filename) const
{
	std::ofstream stream(filename);
	if(!stream.is_open())
		throw std::runtime_error("Failed to open output stream for writing.");

	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Dislocations\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << nodes().size() << " double\n";
	std::map<Node*,int> nodeMap;
	int index = 0;
	for(Node* node : nodes()) {
		Point3 wp = inputNetwork().cell().wrapPoint(node->pos);
		stream << wp.x() << " " << wp.y() << " " << wp.z() << "\n";
		nodeMap.insert(std::make_pair(node, index++));
	}

	size_t numCells = segments().size() / 2;
	stream << "\nCELLS " << numCells << " " << (numCells * 3) << "\n";
	for(auto segment = segments().begin(); segment != segments().end(); segment += 2) {
		stream << "2 " << nodeMap[(*segment)->node2] << " " << nodeMap[(*segment)->node1()] << "\n";
	}

	stream << "\nCELL_TYPES " << numCells << "\n";
	for(size_t i = 0; i < numCells; i++)
		stream << "3\n";	// Line

	stream << "\nCELL_DATA " << numCells << "\n";

	stream << "\nVECTORS burgers_vector double\n";
	for(auto segment = segments().begin(); segment != segments().end(); segment += 2) {
		const Vector3& b = (*segment)->burgersVector.localVec();
		stream << b.x() << " " << b.y() << " " << b.z() << "\n";
	}
}

/******************************************************************************
* Writes the tessellation to a VTK file for visualization.
******************************************************************************/
void LoopGenerator::writeTessellationVTKFile(const char* filename) const
{
	std::ofstream stream(filename);
	if(!stream.is_open())
		throw std::runtime_error("Failed to open output stream for writing.");

	// Write VTK output file.
	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Tessellation\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << _dt.number_of_vertices() << " double\n";
	std::map<VertexHandle,int> outputVertices;
	for(auto v = _dt.finite_vertices_begin(); v != _dt.finite_vertices_end(); ++v) {
		outputVertices.insert(std::make_pair((VertexHandle)v, (int)outputVertices.size()));
		stream << v->point().x() << " " << v->point().y() << " " << v->point().z() << "\n";
	}

	int numCells = _dt.number_of_finite_cells();
	stream << "\nCELLS " << numCells << " " << (numCells * 5) << "\n";
	for(auto cell = _dt.finite_cells_begin(); cell != _dt.finite_cells_end(); ++cell) {
		stream << "4";
		for(int i = 0; i < 4; i++)
			stream << " " << outputVertices[cell->vertex(i)];
		stream << "\n";
	}
	stream << "\nCELL_TYPES " << numCells << "\n";
	for(size_t i = 0; i < numCells; i++)
		stream << "10\n";
}

/******************************************************************************
* Writes the cut faces of the tessellation to a VTK file for visualization.
******************************************************************************/
void LoopGenerator::writeCutFacesVTKFile(const char* filename) const
{
	std::ofstream stream(filename);
	if(!stream.is_open())
		throw std::runtime_error("Failed to open output stream for writing.");

	// Write VTK output file.
	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Cut faces\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << _dt.number_of_vertices() << " double\n";
	std::map<VertexHandle,int> outputVertices;
	for(auto v = _dt.finite_vertices_begin(); v != _dt.finite_vertices_end(); ++v) {
		outputVertices.insert(std::make_pair((VertexHandle)v, (int)outputVertices.size()));
		stream << v->point().x() << " " << v->point().y() << " " << v->point().z() << "\n";
	}

	int numFacets = 0;
	for(auto cell = _dt.finite_cells_begin(); cell != _dt.finite_cells_end(); ++cell) {
		for(int f = 0; f < 4; f++) {
			if(!cell->info().jumpVecs[f].isZero()) {
				numFacets++;
			}
		}
	}

	stream << "\nCELLS " << numFacets << " " << (numFacets * 4) << "\n";
	for(auto cell = _dt.finite_cells_begin(); cell != _dt.finite_cells_end(); ++cell) {
		for(int f = 0; f < 4; f++) {
			if(!cell->info().jumpVecs[f].isZero()) {
				stream << "3";
				for(int i = 0; i < 3; i++)
					stream << " " << outputVertices[cell->vertex(CGAL::Triangulation_utils_3::vertex_triple_index(f, i))];
				stream << "\n";
			}
		}
	}
	stream << "\nCELL_TYPES " << numFacets << "\n";
	for(size_t i = 0; i < numFacets; i++)
		stream << "5\n";

	stream << "\nCELL_DATA " << numFacets << "\n";

	stream << "\nVECTORS jump_vector double\n";
	for(auto cell = _dt.finite_cells_begin(); cell != _dt.finite_cells_end(); ++cell) {
		for(int f = 0; f < 4; f++) {
			const Vector3& jv = cell->info().jumpVecs[f];
			if(!jv.isZero()) {
				stream << jv.x() << " " << jv.y() << " " << jv.z() << "\n";
			}
		}
	}

	stream << "\nSCALARS jump_vector_x double\n";
	stream << "\nLOOKUP_TABLE default\n";
	for(auto cell = _dt.finite_cells_begin(); cell != _dt.finite_cells_end(); ++cell) {
		for(int f = 0; f < 4; f++) {
			const Vector3& jv = cell->info().jumpVecs[f];
			if(!jv.isZero()) {
				stream << std::abs(jv.dot(Vector3(-1.0, 0.5, 0))) << "\n";
			}
		}
	}
}
