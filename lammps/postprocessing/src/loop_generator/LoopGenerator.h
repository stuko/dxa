#ifndef __LOOP_GENERATOR_H
#define __LOOP_GENERATOR_H

#include <vector>

#include "../dislocation_network/DislocationNetwork.h"
#include "../crystal_cluster/ClusterVector.h"
#include "../utilities/MemoryPool.h"

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Delaunay_triangulation_3.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Triangulation_cell_base_with_info_3.h>

class LoopGenerator
{
public:

	struct Segment;
	struct Node;

	struct CellInfo
	{
		/// The jump vectors assigned to the four cell faces.
		std::array<Vector3,4> jumpVecs = std::array<Vector3,4>{ Vector3::Zero(), Vector3::Zero(), Vector3::Zero(), Vector3::Zero() };

		/// Visit flags for recursive algorithm.
		std::array<bool,4> visitedFacets = std::array<bool,4>{ false, false, false, false };
	};

	typedef CGAL::Exact_predicates_inexact_constructions_kernel 		Kernel;
	typedef CGAL::Triangulation_cell_base_with_info_3<CellInfo,Kernel> 	CbDS;
	typedef CGAL::Triangulation_vertex_base_with_info_3<Node*,Kernel>	VbDS;

	// Define data types for Delaunay triangulation class.
	typedef Kernel 													GT;
	typedef CGAL::Triangulation_data_structure_3<VbDS, CbDS> 		TDS;
	typedef CGAL::Delaunay_triangulation_3<GT, TDS>					DT;

	// Often-used iterator types.
	typedef DT::Triangulation_data_structure::Cell_iterator 		CellIterator;
	typedef DT::Triangulation_data_structure::Vertex_iterator 		VertexIterator;

	typedef DT::Triangulation_data_structure::Vertex_handle 		VertexHandle;
	typedef DT::Triangulation_data_structure::Cell_handle 			CellHandle;

	typedef DT::Triangulation_data_structure::Facet_circulator		FacetCirculator;

public:

	struct Node
	{
		Point3 pos;
		Segment* segments = nullptr;
		VertexHandle vertex;
	};

	struct Segment
	{
		Node* node2;
		Node* node1() const { return reverseSegment->node2; }
		ClusterVector burgersVector = Vector3::Zero();
		Segment* nextNodeSegment;
		Segment* reverseSegment;
	};

public:

	/// Constructor.
	LoopGenerator(const DislocationNetwork& inputNetwork) : _inputNetwork(inputNetwork) {}

	/// Returns the input dislocation network.
	const DislocationNetwork& inputNetwork() const { return _inputNetwork; }

	/// Returns the output dislocation network.
	DislocationNetwork& outputNetwork() { return _outputNetwork; }

	/// Returns the list of dislocation nodes.
	const std::vector<Node*>& nodes() const { return _nodes; }

	/// Returns the list of dislocation segments.
	const std::vector<Segment*>& segments() const { return _segments; }

	/// Computes the closed loop dislocation network.
	void compute();

	/// Computes the displacements vector at the given position.
	Vector3 computeDisplacement(const Point3& p0);

private:

	/// Builds the internal representation of the input dislocation network.
	void buildInputNodesAndSegments();

	/// Computes the Delaunay tetrahedrilazation.
	void buildTessellation();

	/// Computes the jump vectors of tessellation faces.
	void assignJumpVectors();

	/// Creates a new dislocation node.
	Node* createNode(const Point3& pos);

	/// Creates a dislocation segment and a reverse segment between two dislocation nodes.
	Segment* createSegmentPair(Node* node1, Node* node2, const ClusterVector& burgersVector);

	/// Dumps the dislocation segments to a VTK file for visualization purposes.
	void writeDislocationsVTKFile(const char* filename) const;

	/// Writes the tessellation to a VTK file for visualization.
	void writeTessellationVTKFile(const char* filename) const;

	/// Writes the cut faces of the tessellation to a VTK file for visualization.
	void writeCutFacesVTKFile(const char* filename) const;

private:

	/// The input dislocation network.
	const DislocationNetwork& _inputNetwork;

	/// The output dislocation network.
	DislocationNetwork _outputNetwork;

	// Used for allocating nodes.
	MemoryPool<Node> _nodePool;

	// Used for allocating segments.
	MemoryPool<Segment> _segmentPool;

	/// The list of dislocation nodes.
	std::vector<Node*> _nodes;

	/// The list of dislocation segments.
	std::vector<Segment*> _segments;

	/// The space-filling tessellation.
	DT _dt;
};

#endif // __LOOP_GENERATOR_H
