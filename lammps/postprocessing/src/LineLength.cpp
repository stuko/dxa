#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"
#include "io/CAReader.h"
#include "CodeVersion.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		FloatType linePointInterval = 0;
		int smoothingLevel = 0;

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			if(strcmp(argv[iarg], "--coarsening") == 0 && iarg < argc - 1) {
				linePointInterval = std::atof(argv[iarg+1]);
				if(linePointInterval <= 0) {
					cerr << "Invalid coarsening parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--smoothing") == 0 && iarg < argc - 1) {
				smoothingLevel = std::atoi(argv[iarg+1]);
				if(smoothingLevel <= 0) {
					cerr << "Invalid smoothing level: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else {
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		if(iarg >= argc) {
			cerr << "This program computes dislocation line length and density." << endl << endl;
			cerr << "Version: " << TRACE_TOOL_VERSION << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    line_length [options] input.ca" << endl;
			cerr << endl;
			cerr << "Post-processing options:" << endl;
			cerr << endl;
			cerr << "    --coarsening X             Reduces line point density to X times the interatomic spacing" << endl;
			cerr << "    --smoothing N              Applies N iterations of line smoothing algorithm" << endl;
			cerr << endl;
			return 1;
		}

#ifndef NDEBUG
		cerr << "This is line_length version " << TRACE_TOOL_VERSION << " (DEBUG BUILD)" << endl;
#else
		cerr << "This is line_length version " << TRACE_TOOL_VERSION << endl;
#endif

		DislocationNetwork dislocationNetwork;
		cerr << "Reading CA dislocation network file '" << argv[iarg] << "'" << endl;
		ifstream in_stream(argv[iarg]);
		if(!in_stream.is_open()) {
			cerr << "Failed to open input file for reading: " << argv[iarg] << endl;
			return 1;
		}
		CAReader reader(dislocationNetwork.cell(), dislocationNetwork.clusterGraph());
		reader.readCAFile(in_stream, &dislocationNetwork, nullptr, nullptr);
		cerr << "Number of dislocation lines: " << dislocationNetwork.segments().size() << endl;

		if(linePointInterval > 0.0 || smoothingLevel >= 1) {
			cerr << "Post-processing dislocation lines (--coarsening " << linePointInterval << " --smoothing " << smoothingLevel << ")" << endl;
			dislocationNetwork.smoothDislocationLines(linePointInterval, smoothingLevel);
		}

		// List of Burgers vector families defined in the CA file.
		std::vector<Vector3> burgersFamilies;
		burgersFamilies.push_back(Vector3(0.5, 0.5, 0.0));	// 1/2<110> (Perfect)
		burgersFamilies.push_back(Vector3(1.0/6.0, 1.0/6.0, 2.0/6.0));  // 1/6<112> (Shockley)
		burgersFamilies.push_back(Vector3(1.0/6.0, 1.0/6.0, 0.0));  // 1/6<110> (Stair-rod)
		std::vector<double> familyLengths(burgersFamilies.size(), 0.0);

		// Compute total line length.
		FloatType totalLineLength = 0;
		for(DislocationSegment* segment : dislocationNetwork.segments()) {
			double len = segment->calculateLength();
			totalLineLength += len;
			for(size_t i = 0; i < burgersFamilies.size(); i++) {
				Vector3 sc1(std::fabs(burgersFamilies[i].x()), std::fabs(burgersFamilies[i].y()), std::fabs(burgersFamilies[i].z()));
				std::sort(sc1.data(), sc1.data() + 3);
				Vector3 sc2(std::fabs(segment->burgersVector.localVec().x()), std::fabs(segment->burgersVector.localVec().y()), std::fabs(segment->burgersVector.localVec().z()));
				std::sort(sc2.data(), sc2.data() + 3);
				if(sc2.equals(sc1, CA_LATTICE_VECTOR_EPSILON)) {
					familyLengths[i] += len;
					break;
				}
			}
		}
		cerr << "Total dislocation line length: " << totalLineLength << " [A]" << endl;
		FloatType dislocationDensity = totalLineLength / dislocationNetwork.cell().volume();
		cerr << "Dislocation density: " << dislocationDensity << " [1/A^2] = " << (dislocationDensity * 1e20) << " [1/m^2]" << endl;

		cout << dislocationNetwork.segments().size() << " " << totalLineLength << " " << dislocationNetwork.cell().volume() << " " <<
				dislocationDensity << " " << (dislocationDensity * 1e20) << " " << reader.simulationTimestep() << " " << dislocationNetwork.cell().matrix()(2,2);
		for(size_t i = 0; i < burgersFamilies.size(); i++) {
			cout << " " << familyLengths[i];
		}
		cout << endl;
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
