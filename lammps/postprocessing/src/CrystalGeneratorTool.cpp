#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"
#include "loop_generator/LoopGenerator.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
			return 1;
		}

		if(iarg >= argc) {
			cerr << "This program generates dislocated crystals." << endl << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    crystal_generator_tool [options] input.ca > output.data" << endl;
			cerr << endl;
			return 1;
		}

		DislocationNetwork dislocationNetwork;
		cerr << "Reading CA dislocation network file '" << argv[iarg] << "'" << endl;
		ifstream in_stream(argv[iarg]);
		if(!in_stream.is_open()) {
			cerr << "Failed to open input file for reading: " << argv[iarg] << endl;
			return 1;
		}
		dislocationNetwork.readCAFile(in_stream);
		cerr << "Number of dislocation lines: " << dislocationNetwork.segments().size() << endl;

		LoopGenerator loopGen(dislocationNetwork);
		loopGen.compute();

		AffineTransformation lattice;
		lattice.column(0) = Vector3(1,0,0);
		lattice.column(1) = Vector3(0,1,0);
		lattice.column(2) = Vector3(0,0,1);
		lattice.column(3) = Vector3(FLOATTYPE_EPSILON,FLOATTYPE_EPSILON,FLOATTYPE_EPSILON);
		std::vector<std::pair<Point3,int>> basis;
		basis.push_back(std::make_pair(Point3(0,0,0), 1));
		basis.push_back(std::make_pair(Point3(0.5,0.5,0.5), 1));

		const SimulationCell& simCell = dislocationNetwork.cell();
		AffineTransformation tm = simCell.matrix() * lattice.inverse();
		Point3 minc(FLOATTYPE_MAX), maxc(FLOATTYPE_MIN);
		for(int z = 0; z <= 1; z++) {
			for(int y = 0; y <= 1; y++) {
				for(int x = 0; x <= 1; x++) {
					Point3 p = tm * Point3(x,y,z);
					for(size_t dim = 0; dim < 3; dim++) {
						minc[dim] = std::min(minc[dim], p[dim]);
						maxc[dim] = std::max(maxc[dim], p[dim]);
					}
				}
			}
		}
		Point3I minci((int)std::floor(minc.x()), (int)std::floor(minc.y()), (int)std::floor(minc.z()));
		Point3I maxci((int)std::ceil(maxc.x()), (int)std::ceil(maxc.y()), (int)std::ceil(maxc.z()));
		std::cerr << minci << " " << maxci << std::endl;

		std::vector<std::pair<Point3,int>> atoms;

		Point3I latticeSite;
		for(latticeSite.z() = minci.z(); latticeSite.z() <= maxci.z(); latticeSite.z()++) {
			for(latticeSite.y() = minci.y(); latticeSite.y() <= maxci.y(); latticeSite.y()++) {
				for(latticeSite.x() = minci.x(); latticeSite.x() <= maxci.x(); latticeSite.x()++) {
					for(const auto& b : basis) {
						Point3 xyz = lattice * Point3(latticeSite.x() + b.first.x(), latticeSite.y() + b.first.y(), latticeSite.z() + b.first.z());
						Point3 reduced = dislocationNetwork.cell().absoluteToReduced(xyz);
						if(reduced.x() < 0.0 || reduced.x() >= 1.0 || reduced.y() < 0.0 || reduced.y() >= 1.0 || reduced.z() < 0.0 || reduced.z() >= 1.0)
							continue;
						xyz += loopGen.computeDisplacement(xyz);
						atoms.push_back(std::make_pair(xyz, b.second));
					}
				}
			}
		}

		std::cerr << "Generated " << atoms.size() << " atoms" << std::endl;

		const Vector3& a = simCell.matrix().column(0);
		const Vector3& b = simCell.matrix().column(1);
		const Vector3& c = simCell.matrix().column(2);
		FloatType xlo = simCell.matrix().translation().x();
		FloatType ylo = simCell.matrix().translation().y();
		FloatType zlo = simCell.matrix().translation().z();
		FloatType xhi = a.x() + xlo;
		FloatType yhi = b.y() + ylo;
		FloatType zhi = c.z() + zlo;
		FloatType xy = b.x();
		FloatType xz = c.x();
		FloatType yz = c.y();

		std::cout << "# LAMMPS data file\n";
		std::cout << atoms.size() << " atoms\n";
		std::cout << "1 atom types\n";
		std::cout << xlo << ' ' << xhi << " xlo xhi\n";
		std::cout << ylo << ' ' << yhi << " ylo yhi\n";
		std::cout << zlo << ' ' << zhi << " zlo zhi\n";
		if(xy != 0 || xz != 0 || yz != 0) {
			std::cout << xy << ' ' << xz << ' ' << yz << " xy xz yz\n";
		}

		std::cout << "\nAtoms\n\n";
		for(size_t index = 0; index < atoms.size(); index++) {
			const Point3& atomPos = atoms[index].first;
			int atomType = atoms[index].second;
			std::cout << (index+1) << " " << atomType << " " << atomPos.x() << " " << atomPos.y() << " " << atomPos.z() << "\n";
		}
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
