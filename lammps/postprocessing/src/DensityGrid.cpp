#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"
#include "io/CAReader.h"
#include "CodeVersion.h"

using namespace std;

struct NetworkGridSegmentation
{
	DislocationNetwork& _network;

	/// Used to convert coordinates from simulation frame to voxel grid coordinates.
	AffineTransformation _absoluteToGrid;

	/// Used to convert coordinates from voxel grid coordinates to the simulation frame.
	AffineTransformation _gridToAbsolute;
		
	/// The unsigned line length in each voxel.
	std::vector<FloatType> _lineLength;

	/// The dislocation loop mass content in each voxel.
	std::vector<FloatType> _loopMassContent;

	/// The Nye tensor in each voxel.
	std::vector<Matrix3> _nyeContent;
	
	/// Dimensions of a single voxel:
	Vector3 _binSize;

	/// Total line length summed over all voxels:
	FloatType _summedLength;

	/// The number of voxels in each spatial direction:
	Vector3I _binCount;

	/// XYZ coordinates of simulation cell origin.
	Point3 _cellOrigin;

	static constexpr FloatType EPS = 1e-12;

	NetworkGridSegmentation(DislocationNetwork& network, size_t nx, size_t ny, size_t nz) : _network(network), _binCount(nx, ny, nz) {
		_lineLength.resize(nx*ny*nz, 0);
		_loopMassContent.resize(nx*ny*nz, 0);
		_nyeContent.resize(nx*ny*nz, Matrix3::Zero());
		_absoluteToGrid = Matrix3(nx,0,0,0,ny,0,0,0,nz) * network.cell().inverseMatrix();
		_gridToAbsolute = network.cell().matrix() * Matrix3(1.0/nx,0,0,0,1.0/ny,0,0,0,1.0/nz);
		_binSize[0] = network.cell().matrix()(0,0) / nx;
		_binSize[1] = network.cell().matrix()(1,1) / ny;
		_binSize[2] = network.cell().matrix()(2,2) / nz;
		_cellOrigin = Point3::Origin() + network.cell().matrix()[3];
		_summedLength = 0;
	}

	// Adds the contributes of a single dislocation segment fully contained in a voxel 
	// to the corresponding bin fields.
	void addVoxelContribution(const Point3I& voxel, const Vector3& ray, FloatType delta_t, const Point3& basePoint, const Vector3& b) {
		// Wrap voxel coordinates at periodic boundaries.
		Point3I wrappedVoxel(
			SimulationCell::modulo(voxel[0], _binCount[0]),
			SimulationCell::modulo(voxel[1], _binCount[1]),
			SimulationCell::modulo(voxel[2], _binCount[2]));
		Point3 wrappedBasePoint = basePoint + _gridToAbsolute * Vector3(wrappedVoxel - voxel);

		size_t voxelIndex = (size_t)wrappedVoxel[0] + (size_t)wrappedVoxel[1] * (size_t)_binCount[0] + (size_t)wrappedVoxel[2] * (size_t)_binCount[0] * (size_t)_binCount[1];

		Vector3 dl = delta_t * ray;
		FloatType len = dl.length();

		_lineLength[voxelIndex] += len;

		Vector3 v1 = wrappedBasePoint - Point3::Origin();
		_loopMassContent[voxelIndex] += 0.5 * b.dot(v1.cross(dl));

		for(size_t i = 0; i < 3; i++)
			for(size_t j = 0; j < 3; j++)
				_nyeContent[voxelIndex](i,j) += b[i]*dl[j];

		_summedLength += len;
	}

	/// Adds the contribution of a dislocation segment from the original line network
	/// to all the voxels it intersects.
	void addSegment(const Point3& p1, const Point3& p2, const Vector3& b) {

		// Algorithm to break down a long dislocation segment that stretches over several voxels into smaller pieces:
		// J. Amanatides, A. Woo. A Fast Voxel Traversal Algorithm for Ray Tracing. Eurographics '87

		Vector3 ray = p2 - p1;
		Point3 p1g = _absoluteToGrid * p1;
		Point3 p2g = _absoluteToGrid * p2;
		Point3I current_voxel((int)floor(p1g.x()), (int)floor(p1g.y()), (int)floor(p1g.z()));
		Point3I last_voxel((int)floor(p2g.x()), (int)floor(p2g.y()), (int)floor(p2g.z()));
		FloatType current_t = 0;
		
		// In which direction the voxel ids are incremented.
		FloatType stepX = (ray[0] >= 0) ? 1 : -1;
		FloatType stepY = (ray[1] >= 0) ? 1 : -1;
		FloatType stepZ = (ray[2] >= 0) ? 1 : -1;
		  
		// Distance along the ray to the next voxel border from the current position (tMaxX, tMaxY, tMaxZ).
		FloatType next_voxel_boundary_x = (current_voxel[0] + stepX) * _binSize[0] + _cellOrigin[0];
		FloatType next_voxel_boundary_y = (current_voxel[1] + stepY) * _binSize[1] + _cellOrigin[1];
		FloatType next_voxel_boundary_z = (current_voxel[2] + stepZ) * _binSize[2] + _cellOrigin[2];
		if(ray[0] < 0) next_voxel_boundary_x += _binSize[0];
		if(ray[1] < 0) next_voxel_boundary_y += _binSize[1];
		if(ray[2] < 0) next_voxel_boundary_z += _binSize[2];

		// tMaxX, tMaxY, tMaxZ -- distance until next intersection with voxel-border
		// the value of t at which the ray crosses the first vertical voxel boundary
		FloatType tMaxX = (ray[0] != 0) ? (next_voxel_boundary_x - p1[0]) / ray[0] : FLOATTYPE_MAX;
		FloatType tMaxY = (ray[1] != 0) ? (next_voxel_boundary_y - p1[1]) / ray[1] : FLOATTYPE_MAX;
		FloatType tMaxZ = (ray[2] != 0) ? (next_voxel_boundary_z - p1[2]) / ray[2] : FLOATTYPE_MAX;

		// tDeltaX, tDeltaY, tDeltaZ --
		// how far along the ray we must move for the horizontal component to equal the width of a voxel
		// the direction in which we traverse the grid
		// can only be FLOATTYPE_MAX if we never go in that direction
		FloatType tDeltaX = (ray[0]!=0) ? _binSize[0] / ray[0] * stepX : FLOATTYPE_MAX;
		FloatType tDeltaY = (ray[1]!=0) ? _binSize[1] / ray[1] * stepY : FLOATTYPE_MAX;
		FloatType tDeltaZ = (ray[2]!=0) ? _binSize[2] / ray[2] * stepZ : FLOATTYPE_MAX;

		while(last_voxel != current_voxel) {
			FloatType new_t;
			Point3I new_voxel = current_voxel;
			if(tMaxX < tMaxY) {
				if(tMaxX < tMaxZ) {
					new_voxel[0] += stepX;
					new_t = tMaxX;
					tMaxX += tDeltaX;
				}
				else {
					new_voxel[2] += stepZ;
					new_t = tMaxZ;
					tMaxZ += tDeltaZ;
				}
			}
			else {
				if(tMaxY < tMaxZ) {
					new_voxel[1] += stepY;
					new_t = tMaxY;
					tMaxY += tDeltaY;
				}
				else {
					new_voxel[2] += stepZ;
					new_t = tMaxZ;
					tMaxZ += tDeltaZ;
				}
			}
			addVoxelContribution(current_voxel, ray, new_t - current_t, p1 + current_t * ray, b);
			current_t = new_t;
			current_voxel = new_voxel;
		}

		assert(current_t <= 1.0+EPS);
		addVoxelContribution(current_voxel, ray, 1.0 - current_t, p1 + current_t * ray, b);
	}

	/// Writes an output file in VTK format with the voxel data.
	void writeOutput(ostream& stream) {
		FloatType binVolume = _binSize[0] * _binSize[1] * _binSize[2];
		stream << "# vtk DataFile Version 3.0\n";
		stream << "# Density grid\n";
		stream << "ASCII\n";
		stream << "DATASET RECTILINEAR_GRID\n";

		stream << "DIMENSIONS " << (_binCount[0]+1) << " " << (_binCount[1]+1) << " " << (_binCount[2]+1) << "\n";		
		char dimName[3] = {'X','Y','Z'};
		for(int dim = 0; dim < 3; dim++) {
			stream << dimName[dim] << "_COORDINATES " << (_binCount[dim]+1) << " double\n";
			for(int i = 0; i <= _binCount[dim]; i++)
				stream << (i * _binSize[dim]) + _cellOrigin[dim] << " ";
			stream << "\n";
		}

		stream << "CELL_DATA " << (_binCount[0] * _binCount[1] * _binCount[2]) << "\n";
		
		stream << "SCALARS density double 1\n";
		stream << "LOOKUP_TABLE default\n";
		for(FloatType d : _lineLength)
			stream << (d / binVolume) << "\n";
				
		stream << "SCALARS density double 1\n";
		stream << "LOOKUP_TABLE default\n";
		for(FloatType d : _lineLength)
			stream << (d / binVolume) << "\n";

		stream << "SCALARS loop_mass_content double 1\n";
		stream << "LOOKUP_TABLE default\n";
		for(FloatType d : _loopMassContent)
			stream << d << "\n";

		stream << "TENSORS nye_tensor double\n";
		for(const Matrix3& nye : _nyeContent) {
			for(size_t i = 0; i < 3; i++) {
				for(size_t j = 0; j < 3; j++) {
					stream << (nye(i, j) / binVolume) << " ";
				}
				stream << "\n";
			}
		}
	}
};

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		FloatType binsize = 0;
		FloatType linePointInterval = 0;
		int smoothingLevel = 0;		

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			if(strcmp(argv[iarg], "--coarsening") == 0 && iarg < argc - 1) {
				linePointInterval = std::atof(argv[iarg+1]);
				if(linePointInterval <= 0) {
					cerr << "Invalid coarsening parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--smoothing") == 0 && iarg < argc - 1) {
				smoothingLevel = std::atoi(argv[iarg+1]);
				if(smoothingLevel <= 0) {
					cerr << "Invalid smoothing level: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else {
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		if(iarg+1 >= argc) {
			cerr << "This program computes various dislocation density measures on a volume grid." << endl << endl;
			cerr << "Version: " << TRACE_TOOL_VERSION << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    density_grid [options...] binsize input.ca > output.vtk" << endl;
			cerr << endl;
			cerr << "Post-processing options:" << endl;
			cerr << endl;
			cerr << "    --coarsening X             Reduces line point density to X times the interatomic spacing" << endl;
			cerr << "    --smoothing N              Applies N iterations of line smoothing algorithm" << endl;
			cerr << endl;
			return 1;
		}

#ifndef NDEBUG
		cerr << "This is density_grid version " << TRACE_TOOL_VERSION << " (DEBUG BUILD)" << endl;
#else
		cerr << "This is density_grid version " << TRACE_TOOL_VERSION << endl;
#endif

		binsize = std::stof(argv[iarg+0]);
		if(binsize <= 0) {
			cerr << "ERROR: Invalid bin size: " << argv[iarg+0] << endl;
			return 1;
		}

		DislocationNetwork dislocationNetwork;
		std::string caFilename = argv[iarg+1];
		cerr << "Reading CA dislocation network file '" << caFilename << "'" << endl;
		ifstream in_stream(caFilename.c_str());
		if(!in_stream.is_open()) {
			cerr << "Failed to open input file for reading: " << caFilename << endl;
			return 1;
		}
		CAReader reader(dislocationNetwork.cell(), dislocationNetwork.clusterGraph());
		reader.readCAFile(in_stream, &dislocationNetwork, nullptr, nullptr);

		// Check if input matches our expectations.
		if(!dislocationNetwork.cell().isAxisAligned()) {
			cerr << "Error: Invalid input. This program can only handle orthogonal simulation cells." << endl;
			return 1;
		}
		// Check if input matches our expectations.
		if(!dislocationNetwork.cell().pbcFlags()[0] || !dislocationNetwork.cell().pbcFlags()[1] || !dislocationNetwork.cell().pbcFlags()[2]) {
			cerr << "Error: Invalid input. This program can only handle simulation cells with 3d periodic boundary conditions." << endl;
			return 1;
		}

		cerr << "Number of dislocation lines: " << dislocationNetwork.segments().size() << endl;

		if(linePointInterval > 0.0 || smoothingLevel >= 1) {
			cerr << "Post-processing dislocation lines (--coarsening " << linePointInterval << " --smoothing " << smoothingLevel << ")" << endl;
			dislocationNetwork.smoothDislocationLines(linePointInterval, smoothingLevel);
		}
		else {
			for(DislocationSegment* segment : dislocationNetwork.segments()) {
				if(!segment->coreSize.empty()) {
					cerr << "Error: " << endl;
					cerr << "The input file contains dislocation lines that have not been post-processed. " << endl;
					cerr << "You probably want to use the --smoothing and --coarsening command line option to " << endl;
					cerr << "post-process the data first before calculating the density measures. " << endl;
					cerr << "The program will quit now. " << endl;
					return 1;
				}
				break;
			}
		}

		// Determine the number of voxels in each direction based on the simulation cell size and the
		// desired bin size.
		int nx = max(1, (int)floor(dislocationNetwork.cell().matrix().column(0).length() / binsize));
		int ny = max(1, (int)floor(dislocationNetwork.cell().matrix().column(1).length() / binsize));
		int nz = max(1, (int)floor(dislocationNetwork.cell().matrix().column(2).length() / binsize));

		cerr << "Selected bin size: " << binsize << endl;
		cerr << "Generated voxel grid with " << nx << " x " << ny << " x " << nz << " bins." << endl;

		FloatType summedLength = 0;
		NetworkGridSegmentation grid(dislocationNetwork, nx, ny, nz);
		for(DislocationSegment* segment : dislocationNetwork.segments()) {
			std::deque<Point3>::const_iterator p1 = segment->line.cbegin();
			std::deque<Point3>::const_iterator p2 = p1;
			++p2;
			for(; p2 != segment->line.cend(); p1 = p2++) {
				grid.addSegment(*p1, *p2, segment->burgersVector.localVec());
				summedLength += (*p2 - *p1).length();
			}
		}
		cerr << "Total line length 1: " << summedLength << endl;
		cerr << "Total line length 2: " << grid._summedLength << endl;
		cerr << "Both should match. Difference is " << (summedLength - grid._summedLength) << endl;

		cerr << "Writing output file" << endl;
		grid.writeOutput(cout);
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
