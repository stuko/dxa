#include <numeric>
#include "DislocationNetwork.h"

/******************************************************************************
* Allocates a new dislocation segment terminated by two nodes.
******************************************************************************/
DislocationSegment* DislocationNetwork::createSegment(const ClusterVector& burgersVector)
{
	DislocationNode* forwardNode  = _nodePool.construct();
	DislocationNode* backwardNode = _nodePool.construct();

	DislocationSegment* segment = _segmentPool.construct(burgersVector, forwardNode, backwardNode);
	segment->id = _segments.size();
	_segments.push_back(segment);

	return segment;
}

/******************************************************************************
* Removes a segment from the list of segments.
******************************************************************************/
void DislocationNetwork::discardSegment(DislocationSegment* segment)
{
	assert(segment != nullptr);
	auto i = std::find(_segments.begin(), _segments.end(), segment);
	assert(i != _segments.end());
	_segments.erase(i);
}

/******************************************************************************
* Computes the location of a point along the segment line.
******************************************************************************/
Point3 DislocationSegment::getPointOnLine(FloatType t) const
{
	if(line.empty())
		return Point3::Origin();

	t *= calculateLength();

	FloatType sum = 0;
	auto i1 = line.begin();
	for(;;) {
		auto i2 = i1 + 1;
		if(i2 == line.end()) break;
		Vector3 delta = *i2 - *i1;
		FloatType len = delta.length();
		if(sum + len >= t && len != 0) {
			return *i1 + (((t - sum) / len) * delta);
		}
		sum += len;
		i1 = i2;
	}

	return line.back();
}

