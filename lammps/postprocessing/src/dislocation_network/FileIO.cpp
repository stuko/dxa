#include <stdexcept>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include "DislocationNetwork.h"
#include "../CodeVersion.h"

/******************************************************************************
* Writes the cell geometry to a file in VTK format for visualization.
******************************************************************************/
void DislocationNetwork::writeCellVTKFile(const char* filename) const
{
	Point3 corners[8];
	corners[0] = cell().matrix() * Point3(0,0,0);
	corners[1] = cell().matrix() * Point3(1,0,0);
	corners[2] = cell().matrix() * Point3(1,1,0);
	corners[3] = cell().matrix() * Point3(0,1,0);
	corners[4] = cell().matrix() * Point3(0,0,1);
	corners[5] = cell().matrix() * Point3(1,0,1);
	corners[6] = cell().matrix() * Point3(1,1,1);
	corners[7] = cell().matrix() * Point3(0,1,1);

	std::ofstream stream(filename);
	if(!stream.is_open())
		throw std::runtime_error("Failed to open output file for writing.");

	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Simulation cell\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS 8 float\n";
	for(int i = 0; i < 8; i++)
		stream << corners[i].x() << " " << corners[i].y() << " " << corners[i].z() << "\n";

	stream << "\nCELLS 1 9\n";
	stream << "8 0 1 2 3 4 5 6 7\n";

	stream << "\nCELL_TYPES 1\n";
	stream << "12\n";  // Hexahedron
}
