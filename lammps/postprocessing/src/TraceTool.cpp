#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include <numeric>
#include "interface_mesh/InterfaceMesh.h"
#include "dislocation_network/DislocationNetwork.h"
#include "crystal_cluster/ClusterGraph.h"
#include "dislocation_tracer/DislocationTracer.h"
#include "dislocation_tracer/BurgersCircuit.h"
#include "io/CAWriter.h"
#include "CodeVersion.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	// Input parameters (can be changed via command line).
	int maxTrialCircuitSize = 14;
	int circuitStretchability = std::numeric_limits<int>::max();
	FloatType linePointInterval = 0;
	int smoothingLevel = 0;
	std::string interfaceMeshOutputFile;
	bool outputDefectMesh = false;

	try {

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			if(strcmp(argv[iarg], "--circuitsize") == 0 && iarg < argc - 1) {
				maxTrialCircuitSize = std::atoi(argv[iarg+1]);
				if(maxTrialCircuitSize <= 3) {
					cerr << "Invalid circuit size: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--stretchability") == 0 && iarg < argc - 1) {
				circuitStretchability = std::atoi(argv[iarg+1]);
				if(circuitStretchability <= 0) {
					cerr << "Invalid stretchability parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--coarsening") == 0 && iarg < argc - 1) {
				linePointInterval = std::atof(argv[iarg+1]);
				if(linePointInterval <= 0) {
					cerr << "Invalid coarsening parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--smoothing") == 0 && iarg < argc - 1) {
				smoothingLevel = std::atoi(argv[iarg+1]);
				if(smoothingLevel <= 0) {
					cerr << "Invalid smoothing level: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--mesh") == 0 && iarg < argc - 1) {
				interfaceMeshOutputFile = argv[iarg+1];
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--defect-mesh") == 0) {
				outputDefectMesh = true;
				iarg += 1;
			}
			else {
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		if(iarg >= argc) {
			cerr << "This helper program traces dislocation lines." << endl << endl;
			cerr << "Version: " << TRACE_TOOL_VERSION << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    trace_tool [options] input.dxa ... > output.ca" << endl;
			cerr << endl;
			cerr << "Tracing options:" << endl;
			cerr << endl;
			cerr << "    --circuitsize N            Sets the max. trial circuit length (default=" << maxTrialCircuitSize << ")" << endl;
			cerr << "    --stretchability N         Sets the circuit stretchability (default=unlimited)" << endl;
			cerr << endl;
			cerr << "Post-processing options:" << endl;
			cerr << endl;
			cerr << "    --coarsening X             Reduces line point density to X times the interatomic spacing" << endl;
			cerr << "    --smoothing N              Applies N iterations of line smoothing algorithm" << endl;
			cerr << endl;
			cerr << "Output options:" << endl;
			cerr << endl;
			cerr << "    --mesh file.vtk            Exports the complete interface mesh to a file for visualization" << endl;
			cerr << "    --defect-mesh              Writes the non-dislocation mesh to the CA output file" << endl;
			cerr << endl;
			return 1;
		}

		InterfaceMesh mesh;

#ifndef NDEBUG
		cerr << "This is trace_tool version " << TRACE_TOOL_VERSION << " (DEBUG BUILD)" << endl;
#else
		cerr << "This is trace_tool version " << TRACE_TOOL_VERSION << endl;
#endif

		for(; iarg < argc; iarg++) {
			cerr << "Reading interface mesh file '" << argv[iarg] << "'" << endl;
			mesh.loadIntermediateFile(argv[iarg]);
		}
		if(!interfaceMeshOutputFile.empty()) {
			mesh.writeVTKFile(interfaceMeshOutputFile);
		}
//		system("heap trace_tool > heap_info.txt");

		cerr << "Linking mesh faces" << endl;
		mesh.linkHalfEdges();

		// Make sure each vertex is only part of a single manifold.
		cerr << "Duplicating shared mesh vertices" << endl;
		mesh.duplicateSharedVertices();

		DislocationNetwork dislocationNetwork(mesh.cell(), mesh.simulationTimestep());

		// Create dummy cluster graph.
		Cluster* defaultCluster = dislocationNetwork.clusterGraph().createCluster(1);
		defaultCluster->structureName = mesh.latticeStructureName();

		// Determine cluster orientation.
		defaultCluster->orientation = mesh.crystalOrientation();

		// Assign cluster transitions to interface mesh edges.
		ClusterTransition* defaultClusterTransition = dislocationNetwork.clusterGraph().createSelfTransition(defaultCluster);

		DislocationTracer tracer(mesh, dislocationNetwork, maxTrialCircuitSize, circuitStretchability);

		cerr << "Tracing dislocation lines (--circuitsize " << maxTrialCircuitSize << " --stretchability " << circuitStretchability << ")" << endl;
		tracer.traceDislocationSegments();

		cerr << "Finishing dislocation network" << endl;
		tracer.finishDislocationSegments(1);

		if(linePointInterval > 0.0 || smoothingLevel >= 1) {
			cerr << "Post-processing dislocation lines (--coarsening " << linePointInterval << " --smoothing " << smoothingLevel << ")" << endl;
			dislocationNetwork.smoothDislocationLines(linePointInterval, smoothingLevel);
		}

		// Compute total line length.
		FloatType totalLineLength = 0;
		for(DislocationSegment* segment : dislocationNetwork.segments())
			totalLineLength += segment->calculateLength();
		cerr << "Total dislocation line length: " << totalLineLength << " [A]" << endl;
		FloatType dislocationDensity = totalLineLength / mesh.cell().volume();
		cerr << "Dislocation density: " << dislocationDensity << " [1/A^2] = " << (dislocationDensity * 1e20) << " [1/m^2]" << endl;

		// Generate the defect mesh.
		HalfEdgeMesh<> defectMesh;
		if(outputDefectMesh) {
			cerr << "Generating defect mesh" << endl;
			mesh.generateDefectMesh(tracer, defectMesh);
		}

		cerr << "Writing output" << endl;
		CAWriter writer(dislocationNetwork.cell(), dislocationNetwork.clusterGraph(), dislocationNetwork.simulationTimestep());
		writer.writeCAFile(cout, &dislocationNetwork, outputDefectMesh ? &defectMesh : nullptr, nullptr);

//		system("heap trace_tool >> heap_info.txt");
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
