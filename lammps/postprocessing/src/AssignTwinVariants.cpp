#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>

#include "io/CAReader.h"
#include "io/CAWriter.h"
#include "interface_mesh/PartitionMesh.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {
		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			{
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		std::vector<Matrix3> symmetryElements;
		for(int s1 = -1; s1 <= 1; s1 += 2) {
			for(int s2 = -1; s2 <= 1; s2 += 2) {
				for(int s3 = -1; s3 <= 1; s3 += 2) {
					Matrix3 m;
					std::array<int,3> p{{0,1,2}};
					do {
						m.column(p[0]) = Vector3(s1,0,0);
						m.column(p[1]) = Vector3(0,s2,0);
						m.column(p[2]) = Vector3(0,0,s3);
						symmetryElements.push_back(m);
					}
					while(std::next_permutation(p.begin(), p.end()));
				}
			}
		}

		std::vector<std::pair<Matrix3,Vector3>> twinVariants;
		twinVariants.emplace_back(Matrix3::Identity(), Vector3(1,1,1));

//		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,1),FLOATTYPE_PI/3), Vector3(255.0/255.0,41.0/255.0,41.0/255.0));
//		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,-1,1),FLOATTYPE_PI/3), Vector3(34.0/255.0,255.0/255.0,223.0/255.0));
//		twinVariants.emplace_back(Matrix3::rotation(Vector3(-1,1,1),FLOATTYPE_PI/3), Vector3(235.0/255.0,17.0/255.0,255.0/255.0));
//		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,-1),FLOATTYPE_PI/3), Vector3(255.0/255.0,188.0/255.0,41.0/255.0));

		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,1),FLOATTYPE_PI/3), Vector3(212.0/255.0,161.0/255.0,144.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,-1,1),FLOATTYPE_PI/3), Vector3(144.0/255.0,195.0/255.0,212.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-1,1,1),FLOATTYPE_PI/3), Vector3(198.0/255.0,174.0/255.0,207.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,-1),FLOATTYPE_PI/3), Vector3(214.0/255.0,196.0/255.0,156.0/255.0));

#if 0
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,2),FLOATTYPE_PI), Vector3(255.0/255.0,41.0/255.0,41.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,-2,1),FLOATTYPE_PI), Vector3(153.0/255.0,218.0/255.0,224.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,2,1),FLOATTYPE_PI), Vector3(71.0/255.0,75.0/255.0,225.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,1,-2),FLOATTYPE_PI), Vector3(104.0/255.0,224.0/255.0,115.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(2,1,1),FLOATTYPE_PI), Vector3(238.0/255.0,250.0/255.0,46.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-2,1,1),FLOATTYPE_PI), Vector3(34.0/255.0,255.0/255.0,223.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-1,1,-2),FLOATTYPE_PI), Vector3(255.0/255.0,158.0/255.0,41.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,-1,-2),FLOATTYPE_PI), Vector3(255.0/255.0,17.0/255.0,235.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(1,-2,-1),FLOATTYPE_PI), Vector3(173.0/255.0,3.0/255.0,240.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-1,-2,1),FLOATTYPE_PI), Vector3(180.0/255.0,78.0/255.0,0.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-2,-1,1),FLOATTYPE_PI), Vector3(162.0/255.0,190.0/255.0,34.0/255.0));
		twinVariants.emplace_back(Matrix3::rotation(Vector3(-2,1,-1),FLOATTYPE_PI), Vector3(0.0/255.0,166.0/255.0,252.0/255.0));
#endif

		for(int i = 1; i <= 4; i++) {
			Matrix3 tm2[4];
			tm2[0] = Matrix3::rotation(Vector3(1,1,1),FLOATTYPE_PI/3) * twinVariants[i].first;
			tm2[1] = Matrix3::rotation(Vector3(-1,1,1),FLOATTYPE_PI/3) * twinVariants[i].first;
			tm2[2] = Matrix3::rotation(Vector3(1,-1,1),FLOATTYPE_PI/3) * twinVariants[i].first;
			tm2[3] = Matrix3::rotation(Vector3(1,1,-1),FLOATTYPE_PI/3) * twinVariants[i].first;
			Vector3 dark_color = FloatType(0.5) * twinVariants[i].second;
			for(int j = 0; j < 4; j++) {
				if(std::none_of(symmetryElements.begin(), symmetryElements.end(), [&tm2,j](const Matrix3& s) {
					return tm2[j].equals(s);
				})) {
					twinVariants.emplace_back(tm2[j], dark_color);
				}
			}
		}

#if 0
		for(int twinVariantIndex = 0; twinVariantIndex < twinVariants.size(); twinVariantIndex++) {
			const auto& twinVariant = twinVariants[twinVariantIndex];
			cerr << "Twin variant " << twinVariantIndex << endl << (3.3*twinVariant.first);
		}
		return 0;
#endif

		std::vector<size_t> atomCounts(twinVariants.size(), 0);

		cerr << "Reading CA file" << endl;
		SimulationCell cell;
		ClusterGraph clusterGraph;
		CAReader reader(cell, clusterGraph);
		PartitionMesh partitionMesh;
		reader.readCAFile(cin, nullptr, nullptr, &partitionMesh);

		for(Cluster* cluster : clusterGraph.clusters()) {
			if(cluster->id == 0) continue;
			FloatType det = cluster->orientation.determinant();
			FloatType scaling = pow(std::abs(det), 1.0/3.0);
			Matrix3 normalizedOrientation = (FloatType(1)/scaling) * cluster->orientation;

			FloatType minDeviation = FLOATTYPE_MAX;
			int bestTwinVariant = -1;
			for(int twinVariantIndex = 0; twinVariantIndex < twinVariants.size(); twinVariantIndex++) {
				const auto& twinVariant = twinVariants[twinVariantIndex];
				FloatType minDeviation2 = FLOATTYPE_MAX;
				for(const Matrix3& symmetryElement : symmetryElements) {
					Matrix3 b = normalizedOrientation - (twinVariant.first * symmetryElement);
					FloatType dev = 0;
					for(size_t i = 0; i < 3; i++)
						for(size_t j = 0; j < 3; j++)
							dev += std::abs(b(i,j));
					if(dev < minDeviation2)
						minDeviation2 = dev;
				}
				if(minDeviation2 < minDeviation) {
					minDeviation = minDeviation2;
					bestTwinVariant = twinVariantIndex;
				}
			}
			cluster->color = twinVariants[bestTwinVariant].second;
			atomCounts[bestTwinVariant] += cluster->atomCount;
		}

		for(int twinVariantIndex = 0; twinVariantIndex < twinVariants.size(); twinVariantIndex++) {
			cerr << "Twin variant " << twinVariantIndex << ": " << atomCounts[twinVariantIndex] << " atoms" << endl;
		}

		cerr << "Writing CA file" << endl;
		CAWriter writer(cell, clusterGraph, reader.simulationTimestep());
		writer.writeCAFile(cout, nullptr, nullptr, &partitionMesh);

	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
