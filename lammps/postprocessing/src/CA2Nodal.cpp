#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"
#include "dislocation_network/DislocationNetwork2.h"
#include "io/CAReader.h"
#include "io/NodalFileWriter.h"
#include "CodeVersion.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		FloatType linePointInterval = 0;
		int smoothingLevel = 0;

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			if(strcmp(argv[iarg], "--coarsening") == 0 && iarg < argc - 1) {
				linePointInterval = std::atof(argv[iarg+1]);
				if(linePointInterval <= 0) {
					cerr << "Invalid coarsening parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--smoothing") == 0 && iarg < argc - 1) {
				smoothingLevel = std::atoi(argv[iarg+1]);
				if(smoothingLevel <= 0) {
					cerr << "Invalid smoothing level: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else {
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		if(iarg >= argc) {
			cerr << "This program converts a CA dislocation line file to the node/segment representation." << endl << endl;
			cerr << "Version: " << TRACE_TOOL_VERSION << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    ca2nodal [options] input.ca > nodalfile.out" << endl;
			cerr << endl;
			cerr << "Post-processing options:" << endl;
			cerr << endl;
			cerr << "    --coarsening X             Reduces line point density to X times the interatomic spacing" << endl;
			cerr << "    --smoothing N              Applies N iterations of line smoothing algorithm" << endl;
			cerr << endl;
			return 1;
		}

#ifndef NDEBUG
		cerr << "This is ca2nodal version " << TRACE_TOOL_VERSION << " (DEBUG BUILD)" << endl;
#else
		cerr << "This is ca2nodal version " << TRACE_TOOL_VERSION << endl;
#endif

		DislocationNetwork inputNetwork;
		cerr << "Reading CA dislocation file '" << argv[iarg] << "'" << endl;
		ifstream in_stream(argv[iarg]);
		if(!in_stream.is_open()) {
			cerr << "Failed to open input file for reading: " << argv[iarg] << endl;
			return 1;
		}
		CAReader reader(inputNetwork.cell(), inputNetwork.clusterGraph());
		reader.readCAFile(in_stream, &inputNetwork, nullptr, nullptr);
		cerr << "Number of input dislocation lines: " << inputNetwork.segments().size() << endl;

		if(linePointInterval > 0.0 || smoothingLevel >= 1) {
			cerr << "Post-processing dislocation lines (--coarsening " << linePointInterval << " --smoothing " << smoothingLevel << ")" << endl;
			inputNetwork.smoothDislocationLines(linePointInterval, smoothingLevel);
		}

		bool isClosed = true;
		for(DislocationSegment* segment : inputNetwork.segments()) {
			for(int nodeIndex = 0; nodeIndex < 2; nodeIndex++) {
				if(segment->nodes[nodeIndex]->isDangling())
					isClosed = false;
			}
			if(!isClosed) break;
		}
		if(!isClosed)
			cerr << "Note: Dislocation network is NOT closed. There are dangling line ends." << endl;

		DislocationNetwork2 outputNetwork;
		outputNetwork.convertFromLineNetwork(inputNetwork);

		cerr << "Number of output nodes: " << outputNetwork.nodes().size() << endl;
		cerr << "Number of output segments: " << outputNetwork.segments().size() << endl;

		NodalFileWriter writer(outputNetwork.cell(), outputNetwork.clusterGraph(), outputNetwork.simulationTimestep());
		writer.writeNodalFile(cout, &outputNetwork);
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
