#ifndef __PARTITION_MESH_H
#define __PARTITION_MESH_H

#include "HalfEdgeMesh.h"

struct PartitionMeshFace;	// defined below

struct PartitionMeshEdge
{
	/// Pointer to the next manifold sharing this edge.
	HalfEdgeMesh<PartitionMeshEdge, PartitionMeshFace, EmptyHalfEdgeMeshStruct>::Edge* nextManifoldEdge = nullptr;
};

struct PartitionMeshFace
{
	/// The face on the opposite side of the manifold.
	HalfEdgeMesh<PartitionMeshEdge, PartitionMeshFace, EmptyHalfEdgeMeshStruct>::Face* oppositeFace = nullptr;

	/// The region to which this face belongs.
	int region;
};

using PartitionMesh = HalfEdgeMesh<PartitionMeshEdge, PartitionMeshFace, EmptyHalfEdgeMeshStruct>;

#endif // __PARTITION_MESH_H
