#ifndef __HALF_EDGE_MESH_H
#define __HALF_EDGE_MESH_H

#include <cassert>
#include <vector>
#include <algorithm>

#include "../math/Point3.h"
#include "../utilities/MemoryPool.h"
#include "../dislocation_network/SimulationCell.h"

// An empty structure that is used as default base class for edges, faces, and vertices by the HaldEdgeMesh class template below.
struct EmptyHalfEdgeMeshStruct {};

/**
 * Stores a closed polygonal mesh as a half-edge data structure.
 *
 * Each half-edge is adjacent to one face.
 * Each half-edge has a pointer to the next half-edge adjacent to the same face.
 * Each half-edge has a pointer to its opposite half-edge.
 * Each half-edge has a pointer to to the vertex it points to.
 * Each half-edge has a pointer to the next edge in the linked list of edges originating from the same vertex.
 * Each vertex has a pointer to the first edge originating from it.
 * Each face has a pointer to one of the edges adjacent to it.
 */
template<class EdgeBase = EmptyHalfEdgeMeshStruct, class FaceBase = EmptyHalfEdgeMeshStruct, class VertexBase = EmptyHalfEdgeMeshStruct>
class HalfEdgeMesh
{
public:

	class Vertex;
	class Face;

	/// A single half-edge.
	class Edge : public EdgeBase
	{
	public:

		/// Returns the vertex this half-edge is coming from.
		Vertex* vertex1() const { return _prevFaceEdge->_vertex2; }

		/// Returns the vertex this half-edge is pointing to.
		Vertex* vertex2() const { return _vertex2; }

		/// Returns a pointer to the face that is adjacent to this half-edge.
		Face* face() const { return _face; }

		/// Returns the next half-edge in the linked-list of half-edges that
		/// leave the same vertex as this edge.
		Edge* nextVertexEdge() const { return _nextVertexEdge; }

		/// Returns the next half-edge in the linked-list of half-edges adjacent to the
		/// same face as this edge.
		Edge* nextFaceEdge() const { return _nextFaceEdge; }

		/// Returns the previous half-edge in the linked-list of half-edges adjacent to the
		/// same face as this edge.
		Edge* prevFaceEdge() const { return _prevFaceEdge; }

		/// Returns a pointer to this edge's opposite half-edge.
		Edge* oppositeEdge() const { return _oppositeEdge; }

		/// Links two opposite half-edges.
		void linkToOppositeEdge(Edge* oppositeEdge) {
			assert(_oppositeEdge == nullptr || _oppositeEdge == oppositeEdge);
			assert(oppositeEdge->_oppositeEdge == nullptr || oppositeEdge->_oppositeEdge == this);
			assert(vertex1() == oppositeEdge->vertex2());
			assert(vertex2() == oppositeEdge->vertex1());
			_oppositeEdge = oppositeEdge;
			oppositeEdge->_oppositeEdge = this;
		}

		/// Unlinks this edge from its opposite edge.
		Edge* unlinkFromOppositeEdge() {
			assert(_oppositeEdge != nullptr);
			assert(_oppositeEdge->_oppositeEdge == this);
			Edge* oe = _oppositeEdge;
			_oppositeEdge = nullptr;
			oe->_oppositeEdge = nullptr;
			return oe;
		}

		/// Computes the (unwrapped) vector between the two vertices of the edge.
		Vector3 physicalVector(const SimulationCell& cell) const {
			return cell.wrapVector(vertex2()->pos() - vertex1()->pos());
		}

	protected:

		/// Constructor.
		Edge(Vertex* vertex2, Face* face) : _oppositeEdge(nullptr), _vertex2(vertex2), _face(face) {}

		/// The opposite half-edge.
		Edge* _oppositeEdge;

		/// The vertex this half-edge is pointing to.
		Vertex* _vertex2;

		/// The face adjacent to this half-edge.
		Face* _face;

		/// The next half-edge in the linked-list of half-edges of the source vertex.
		Edge* _nextVertexEdge;

		/// The next half-edge in the linked-list of half-edges adjacent to the face.
		Edge* _nextFaceEdge;

		/// The previous half-edge in the linked-list of half-edges adjacent to the face.
		Edge* _prevFaceEdge;

		friend class HalfEdgeMesh;
	};

	/// A vertex of a mesh.
	class Vertex : public VertexBase
	{
	public:

		/// Returns the head of the vertex' linked-list of outgoing half-edges.
		Edge* edges() const { return _edges; }

		/// Returns the coordinates of the vertex.
		const Point3& pos() const { return _pos; }

		/// Returns the coordinates of the vertex.
		Point3& pos() { return _pos; }

		/// Sets the coordinates of the vertex.
		void setPos(const Point3& p) { _pos = p; }

		/// Returns the index of the vertex in the list of vertices of the mesh.
		int index() const { return _index; }

		/// Returns the number of faces (as well as half-edges) adjacent to this vertex.
		int numEdges() const { return _numEdges; }

		/// Returns the number of manifolds this vertex is part of.
		int numManifolds() const {
			int n = 0;
			std::vector<Edge*> visitedEdges;
			for(Edge* startEdge = edges(); startEdge != nullptr; startEdge = startEdge->nextVertexEdge()) {
				if(std::find(visitedEdges.cbegin(), visitedEdges.cend(), startEdge) != visitedEdges.cend()) continue;
				n++;
				Edge* currentEdge = startEdge;
				do {
					assert(currentEdge->vertex1() == this);
					assert(std::find(visitedEdges.cbegin(), visitedEdges.cend(), currentEdge) == visitedEdges.cend());
					visitedEdges.push_back(currentEdge);
					currentEdge = currentEdge->prevFaceEdge()->oppositeEdge();
				}
				while(currentEdge != startEdge);
			}
			return n;
		}

		/// Disconnects an edge from this vertex and adds it to the list of edges of another vertex.
		/// Also transfers the opposite edge to the new vertex.
		void transferEdgeToVertex(Edge* edge, Vertex* newVertex) {
			assert(edge->oppositeEdge() != nullptr);
			assert(edge->oppositeEdge()->vertex2() == this);
			this->removeEdge(edge);
			newVertex->addEdge(edge);
			edge->oppositeEdge()->_vertex2 = newVertex;
		}

	protected:

		/// Constructor.
		Vertex(const Point3& pos, int index = -1) : _pos(pos), _edges(nullptr), _numEdges(0),  _index(index) {}

		/// Adds an adjacent half-edge to this vertex.
		void addEdge(Edge* edge) {
			edge->_nextVertexEdge = _edges;
			_edges = edge;
			_numEdges++;
		}

		// Removes a half-edge from this vertex' list of edges.
		void removeEdge(Edge* edge) {
			_numEdges--;
			if(edge == _edges) {
				_edges = edge->nextVertexEdge();
				edge->_nextVertexEdge = nullptr;
			}
			else {
				for(Edge* precedingEdge = edges(); precedingEdge != nullptr; precedingEdge = precedingEdge->nextVertexEdge()) {
					if(precedingEdge->nextVertexEdge() == edge) {
						precedingEdge->_nextVertexEdge = edge->_nextVertexEdge;
						edge->_nextVertexEdge = nullptr;
						return;
					}
				}
				assert(false);
			}
		}

		/// The coordinates of the vertex.
		Point3 _pos;

		/// The number of faces (as well as half-edges) adjacent to this vertex.
		int _numEdges;

		/// The head of the linked-list of outgoing half-edges.
		Edge* _edges;

		/// The index of the vertex in the list of vertices of the mesh.
		int _index;

		friend class HalfEdgeMesh;
	};

	/// A polygonal face of the mesh.
	class Face : public FaceBase
	{
	public:

		/// Returns a pointer to the head of the linked-list of half-edges that are adjacent to this face.
		Edge* edges() const { return _edges; }

		/// Returns the index of the face in the list of face of the mesh.
		int index() const { return _index; }

		/// Returns the bit flags assigned to this face.
		unsigned int flags() const { return _flags; }

		/// Tests if a flag is set for this face.
		bool testFlag(unsigned int flag) const { return (_flags & flag); }

		/// Sets a bit flag for this face.
		void setFlag(unsigned int flag) { _flags |= flag; }

		/// Clears a bit flag of this face.
		void clearFlag(unsigned int flag) { _flags &= ~flag; }

		/// Computes the number of edges (as well as vertices) of this face.
		int edgeCount() const {
			int c = 0;
			OVITO_CHECK_POINTER(edges());
			Edge* e = edges();
			do {
				c++;
				e = e->nextFaceEdge();
			}
			while(e != edges());
			return c;
		}

		/// Returns the edge of this face that connects the given vertices.
		Edge* findEdge(Vertex* v1, Vertex* v2) const {
			Edge* e = edges();
			do {
				if(e->vertex2() == v2 && e->vertex1() == v1) return e;
				e = e->nextFaceEdge();
			}
			while(e != edges());
			return nullptr;
		}

	protected:

		/// Constructor.
		Face(int index = -1) : _edges(nullptr), _index(index), _flags(0) {}

		/// The head of the linked-list of half-edges that are adjacent to this face.
		Edge* _edges;

		/// The index of the face in the list of faces of the mesh.
		int _index;

		/// The bit-wise flags assigned to this face.
		unsigned int _flags;

		friend class HalfEdgeMesh;
	};


public:

	/// Removes all faces, edges, and vertices from this mesh.
	void clear() {
		//_vertices.clear();
		//_faces.clear();
		_vertexPool.clear();
		_edgePool.clear();
		_facePool.clear();
	}

	/// Returns the list of vertices in the mesh.
	//const std::vector<Vertex*>& vertices() const { return _vertices; }

	/// Returns the list of faces in the mesh.
	//const std::vector<Face*>& faces() const { return _faces; }

	/// Returns the number of vertices in this mesh.
	size_t vertexCount() const { return _vertexPool.size(); }

	/// Returns the number of faces in this mesh.
	size_t faceCount() const { return _facePool.size(); }

	/// Returns a pointer to the vertex with the given index.
	Vertex* vertex(size_t index) const {
		assert(index < vertexCount());
		return _vertexPool.element(index);
	}

	/// Returns a pointer to the face with the given index.
	Face* face(size_t index) const {
		assert(index < faceCount());
		return _facePool.element(index);
	}

	/// Reserves memory for the given total number of vertices.
	//void reserveVertices(int vertexCount) { _vertices.reserve(vertexCount); }

	/// Reserves memory for the given total number of faces.
	//void reserveFaces(int faceCount) { _faces.reserve(faceCount); }

	/// Adds a new vertex to the mesh.
	Vertex* createVertex(const Point3& pos) {
		Vertex* vert = _vertexPool.construct(pos, vertexCount());
		//_vertices.push_back(vert);
		return vert;
	}

	/// Creates a new face defined by the given vertices.
	/// Half-edges connecting the vertices are created by this method too.
	Face* createFace(std::initializer_list<Vertex*> vertices) {
		return createFace(vertices.begin(), vertices.end());
	}

	/// Creates a new face defined by the given range of vertices.
	/// Half-edges connecting the vertices are created by this method too.
	template<typename VertexPointerIterator>
	Face* createFace(VertexPointerIterator begin, VertexPointerIterator end) {
		assert(std::distance(begin, end) >= 2);
		Face* face = createFace();

		VertexPointerIterator v1, v2;
		for(v2 = begin, v1 = v2++; v2 != end; v1 = v2++)
			createEdge(*v1, *v2, face);
		createEdge(*v1, *begin, face);
		return face;
	}

	/// Creates a new face without edges. This is for internal use only.
	Face* createFace() {
		Face* face = _facePool.construct(faceCount());
		//_faces.push_back(face);
		return face;
	}

	/// Create a new half-edge. This is for internal use only.
	Edge* createEdge(Vertex* vertex1, Vertex* vertex2, Face* face) {
		Edge* edge = _edgePool.construct(vertex2, face);
		vertex1->addEdge(edge);
		if(face->_edges) {
			edge->_nextFaceEdge = face->_edges;
			edge->_prevFaceEdge = face->_edges->_prevFaceEdge;
			face->_edges->_prevFaceEdge->_nextFaceEdge = edge;
			face->_edges->_prevFaceEdge = edge;
		}
		else {
			edge->_nextFaceEdge = edge;
			edge->_prevFaceEdge = edge;
			face->_edges = edge;
		}
		return edge;
	}

	/// Tries to wire each half-edge of the mesh with its opposite (reverse) half-edge.
	bool connectOppositeHalfedges() {
		bool isClosed = true;
		for(size_t v1idx = 0; v1idx < vertexCount(); v1idx++) {
			Vertex* v1 = vertex(v1idx);
			for(Edge* edge = v1->edges(); edge != nullptr; edge = edge->nextVertexEdge()) {
				if(edge->oppositeEdge() != nullptr) {
					assert(edge->oppositeEdge()->oppositeEdge() == edge);
					continue;		// Edge is already linked to its opposite edge.
				}

				// Search in the edge list of the second vertex for an half-edge that goes back to the first vertex.
				for(Edge* oppositeEdge = edge->vertex2()->edges(); oppositeEdge != nullptr; oppositeEdge = oppositeEdge->nextVertexEdge()) {
					if(oppositeEdge->oppositeEdge() != nullptr) continue;
					if(oppositeEdge->vertex2() == v1) {

						// Link the two half-edges.
						edge->linkToOppositeEdge(oppositeEdge);
						break;
					}
				}

				if(edge->oppositeEdge() == nullptr)
					isClosed = false;
			}
		}
		return isClosed;
	}

	/// Duplicates vertices that are shared by more than one manifold.
	size_t duplicateSharedVertices() {
		size_t numSharedVertices = 0;
		size_t oldVertexCount = vertexCount();
		std::vector<Edge*> visitedEdges;
		for(size_t vertexIndex = 0; vertexIndex < oldVertexCount; vertexIndex++) {
			Vertex* vertex = this->vertex(vertexIndex);
			assert(vertex->numEdges() >= 2);

			// Go in positive direction around vertex, facet by facet.
			Edge* currentEdge = vertex->edges();
			int numManifoldEdges = 0;
			do {
				assert(currentEdge != nullptr && currentEdge->face() != nullptr);
				currentEdge = currentEdge->prevFaceEdge()->oppositeEdge();
				numManifoldEdges++;
			}
			while(currentEdge != vertex->edges());

			if(numManifoldEdges == vertex->numEdges())
				continue;		// Vertex is not part of multiple manifolds.

			visitedEdges.clear();
			currentEdge = vertex->edges();
			do {
				visitedEdges.push_back(currentEdge);
				currentEdge = currentEdge->prevFaceEdge()->oppositeEdge();
			}
			while(currentEdge != vertex->edges());

			int oldEdgeCount = vertex->numEdges();
			int newEdgeCount = visitedEdges.size();

			while(visitedEdges.size() != oldEdgeCount) {

				// Create a second vertex that takes the edges not visited yet.
				Vertex* secondVertex = createVertex(vertex->pos());

				Edge* startEdge;
				for(startEdge = vertex->edges(); startEdge != nullptr; startEdge = startEdge->nextVertexEdge()) {
					if(std::find(visitedEdges.cbegin(), visitedEdges.cend(), startEdge) == visitedEdges.end())
						break;
				}
				assert(startEdge != nullptr);

				currentEdge = startEdge;
				do {
					assert(std::find(visitedEdges.cbegin(), visitedEdges.cend(), currentEdge) == visitedEdges.end());
					visitedEdges.push_back(currentEdge);
					assert(vertex->edges() != currentEdge);
					vertex->transferEdgeToVertex(currentEdge, secondVertex);
					currentEdge = currentEdge->prevFaceEdge()->oppositeEdge();
				}
				while(currentEdge != startEdge);
			}
			assert(vertex->numEdges() == newEdgeCount);

			numSharedVertices++;
		}

		return numSharedVertices;
	}

	/// Clears the given flag for all faces.
	void clearFaceFlag(unsigned int flag) const {
		for(size_t i = 0; i < faceCount(); i++)
			face(i)->clearFlag(flag);
	}

	/// Determines if this mesh is a closed manifold, i.e., every half edge has an opposite half edge.
	bool isClosed() const {
		for(size_t i = 0; i < vertexCount(); i++) {
			Vertex* vertex = this->vertex(i);
			for(Edge* edge = vertex->edges(); edge != nullptr; edge = edge->nextVertexEdge()) {
				assert(edge->face() != nullptr);
				if(edge->oppositeEdge() == nullptr)
					return false;
				assert(edge->oppositeEdge()->oppositeEdge() == edge);
				assert(edge->oppositeEdge()->face() != edge->face());
				assert(edge->nextFaceEdge()->face() == edge->face());
				assert(edge->prevFaceEdge()->face() == edge->face());
			}
		}
		return true;
	}

private:

	/// This derived class is needed to make the protected Vertex constructor accessible.
	class InternalVertex : public Vertex {
	public:
		InternalVertex(const Point3& pos, int index = -1) : Vertex(pos, index) {}
	};

	/// This derived class is needed to make the protected Edge constructor accessible.
	class InternalEdge : public Edge {
	public:
		InternalEdge(Vertex* vertex2, Face* face) : Edge(vertex2, face) {}
	};

	/// This derived class is needed to make the protected Face constructor accessible.
	class InternalFace : public Face {
	public:
		InternalFace(int index = -1) : Face(index) {}
	};

private:

	/// The vertices of the mesh.
	//std::vector<Vertex*> _vertices;
	MemoryPool<InternalVertex> _vertexPool;

	/// The edges of the mesh.
	MemoryPool<InternalEdge> _edgePool;

	/// The faces of the mesh.
	//std::vector<Face*> _faces;
	MemoryPool<InternalFace> _facePool;
};

#endif // __HALF_EDGE_MESH_H
