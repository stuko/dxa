#ifndef __BURGERS_CIRCUIT_H
#define __BURGERS_CIRCUIT_H

#include <vector>
#include <cassert>
#include "../crystal_cluster/ClusterVector.h"
#include "../interface_mesh/InterfaceMesh.h"

struct DislocationNode;		// Defined in DislocationNetwork.h

/**
 * A closed circuit on the interface mesh that consists of a sequence of mesh edges.
 *
 * During line tracing, every DislocationNode is associated with a circuit that
 * marks the beginning/end of the dislocation segment on the interface mesh.
 */
struct BurgersCircuit
{
	/// The first edge in the sequence of mesh edges.
	InterfaceMesh::Edge* firstEdge;

	/// The last edge in the sequence of mesh edges.
	InterfaceMesh::Edge* lastEdge;

	/// Saves the state of the Burgers circuit right after the primary part of dislocation segment has been traced.
	/// If the segment does not merge into a junction, then this tells us where it merges into the
	/// non-dislocation part of the interface mesh.
	std::vector<InterfaceMesh::Edge*> segmentMeshCap;

	/// The dislocation node this circuit belongs to.
	DislocationNode* dislocationNode;

	/// The number of mesh edges the circuit consists of.
	int edgeCount;

	/// Number of points in the segment's line array that are considered preliminary.
	int numPreliminaryPoints;

	/// Flag that indicates that all mesh edges of this Burgers circuit are blocked by other circuits.
	bool isCompletelyBlocked;

	/// Flag that indicates that this end of a segment does not merge into a junction.
	bool isDangling;

	/// Constructor.
	BurgersCircuit() : numPreliminaryPoints(0), isDangling(true) {}

	/// Calculates the Burgers vector of the dislocation enclosed by the circuit by summing up the
	/// ideal vectors of the interface mesh edges that make up the circuit.
	///
	/// Note that this method is for debugging purposes only since the Burgers vector is
	/// already known and stored in the DislocationSegment this circuit belongs to.
	Vector3 calculateBurgersVector(const InterfaceMesh& mesh) const {
		Vector3 b = Vector3::Zero();
		InterfaceMesh::Edge* edge = firstEdge;
		do {
			assert(edge != nullptr);
			b += edge->clusterVector(mesh);
			edge = edge->nextCircuitEdge;
		}
		while(edge != firstEdge);
		return b;
	}

	/// Calculates the center of mass of the circuit.
	Point3 calculateCenter(const SimulationCell& cell) const {
		Vector3 currentPoint = Vector3::Zero();
		Vector3 center = Vector3::Zero();
		InterfaceMesh::Edge* edge = firstEdge;
		do {
			assert(edge != nullptr);
			center += currentPoint;
			currentPoint += edge->physicalVector(cell);
			edge = edge->nextCircuitEdge;
		}
		while(edge != firstEdge);
		return firstEdge->vertex1()->pos() + (center / edgeCount);
	}

	/// Counts the edges that form the circuit.
	/// Note that this function is for debugging purposes only since we already keep track
	/// of the number of edges with the BurgersCircuit::edgeCount variable.
	int countEdges() const {
		int count = 0;
		InterfaceMesh::Edge* edge = firstEdge;
		do {
			assert(edge != NULL);
			count++;
			edge = edge->nextCircuitEdge;
		}
		while(edge != firstEdge);
		return count;
	}

	/// Returns the i-th edge of the circuit.
	InterfaceMesh::Edge* getEdge(int index) const {
		assert(index >= 0);
		InterfaceMesh::Edge* edge = firstEdge;
		for(; index != 0; index--) {
			edge = edge->nextCircuitEdge;
		}
		return edge;
	}

	/// Saves the current state of the circuit.
	void storeCircuit() {
		assert(segmentMeshCap.empty());
		segmentMeshCap.reserve(edgeCount);
		InterfaceMesh::Edge* edge = firstEdge;
		do {
			segmentMeshCap.push_back(edge);
			edge = edge->nextCircuitEdge;
		}
		while(edge != firstEdge);
		assert(segmentMeshCap.size() >= 2);
	}
};

#endif // __BURGERS_CIRCUIT_H
