#ifndef __DISLOCATION_TRACER_H
#define __DISLOCATION_TRACER_H

#include <random>
#include <vector>
#include "../dislocation_network/DislocationNetwork.h"
#include "../interface_mesh/InterfaceMesh.h"
#include "../utilities/MemoryPool.h"
#include "BurgersCircuit.h"

/**
 * This is the central class for dislocation line tracing.
 */
class DislocationTracer
{
public:

	/// Constructor.
	DislocationTracer(InterfaceMesh& mesh, DislocationNetwork& network, int maxTrialCircuitSize, int maxCircuitElongation) :
		_mesh(mesh),
		_clusterGraph(network.clusterGraph()),
		_network(network),
		_unusedCircuit(nullptr),
		_rng(1),
		_maxBurgersCircuitSize(maxTrialCircuitSize),
		_maxExtendedBurgersCircuitSize(maxTrialCircuitSize + maxCircuitElongation)
	{
		if(maxCircuitElongation == std::numeric_limits<int>::max())
			_maxExtendedBurgersCircuitSize = std::numeric_limits<int>::max();
	}

	/// Returns the interface mesh that separates the crystal defects from the perfect regions.
	const InterfaceMesh& mesh() const { return _mesh; }

	/// Returns a reference to the cluster graph.
	ClusterGraph& clusterGraph() { return _clusterGraph; }

	/// Returns the extracted network of dislocation segments.
	DislocationNetwork& network() { return _network; }

	/// Returns a const-reference to the extracted network of dislocation segments.
	const DislocationNetwork& network() const { return _network; }

	/// Returns the simulation cell.
	const SimulationCell& cell() const { return mesh().cell(); }

	/// Performs a dislocation search on the interface mesh by generating
	/// trial Burgers circuits. Identified dislocation segments are converted to
	/// a continuous line representation
	void traceDislocationSegments();

	/// After dislocation segments have been extracted, this method trims
	/// dangling lines and finds the optimal cluster to express each segment's
	/// Burgers vector.
	void finishDislocationSegments(int crystalStructure);

	/// Returns the list of nodes that are not part of a junction.
	const std::vector<DislocationNode*>& danglingNodes() const { return _danglingNodes; }

private:

	BurgersCircuit* allocateCircuit();
	void discardCircuit(BurgersCircuit* circuit);
	void findPrimarySegments(int maxBurgersCircuitSize);
	bool createBurgersCircuit(InterfaceMesh::Edge* edge, int maxBurgersCircuitSize);
	void createAndTraceSegment(const ClusterVector& burgersVector, BurgersCircuit* forwardCircuit, int maxCircuitLength);
	bool intersectsOtherCircuits(BurgersCircuit* circuit);
	BurgersCircuit* buildReverseCircuit(BurgersCircuit* forwardCircuit);
	bool traceSegment(DislocationSegment& segment, DislocationNode& node, int maxCircuitLength, bool isPrimarySegment);
	bool tryRemoveTwoCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2);
	bool tryRemoveThreeCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool tryRemoveOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool trySweepTwoFacets(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool tryInsertOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, bool isPrimarySegment);
	void appendLinePoint(DislocationNode& node);
	void circuitCircuitIntersection(InterfaceMesh::Edge* circuitAEdge1, InterfaceMesh::Edge* circuitAEdge2, InterfaceMesh::Edge* circuitBEdge1, InterfaceMesh::Edge* circuitBEdge2, int& goingOutside, int& goingInside);
	std::pair<size_t, bool> joinSegments(int maxCircuitLength);
	void createSecondarySegment(InterfaceMesh::Edge* firstEdge, BurgersCircuit* outerCircuit, int maxCircuitLength);

	/// Calculates the shift vector that must be subtracted from point B to bring it close to point A such that
	/// the vector (B-A) is not a wrapped vector.
	Vector3 calculateShiftVector(const Point3& a, const Point3& b) const {
		Vector3 d = cell().absoluteToReduced(b - a);
		d.x() = cell().pbcFlags()[0] ? floor(d.x() + FloatType(0.5)) : FloatType(0);
		d.y() = cell().pbcFlags()[1] ? floor(d.y() + FloatType(0.5)) : FloatType(0);
		d.z() = cell().pbcFlags()[2] ? floor(d.z() + FloatType(0.5)) : FloatType(0);
		return cell().reducedToAbsolute(d);
	}

private:

	/// The interface mesh that separates the crystal defects from the perfect regions.
	InterfaceMesh& _mesh;

	/// The extracted network of dislocation segments.
	DislocationNetwork& _network;

	/// The cluster graph.
	ClusterGraph& _clusterGraph;

	/// The maximum length (number of edges) for Burgers circuits during the first tracing phase.
	int _maxBurgersCircuitSize;

	/// The maximum length (number of edges) for Burgers circuits during the second tracing phase.
	int _maxExtendedBurgersCircuitSize;

	// Used to allocate memory for BurgersCircuit instances.
	MemoryPool<BurgersCircuit> _circuitPool;

	/// List of nodes that do not form a junction.
	std::vector<DislocationNode*> _danglingNodes;

	/// Stores a pointer to the last allocated circuit which has been discarded.
	/// It can be re-used on the next allocation request.
	BurgersCircuit* _unusedCircuit;

	/// Used to generate random numbers;
	std::mt19937 _rng;
};

#endif // __DISLOCATION_TRACER_H
