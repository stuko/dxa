#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		FloatType linePointInterval = 0;
		int smoothingLevel = 0;
		std::string dumpOutputFile;

		// Parse command line options.
		int iarg = 1;
		while(iarg < argc && strncmp(argv[iarg], "--", 2) == 0) {
			if(strcmp(argv[iarg], "--coarsening") == 0 && iarg < argc - 1) {
				linePointInterval = std::atof(argv[iarg+1]);
				if(linePointInterval <= 0) {
					cerr << "Invalid coarsening parameter: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--smoothing") == 0 && iarg < argc - 1) {
				smoothingLevel = std::atoi(argv[iarg+1]);
				if(smoothingLevel <= 0) {
					cerr << "Invalid smoothing level: " << argv[iarg+1] << endl;
					return 1;
				}
				iarg += 2;
			}
			else if(strcmp(argv[iarg], "--dump") == 0 && iarg < argc - 1) {
				dumpOutputFile = argv[iarg+1];
				iarg += 2;
			}
			else {
				cerr << "Invalid or unknown command line parameter: " << argv[iarg] << endl;
				return 1;
			}
		}

		if(iarg + 1 >= argc) {
			cerr << "This program computes the local dislocation displacement field." << endl << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    displacement_field [options] input1.ca input2.ca" << endl;
			cerr << endl;
			cerr << "Post-processing options:" << endl;
			cerr << endl;
			cerr << "    --coarsening X             Reduces line point density to X times the interatomic spacing" << endl;
			cerr << "    --smoothing N              Applies N iterations of line smoothing algorithm" << endl;
			cerr << endl;
			return 1;
		}

		DislocationNetwork dislocationNetworks[2];
		for(int i = 0; i < 2; i++) {
			cerr << "Reading CA dislocation network file '" << argv[iarg + i] << "'" << endl;
			ifstream in_stream(argv[iarg + i]);
			if(!in_stream.is_open()) {
				cerr << "Failed to open input file for reading: " << argv[iarg + i] << endl;
				return 1;
			}
			dislocationNetworks[i].readCAFile(in_stream);

			if(linePointInterval > 0.0 || smoothingLevel >= 1) {
				cerr << "Post-processing dislocation lines" << endl;
				dislocationNetworks[i].smoothDislocationLines(linePointInterval, smoothingLevel);
			}
		}

		// Rescale second simulation box to match first snapshot.
		dislocationNetworks[1].applyTransformation(dislocationNetworks[0].cell().matrix() * dislocationNetworks[1].cell().inverseMatrix());

		std::vector<std::pair<Point3,Vector3>> displacementVectors;

		double histogramBinSize = 0.5;
		std::vector<double> histogram;
		double totalLength = 0.0;
		double movingLength = 0.0;
		double movingThreshold = 4.0;

		int counter = 0;
		for(DislocationSegment* segment : dislocationNetworks[0].segments()) {
			if((counter % 100) == 0)
				cerr << (counter * 100 / dislocationNetworks[0].segments().size()) << "%\n" << std::flush;
			counter++;
			for(auto p1 = segment->line.cbegin(), p2 = p1 + 1; p2 != segment->line.cend(); p1 = p2, ++p2) {
				Point3 center((p1->x() + p2->x()) * 0.5, (p1->y() + p2->y()) * 0.5, (p1->z() + p2->z()) * 0.5);
				center = dislocationNetworks[0].cell().wrapPoint(center);
				Vector3 closestVector;
				double closestDistanceSq = FLOATTYPE_MAX;
				DislocationSegment* closestSegment = nullptr;
				for(DislocationSegment* segment2 : dislocationNetworks[1].segments()) {
					for(auto p1b = segment2->line.cbegin(), p2b = p1b + 1; p2b != segment2->line.cend(); p1b = p2b, ++p2b) {
						Vector3 v = (*p2b) - (*p1b);
						double vmag = v.squaredLength();
						Vector3 d = dislocationNetworks[0].cell().wrapVector(center - (*p1b));
						double t = d.dot(v) / vmag;
						Vector3 closestLocalVector;
						if(t <= 0.0) closestLocalVector = d;
						else if(t >= 1.0) closestLocalVector = d - v;
						else closestLocalVector = d - t * v;
						double distSq = closestLocalVector.squaredLength();
						if(distSq < closestDistanceSq) {
							closestDistanceSq = distSq;
							closestVector = closestLocalVector;
							closestSegment = segment2;
						}
					}
				}
				displacementVectors.push_back(std::make_pair(center, -closestVector));

				double segmentLength = (*p2 - *p1).length();
				if(segment->burgersVector.localVec().equals(closestSegment->burgersVector.localVec()) || segment->burgersVector.localVec().equals(-closestSegment->burgersVector.localVec())) {
					int binIndex = (int)(sqrt(closestDistanceSq) / histogramBinSize);
					if(histogram.size() <= binIndex) histogram.resize(binIndex+1, 0.0);
					histogram[binIndex] += segmentLength;
				}

				if(closestDistanceSq > movingThreshold*movingThreshold)
					movingLength += segmentLength;
				totalLength += segmentLength;
			}
		}

		cerr << "Moving fraction: " << (movingLength / totalLength) << endl;

		cerr << "Histogram:\n";
		for(int i = 0; i < histogram.size(); i++) {
			cout << (i * histogramBinSize) << " " << histogram[i] << endl;
		}

		if(!dumpOutputFile.empty()) {
			ofstream stream(dumpOutputFile.c_str());
			if(!stream.is_open()) {
				cerr << "Failed to open output file for reading: " << dumpOutputFile << endl;
				return 1;
			}
			stream << "ITEM: TIMESTEP\n";
			stream << dislocationNetworks[0].simulationTimestep() << "\n";
			const AffineTransformation& cell = dislocationNetworks[0].cell().matrix();
			if(cell(0,1) == 0.0 && cell(0,2) == 0.0 && cell(1,2) == 0.0) {
				stream << "ITEM: BOX BOUNDS";
				for(int i = 0; i < 3; i++)
					if(dislocationNetworks[0].cell().pbcFlags()[i]) stream << " pp"; else stream << " ff";
				stream << "\n";
				for(int i = 0; i < 3; i++)
					stream << cell(i,3) << " " << (cell(i,3) + cell(i,i)) << "\n";
			}
			else {
				stream << "ITEM: BOX BOUNDS xy xz yz";
				for(int i = 0; i < 3; i++)
					if(dislocationNetworks[0].cell().pbcFlags()[i]) stream << " pp"; else stream << " ff";
				stream << "\n";
				double xlo = cell(0,3);
				double ylo = cell(1,3);
				double zlo = cell(2,3);
				double xhi = cell(0,0) + xlo;
				double yhi = cell(1,1) + ylo;
				double zhi = cell(2,2) + zlo;
				double xy = cell(0,1);
				double xz = cell(0,2);
				double yz = cell(1,2);
				xlo = min(xlo, xlo+xy);
				xlo = min(xlo, xlo+xz);
				ylo = min(ylo, ylo+yz);
				xhi = max(xhi, xhi+xy);
				xhi = max(xhi, xhi+xz);
				yhi = max(yhi, yhi+yz);
				stream << xlo << " " << xhi << " " << xy << "\n";
				stream << ylo << " " << yhi << " " << xz << "\n";
				stream << zlo << " " << zhi << " " << yz << "\n";
			}
			stream << "ITEM: NUMBER OF ATOMS\n";
			stream << displacementVectors.size() << "\n";
			stream << "ITEM: ATOMS x y z fx fy fz\n";
			for(auto const& p : displacementVectors) {
				stream << p.first.x() << " " << p.first.y() << " " << p.first.z() << " " << p.second.x() << " " << p.second.y()<< " " << p.second.z() << "\n";
			}
		}
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
