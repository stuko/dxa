#ifndef POLAR_DECOMPOSITION_HPP
#define POLAR_DECOMPOSITION_HPP

#include "FloatType.h"

int polar_decomposition_3x3(FloatType* A, bool right_sided, FloatType* U, FloatType* P);

#endif

