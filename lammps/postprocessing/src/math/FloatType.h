#ifndef __FLOATTYPE_H
#define __FLOATTYPE_H

#include <limits>

#ifdef FLOATTYPE_FLOAT

    /// The default floating-point type used by the program.
    using FloatType = float;

#else

    /// The default floating-point type used by the program.
    using FloatType = double;

#endif

/// A small epsilon, which is used in OVITO to test if a number is (almost) zero.
#define FLOATTYPE_EPSILON	FloatType(1e-6)

/// The maximum value for floating-point variables of type Ovito::FloatType.
#define FLOATTYPE_MAX	(std::numeric_limits<FloatType>::max())

/// The lowest value for floating-point variables of type Ovito::FloatType.
#define FLOATTYPE_MIN	(std::numeric_limits<FloatType>::lowest())

/// The constant PI.
#define FLOATTYPE_PI	FloatType(3.14159265358979323846)

/// The format specifier to be passed to the sscanf() function to parse floating-point numbers of type Ovito::FloatType.
#define FLOATTYPE_SCANF_STRING 		"%lg"

#endif // __FLOATTYPE_H
