#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include "dislocation_network/DislocationNetwork.h"
#include "io/CAReader.h"
#include "CodeVersion.h"

#include "math/polar_decomposition.hpp"

using namespace std;

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
	try {

		FloatType linePointInterval = 0;
		int smoothingLevel = 0;

		// Parse command line options.
		int iarg = 1;

		if(iarg >= argc) {
			cerr << "This program computes the lattice orientation of the largest crystal cluster." << endl << endl;
			cerr << "Version: " << TRACE_TOOL_VERSION << endl;
			cerr << "Usage:" << endl;
			cerr << endl;
			cerr << "    calculate_crystal_orientation input.ca" << endl;
			cerr << endl;
			return 1;
		}

#ifndef NDEBUG
		cerr << "This is program version " << TRACE_TOOL_VERSION << endl;
#endif

		DislocationNetwork dislocationNetwork;
		cerr << "Reading CA dislocation network file '" << argv[iarg] << "'" << endl;
		ifstream in_stream(argv[iarg]);
		if(!in_stream.is_open()) {
			cerr << "Failed to open input file for reading: " << argv[iarg] << endl;
			return 1;
		}
		CAReader reader(dislocationNetwork.cell(), dislocationNetwork.clusterGraph());
		reader.readCAFile(in_stream, &dislocationNetwork, nullptr, nullptr);

		Cluster* largestCluster = nullptr;
		size_t totalAtomCount = 0;
		for(Cluster* cluster : dislocationNetwork.clusterGraph().clusters()) {
			if(!largestCluster || cluster->atomCount > largestCluster->atomCount) {
				largestCluster = cluster;
			}
			totalAtomCount += cluster->atomCount;
		}
		if(!largestCluster) throw runtime_error("There is no cluster at all.");
		cerr << "Found " << dislocationNetwork.clusterGraph().clusters().size() << " crystal clusters with a total number of atoms of " << totalAtomCount << " in CA file." << endl;
		cerr << "Largest crystal cluster consists of " << largestCluster->atomCount << " atoms (" << ((double)largestCluster->atomCount / (double)std::max(totalAtomCount, (size_t)1) * 100) << "%)" << endl;
		cout << "Cluster matrix F:" << endl;
		for(size_t row = 0; row < 3; row++) {
			cout << "  " << largestCluster->orientation(row, 0) << " " << largestCluster->orientation(row, 1) << " " << largestCluster->orientation(row, 2) << endl;
		}
		cout << "Inverse cluster matrix inv(F):" << endl;
		Matrix3 invTM = largestCluster->orientation.inverse();
		for(size_t row = 0; row < 3; row++) {
			cout << "  " << invTM(row, 0) << " " << invTM(row, 1) << " " << invTM(row, 2) << endl;
		}
		double angle = acos(largestCluster->orientation.column(2).normalized().z());
		cout << "[001] misorientation angle: " << (angle * 180 / M_PI) << endl;

		// Perform polar decomposition F=R*U
		Matrix3 R, U;
		polar_decomposition_3x3(largestCluster->orientation.elements(), false, R.elements(), U.elements());
		cout << "Stretch matrix U from polar decomposition F=R*U:" << endl;
		for(size_t row = 0; row < 3; row++) {
			cout << "  " << U(row, 0) << " " << U(row, 1) << " " << U(row, 2) << endl;
		}
		cout << "Rotation matrix R from polar decomposition F=R*U:" << endl;
		for(size_t row = 0; row < 3; row++) {
			cout << "  " << R(row, 0) << " " << R(row, 1) << " " << R(row, 2) << endl;
		}

		// Compute angle from rotation matrix.
		double angle2;
    	Vector3 axis(R(2,1) - R(1,2), R(0,2) - R(2,0), R(1,0) - R(0,1));
    	if(axis == Vector3::Zero()) {
    		angle2 = 0;
    		axis = Vector3(0, 0, 1);
    	}
    	else {
	    	double trace = R(0,0) + R(1,1) + R(2,2) - 1.0;
			double s = axis.length();
    		axis /= s;
    		angle2 = atan2(s, trace);
		}
		cout << "Rotation angle: " << (angle2 * 180 / M_PI) << endl;
		cout << "Rotation axis: " << axis << endl;
	}
	catch(const std::exception& ex) {
		cerr << "ERROR: " << ex.what() << endl;
		return 1;
	}

	return 0;
}
