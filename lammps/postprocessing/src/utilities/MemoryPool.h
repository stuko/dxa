#ifndef __MEMORY_POOL_H
#define __MEMORY_POOL_H

#include <cstddef>
#include <vector>

/**
 * \brief A simple memory pool for the efficient allocation of a large number of object instances.
 *
 * \tparam T The type of object to be allocated by the memory pool.
 *
 * New object instances can be dynamically allocated via #construct().
 *
 * A restriction is that all instances belonging to the pool can only be destroyed at once using the
 * #clear() method. This memory pool provides no way to release individual instances.
 *
 */
template<typename T>
class MemoryPool
{
public:

	/// Constructs a new memory pool.
	/// \param pageSize Controls the number of objects per memory page allocated by this pool.
	MemoryPool(size_t pageSize = 1024) : _lastPageNumber(pageSize), _pageSize(pageSize), _size(0) {}

	/// Do not allow copying.
	MemoryPool(const MemoryPool& other) = delete;

	/// Releases the memory reserved by this pool and destroys all allocated object instances.
	~MemoryPool() { clear(); }

	/// Do not allow copying.
	MemoryPool& operator=(const MemoryPool& other) = delete;

	/// Allocates, constructs, and returns a new object instance.
	/// Any arguments passed to this method are forwarded to the class constructor.
	template<class... Args>
	inline T* construct(Args&&... args) {
		T* p = malloc();
		_alloc.construct(p, std::forward<Args>(args)...);
		return p;
	}

	/// Destroys all object instances belonging to the memory pool
	/// and releases the memory pages allocated by the pool.
	inline void clear(bool keepPageReserved = false) {
		for(auto i = _pages.cbegin(); i != _pages.cend(); ++i) {
			T* p = *i;
			T* pend = p + _pageSize;
			if(i+1 == _pages.end())
				pend = p + _lastPageNumber;
			for(; p != pend; ++p)
				_alloc.destroy(p);
			if(!keepPageReserved || i != _pages.cbegin()) {
				_alloc.deallocate(*i, _pageSize);
			}
		}
		if(!keepPageReserved) {
			_pages.clear();
			_lastPageNumber = _pageSize;
		}
		else if(!_pages.empty()) {
			_pages.resize(1);
			_lastPageNumber = 0;
		}
		_size = 0;
	}

	/// Returns the number of bytes currently reserved by this memory pool.
	size_t memoryUsage() const {
		return _pages.size() * _pageSize * sizeof(T);
	}

	/// Swaps this memory pool with another pool instance.
	void swap(MemoryPool<T>& other) {
		_pages.swap(other._pages);
		std::swap(_lastPageNumber, other._lastPageNumber);
		std::swap(_pageSize, other._pageSize);
		std::swap(_alloc, other._alloc);
		std::swap(_size, other._size);
	}

	size_t size() const { return _size; }

	T* element(size_t index) const {
		return &_pages[index / _pageSize][index % _pageSize];
	}

private:

	/// Allocates memory for a new object instance.
	T* malloc() {
		T* p;
		if(_lastPageNumber == _pageSize) {
			_pages.push_back(p = _alloc.allocate(_pageSize));
			_lastPageNumber = 1;
		}
		else {
			p = _pages.back() + _lastPageNumber;
			_lastPageNumber++;
		}
		_size++;
		return p;
	}

	std::vector<T*> _pages;
	size_t _lastPageNumber;
	size_t _pageSize;
	size_t _size;
	std::allocator<T> _alloc;
};

#endif // __MEMORY_POOL_H

