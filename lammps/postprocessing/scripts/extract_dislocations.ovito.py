"""
	This is a Python script for OVITO. It loads a MD simulation file, runs the DXA analysis on
	the dataset to extract dislocation lines and writes the results to a .ca file.

	Usage:

		ovitos extract_dislocations.ovito.py <input_file.dump> <output_file.ca>
"""

# Import OVITO modules.
from ovito.io import *
from ovito.modifiers import *
from ovito.data import *

import sys

# Parse command line parameters:
if len(sys.argv) != 3:
	sys.exit("Please specify the simulation file to process and the output file to write.")
input_file = sys.argv[1]
output_file = sys.argv[2]
if not output_file.endswith('.ca'):
	sys.exit("Output filename should end with .ca extension.")

# Load the simulation dataset to be analyzed.
print("Loading file %s" % input_file, flush=True)
node = import_file(input_file)

# Added analysis modifier for extracting dislocations.
node.modifiers.append(
	DislocationAnalysisModifier(
		input_crystal_structure = DislocationAnalysisModifier.Lattice.BCC, 
		trial_circuit_length=14, 
		circuit_stretchability=14, 
		line_point_separation=3.0))

# Evaluate data pipeline.
print("Identifying dislocations", flush=True)
node.compute()

# Export dislocations to CA file.
print("Saving results to file %s" % output_file, flush=True)
export_file(node, output_file, format="ca", export_mesh=True)
