#include "fix_disloc_orig.h"
#include "force.h"
#include "comm.h"
#include "modify.h"
#include "domain.h"
#include "error.h"
#include "atom.h"
#include "neighbor.h"
#include "error.h"
#include "update.h"

#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdarg>
#include <cassert>

#include <sys/stat.h>
#include <errno.h>

// Controls whether a single output file is written using MPI-IO.
// If disabled, each MPI task will write a separate output file.
#define FIX_DISLOC_USE_MPIIO 		0

/******************************************************************************
* Constructor.
******************************************************************************/
DislocationIdentificationFixOrig::DislocationIdentificationFixOrig(LAMMPS *lmp, int narg, char **arg) :
    Fix(lmp, narg, arg),
    edgePool(lmp)
{
    // Parse fix command parameters.
    int iarg = 3;
    if(narg < 7) error->all(FLERR, "Not enough parameters for fix disloc.");

    // Timestep interval for extracting dislocations.
    this->nevery = force->inumeric(FLERR,arg[iarg++]);
    if(this->nevery < 1) error->all(FLERR,"Invalid timestep parameter for fix disloc.");

    // Output base name
    outputBasename = arg[iarg++];
    fileper = 1;

    // Select lattice type and initialize ideal lattice vectors.
    latticeStructure = arg[iarg++];
    if(latticeStructure == "bcc") {
        // 1/2<111> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(0.5,0.5,0.5));
        latticeVectors.push_back(Vector3(-0.5,0.5,0.5));
        latticeVectors.push_back(Vector3(0.5,-0.5,0.5));
        latticeVectors.push_back(Vector3(-0.5,-0.5,0.5));
        latticeVectors.push_back(Vector3(0.5,0.5,-0.5));
        latticeVectors.push_back(Vector3(-0.5,0.5,-0.5));
        latticeVectors.push_back(Vector3(0.5,-0.5,-0.5));
        latticeVectors.push_back(Vector3(-0.5,-0.5,-0.5));
        // <100> second nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(1.0,0.0,0.0));
        latticeVectors.push_back(Vector3(-1.0,0.0,0.0));
        latticeVectors.push_back(Vector3(0.0,1.0,0.0));
        latticeVectors.push_back(Vector3(0.0,-1.0,0.0));
        latticeVectors.push_back(Vector3(0.0,0.0,1.0));
        latticeVectors.push_back(Vector3(0.0,0.0,-1.0));
    }
    else if(latticeStructure == "fcc") {
        // 1/2<110> nearest neighbor vectors in standard Bravais cell:
    	latticeVectors.push_back(Vector3( 0.5,  0.5,  0.0));
    	latticeVectors.push_back(Vector3( 0.0,  0.5,  0.5));
    	latticeVectors.push_back(Vector3( 0.5,  0.0,  0.5));
    	latticeVectors.push_back(Vector3(-0.5, -0.5,  0.0));
    	latticeVectors.push_back(Vector3( 0.0, -0.5, -0.5));
    	latticeVectors.push_back(Vector3(-0.5,  0.0, -0.5));
    	latticeVectors.push_back(Vector3(-0.5,  0.5,  0.0));
    	latticeVectors.push_back(Vector3( 0.0, -0.5,  0.5));
    	latticeVectors.push_back(Vector3(-0.5,  0.0,  0.5));
    	latticeVectors.push_back(Vector3( 0.5, -0.5,  0.0));
    	latticeVectors.push_back(Vector3( 0.0,  0.5, -0.5));
    	latticeVectors.push_back(Vector3( 0.5,  0.0, -0.5));
        // 1/6<112> partial vectors:
    	latticeVectors.push_back(Vector3( 0.3333333333333,  0.1666666666666,  0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666,  0.3333333333333,  0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666,  0.1666666666666,  0.3333333333333));
    	latticeVectors.push_back(Vector3(-0.3333333333333,  0.1666666666666,  0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666,  0.3333333333333,  0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666,  0.1666666666666,  0.3333333333333));
    	latticeVectors.push_back(Vector3( 0.3333333333333, -0.1666666666666,  0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666, -0.3333333333333,  0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666, -0.1666666666666,  0.3333333333333));
    	latticeVectors.push_back(Vector3( 0.3333333333333,  0.1666666666666, -0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666,  0.3333333333333, -0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666,  0.1666666666666, -0.3333333333333));
    	latticeVectors.push_back(Vector3(-0.3333333333333, -0.1666666666666,  0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666, -0.3333333333333,  0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666, -0.1666666666666,  0.3333333333333));
    	latticeVectors.push_back(Vector3( 0.3333333333333, -0.1666666666666, -0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666, -0.3333333333333, -0.1666666666666));
    	latticeVectors.push_back(Vector3( 0.1666666666666, -0.1666666666666, -0.3333333333333));
    	latticeVectors.push_back(Vector3(-0.3333333333333,  0.1666666666666, -0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666,  0.3333333333333, -0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666,  0.1666666666666, -0.3333333333333));
    	latticeVectors.push_back(Vector3(-0.3333333333333, -0.1666666666666, -0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666, -0.3333333333333, -0.1666666666666));
    	latticeVectors.push_back(Vector3(-0.1666666666666, -0.1666666666666, -0.3333333333333));
    }
    else error->all(FLERR,"Invalid lattice type in fix disloc command.");

    // Lattice parameter.
    double latticeParameter = force->numeric(FLERR,arg[iarg++]);
    if(latticeParameter <= 0.0) error->all(FLERR,"Invalid lattice parameter in fix disloc command.");

    // The lattice orientation within the global simulation coordinate system.
    // Assume identity matrix by default, i.e., Bravais cell is aligned with coordinate axes.
    double orientation[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

    // Parse extra keyword parameters.
    while(iarg < narg) {
        if(strcmp(arg[iarg], "orient") == 0) {
            if(iarg + 5 > narg) error->all(FLERR,"Illegal 'orient' keyword");
            int dim = -1;
            if(strcmp(arg[iarg+1],"x") == 0) dim = 0;
            else if(strcmp(arg[iarg+1],"y") == 0) dim = 1;
            else if(strcmp(arg[iarg+1],"z") == 0) dim = 2;
            else error->all(FLERR,"Invalid axis in 'orient' keyword");
            orientation[dim][0] = force->inumeric(FLERR,arg[iarg+2]);
            orientation[dim][1] = force->inumeric(FLERR,arg[iarg+3]);
            orientation[dim][2] = force->inumeric(FLERR,arg[iarg+4]);
            iarg += 5;
        }
        else if(strcmp(arg[iarg], "fileper") == 0) {
            if(iarg + 2 > narg) error->all(FLERR,"Illegal 'fileper' keyword");
            fileper = force->inumeric(FLERR,arg[iarg+1]);
            if(fileper <= 0) error->all(FLERR,"Invalid 'fileper' parameter value");
            iarg += 2;
        }
        else error->all(FLERR,"Unknown optional parameter in fix disloc command.");
    }

    prepareLatticeVectors(latticeParameter, orientation);
}

/******************************************************************************
* Destructor.
******************************************************************************/
DislocationIdentificationFixOrig::~DislocationIdentificationFixOrig()
{
}

/*********************************************************************
 * Precomputes the ideal lattice vectors.
 *********************************************************************/
void DislocationIdentificationFixOrig::prepareLatticeVectors(double latticeParameter, double orientation[3][3])
{
    // Check if coordinate axes are orthogonal.
    if(orientation[0][0]*orientation[1][0] + orientation[0][1]*orientation[1][1] + orientation[0][2]*orientation[1][2] ||
        orientation[1][0]*orientation[2][0] + orientation[1][1]*orientation[2][1] + orientation[1][2]*orientation[2][2] ||
        orientation[0][0]*orientation[2][0] + orientation[0][1]*orientation[2][1] + orientation[0][2]*orientation[2][2])
        error->all(FLERR,"Lattice orient vectors are not orthogonal.");

    // Check righthandedness of orientation vectors. x cross y must be in same direction as z.
    int xy0 = orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1];
    int xy1 = orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2];
    int xy2 = orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0];
    if(xy0*orientation[2][0] + xy1*orientation[2][1] + xy2*orientation[2][2] <= 0)
        error->all(FLERR,"Lattice orient vectors are not right-handed.");

    // Normalize orient vectors.
    for(int i = 0; i < 3; i++) {
        double len = sqrt(orientation[i][0]*orientation[i][0] + orientation[i][1]*orientation[i][1] + orientation[i][2]*orientation[i][2]);
        for(int j = 0; j < 3; j++) orientation[i][j] /= len;
    }

    // Prepare lattice orientation matrix.
    double det =((orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0])*(orientation[2][2])
			  -(orientation[0][0]*orientation[1][2] - orientation[0][2]*orientation[1][0])*(orientation[2][1])
			  +(orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1])*(orientation[2][0]));
    double inv_rotation[3][3] = {
    				{		(orientation[1][1]*orientation[2][2] - orientation[1][2]*orientation[2][1])/det,
							(orientation[2][0]*orientation[1][2] - orientation[1][0]*orientation[2][2])/det,
							(orientation[1][0]*orientation[2][1] - orientation[1][1]*orientation[2][0])/det},
					{		(orientation[2][1]*orientation[0][2] - orientation[0][1]*orientation[2][2])/det,
							(orientation[0][0]*orientation[2][2] - orientation[2][0]*orientation[0][2])/det,
							(orientation[0][1]*orientation[2][0] - orientation[0][0]*orientation[2][1])/det},
					{		(orientation[0][1]*orientation[1][2] - orientation[1][1]*orientation[0][2])/det,
							(orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2])/det,
							(orientation[0][0]*orientation[1][1] - orientation[1][0]*orientation[0][1])/det}};

    // Put null vector at index 0 of vector table.
    latticeVectors.insert(latticeVectors.begin(), Vector3(0,0,0));

    printLog("Initial number of lattice vectors: %i\n", latticeVectors.size());

    // Compute linear combinations of existing lattice vectors to generate more lattice vectors.
    for(int depth = 0; depth < 1; depth++) {
        for(size_t i = 0, lvcount = latticeVectors.size(); i < lvcount; i++) {
            for(size_t j = 0; j <= i; j++) {
                Vector3 v = latticeVectors[i] + latticeVectors[j];
                if(latticeVectors.find(v) == -1) latticeVectors.push_back(v);
            }
        }
    }

    printLog("Final number of lattice vectors: %i\n", latticeVectors.size());

    // Transform lattice vectors and scale by the user-specified lattice constant.
    scaledLatticeVectors.resize(latticeVectors.size());
    for(size_t i = 0; i < latticeVectors.size(); i++) {
    	for(size_t a = 0; a < 3; a++) {
    		scaledLatticeVectors[i][a] = 0;
    		for(size_t b = 0; b < 3; b++)
    			scaledLatticeVectors[i][a] += inv_rotation[a][b] * latticeVectors[i][a] * latticeParameter;
    	}
    }

    // Generate lookup table for reverse vectors.
    reverseLatticeVectors.resize(latticeVectors.size());
    for(size_t i = 0; i < latticeVectors.size(); i++)
        reverseLatticeVectors[i] = latticeVectors.find(-latticeVectors[i]);

    // Build lookup table for vector sums.
    latticeVectorSums.resize(latticeVectors.size());
    for(size_t i = 0; i < latticeVectors.size(); i++) {
        latticeVectorSums[i].resize(latticeVectors.size());
        for(size_t j = 0; j < latticeVectors.size(); j++) {
            latticeVectorSums[i][j] = latticeVectors.find(latticeVectors[i] + latticeVectors[j]);
        }
    }
}

/*********************************************************************
 * The return value of this method specifies at which points the
 * fix is invoked during the simulation.
 *********************************************************************/
int DislocationIdentificationFixOrig::setmask()
{
    return FixConst::END_OF_STEP;
}

/*********************************************************************
 * This gets called by the system before the simulation starts.
 *********************************************************************/
void DislocationIdentificationFixOrig::init()
{
    if(atom->tag_enable == 0)
        error->all(FLERR, "Fix disloc requires atoms having IDs.");
}

/*********************************************************************
 * This is called by LAMMPS at the beginning of a run.
 *********************************************************************/
void DislocationIdentificationFixOrig::setup(int)
{
    end_of_step();
}

/*********************************************************************
 * Called by LAMMPS at the end of each selected MD integration step.
 *********************************************************************/
void DislocationIdentificationFixOrig::end_of_step()
{
    printLog("Performing dislocation analysis on timestep %i\n", update->ntimestep);

    // Clean up from last analysis run.
    tessellation.reset();
    edgePool.clear();
    vertexEdges.clear();

    // Determine output filename by replacing '*' with the current timestep.
    std::string pathname = outputBasename;
    size_t wildcardPos = pathname.find('*');
    if(wildcardPos != std::string::npos) {
        char time_str[16];
        sprintf(time_str, "%i", (int)update->ntimestep);
        pathname.replace(wildcardPos, 1, time_str);
    }

#if FIX_DISLOC_USE_MPIIO
    // Nothing to do.
#else
    // Create output directory, which will receive the per-task output files.
    if(comm->me == 0) {
		if(mkdir(pathname.c_str(), 0755) == -1 && errno != EEXIST) {
			error->one(FLERR,"Failed to create output directory.");
		}
    }
    MPI_Barrier(world);
#endif

    // Compute the Delaunay tessellation based on the input atoms.
    generateTessellation();

    // Assign ideal lattice vectors to tessellation edges.
    assignLatticeVectors();

#if 0
    // Optimize assigned ideal lattice vectors to avoid artifacts.
    optimizeAssignedLatticeVectors();
#endif

    // Find tessellation cells which are potentially intersected by a dislocation.
    classifyCells();

    // Write intermediate output file.
    writeOutputFile(pathname);
}

/*********************************************************************
 * Algorithm step 1:
 *
 * Calls the GEOGRAM library to compute the Delaunay tessellation.
 * The atomic coordinates serve as input vertices for the tessellation.
 *********************************************************************/
void DislocationIdentificationFixOrig::generateTessellation()
{
    printLog("Number of local atoms: %i\n", atom->nlocal);
    printLog("Number of ghost atoms: %i\n", atom->nghost);

	// Initialize the Geogram library.
	static bool isGeogramInitialized = false;
	if(!isGeogramInitialized) {
		isGeogramInitialized = true;
		GEO::initialize();
		GEO::set_assert_mode(GEO::ASSERT_ABORT);
	}

	// Create the internal Delaunay generator object.
	tessellation = new GEO::Delaunay3d();
	tessellation->set_keeps_infinite(true);
	tessellation->set_reorder(true);

	// Construct Delaunay tessellation.
	tessellation->set_vertices(atom->nlocal + atom->nghost, atom->x[0]);

    printLog("Number of tessellation cells: %i\n", tessellation->nb_cells());

    vertexEdges.resize(atom->nlocal + atom->nghost, NULL);
}

/*********************************************************************
 * Algorithm step 2:
 *
 * Assigns ideal lattice vectors to edges of the tessellation.
 *********************************************************************/
void DislocationIdentificationFixOrig::assignLatticeVectors()
{
    double ghostLayerSize;
    if(domain->triclinic) {
		// cutghost is in lamda coordinates for triclinic boxes, use subxx_lamda
		double *h = domain->h;
		double cut_bound[3];
		cut_bound[0] = h[0]*comm->cutghost[0] + h[5]*comm->cutghost[1] + h[4]*comm->cutghost[2];
		cut_bound[1] = h[1]*comm->cutghost[1] + h[3]*comm->cutghost[2];
		cut_bound[2] = h[2]*comm->cutghost[2];
		ghostLayerSize = std::min(cut_bound[0], std::min(cut_bound[1], cut_bound[2]));
    }
    else {
    	ghostLayerSize = std::min(comm->cutghost[0], std::min(comm->cutghost[1], comm->cutghost[2]));
    }

    // Maximum deviation threshold:
    double skipEdgeThreshold = scaledLatticeVectors[1].squared_length() / 2.0;
    double maxEdgeLength = 0.5 * (ghostLayerSize - neighbor->skin);
    double maxEdgeLengthSq = maxEdgeLength * maxEdgeLength;

    printLog("Ghost layer size: %f\n", ghostLayerSize);
    printLog("Effective ghost layer size: %f\n", ghostLayerSize - neighbor->skin);
    printLog("Max. tessellation edge length: %f\n", maxEdgeLength);
    printLog("Min. required ghost layer size: %f\n", sqrt(scaledLatticeVectors[1].squared_length() * 2.0));
    if(maxEdgeLengthSq <= scaledLatticeVectors[1].squared_length() * 2.0)
    	error->all(FLERR,"Ghost layer size is too small to perform dislocation identification.");

    // List of six edges of a tetrahedron.
    // Each edge is defined in terms of two cell vertex indices.
    static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

    double** x = atom->x;

    size_t numEdges = 0;
    cellInfo.resize(tessellation->nb_cells());
    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {
    	if(!isValidCell(cell)) {
    		cellInfo[cell].markBad();
    		continue;
    	}

        for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
        	GEO::index_t v1 = tessellation->cell_vertex(cell, edgeVertices[edgeIndex][0]);
        	GEO::index_t v2 = tessellation->cell_vertex(cell, edgeVertices[edgeIndex][1]);
            Edge* edge = findEdge(v1, v2);
            if(!edge) {
                int lv = -1;

                Vector3 delta(x[v1][0] - x[v2][0], x[v1][1] - x[v2][1], x[v1][2] - x[v2][2]);
                if(delta.squared_length() <= maxEdgeLengthSq)
                    lv = scaledLatticeVectors.closestVector(delta, skipEdgeThreshold);

                if(lv != -1) {
                    edge = edgePool.construct(lv, v2);
                    Edge* oppositeEdge = edgePool.construct(reverseLatticeVectors[lv], v1);

                    // Insert edge and its opposite edge into the linked list of
                    // edges of the vertices they connect.
                    edge->nextLeavingEdge = vertexEdges[v1];
                    vertexEdges[v1] = edge;
                    oppositeEdge->nextLeavingEdge = vertexEdges[v2];
                    vertexEdges[v2] = oppositeEdge;
                    oppositeEdge->oppositeEdge = edge;
                    edge->oppositeEdge = oppositeEdge;

                    numEdges++;
                }
                else {
                    // Mark cell as 'bad' if it has at least one edge which cannot be assigned
                    // a valid lattice vector.
                    cellInfo[cell].markBad();
                    break;
                }
            }

            // Store pointer to edge in cell.
            cellInfo[cell].edges[edgeIndex] = edge;
        }
    }

    printLog("Number of tessellation edges: %i\n", numEdges);
}

/*********************************************************************
 * Algorithm step 3:
 *
 * Classifies tessellation cells as either 'good' or 'bad'.
 *********************************************************************/
void DislocationIdentificationFixOrig::classifyCells()
{
    // List of Burgers circuits on a tetrahedron, each defined by a sequence of three edges.
    static const int burgersCircuitEdges[4][3] = {{3,5,4},{1,5,2},{0,4,2},{0,3,1}};

    size_t numBadCells = 0;
    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {

        // Skip cells which have already been marked as bad.
        if(cellInfo[cell].isBad()) {
            numBadCells++;
            continue;
        }

        // Mark cells that are intersected by a dislocation as 'bad'.

        // Gather lattice vectors assigned to the six edges of the tetrahedron.
        int edgeVectors[6];
        for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++)
            edgeVectors[edgeIndex] = cellInfo[cell].edges[edgeIndex]->latticeVector;

        // Perform Burgers test on three of the four tetrahedron facets.
        for(int face = 0; face < 3; face++) {
            Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[face][0]]] +
                        latticeVectors[edgeVectors[burgersCircuitEdges[face][1]]] -
                        latticeVectors[edgeVectors[burgersCircuitEdges[face][2]]];
            if(b.squared_length() > 1e-6) {
                cellInfo[cell].markBad();
                numBadCells++;
                break;
            }
        }
    }

    printLog("Number of bad cells: %i\n", numBadCells);
}

/*********************************************************************
 * Reports the memory usage of this fix to LAMMPS.
 *********************************************************************/
double DislocationIdentificationFixOrig::memory_usage()
{
    return edgePool.memoryUsage();
}

/*********************************************************************
 * Sends a formatted string to the log file and stdout.
 *********************************************************************/
void DislocationIdentificationFixOrig::printLog(const char* format, ...)
{
    if(comm->me == 0 && screen) {
        va_list ap;
        va_start(ap,format);
        vfprintf(screen, format, ap);
        va_end(ap);
    }
    if(comm->me == 0 && logfile) {
        va_list ap;
        va_start(ap,format);
        vfprintf(logfile, format, ap);
        va_end(ap);
    }
}

/*********************************************************************
 * Generates the intermediate output file, which contains the
 * interface mesh facets of the local processor.
 *********************************************************************/
void DislocationIdentificationFixOrig::writeOutputFile(const std::string& pathname)
{
#if FIX_DISLOC_USE_MPIIO
	// Write a single file using MPI-IO

    // Determine output filename.
    std::string filename = pathname;

    // Open output file for writing using MPI-IO.
    MPI_File fh;
    int err = MPI_File_open(world, (char*)filename.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
    if(err != MPI_SUCCESS) {
    	char str[1024];
    	sprintf(str,"Cannot open output file %s for writing using MPI-IO.", filename.c_str());
    	error->one(FLERR,str);
    }

    // Write the local data to a byte buffer.
    std::ostringstream bufferStream;
    writeOutput(bufferStream);
    std::string buffer = bufferStream.str();
    bigint localBufferSize = buffer.size();

    // Determine the byte offset at which each proc starts writing.
    bigint offset;
    MPI_Scan(&localBufferSize, &offset, 1, MPI_LMP_BIGINT, MPI_SUM, world);
    offset -= localBufferSize;

    // Write data to file.
    MPI_File_set_size(fh, offset);
    MPI_File_write_at_all(fh, offset, (void*)buffer.c_str(), localBufferSize, MPI_CHAR, MPI_STATUS_IGNORE);

    // Done.
    MPI_File_close(&fh);

#else
    // Write partial output files. Files are collected in a subdirectory.

    if((comm->me % fileper) == 0) {

		// Determine output filename of this processor.
		std::ostringstream filename_stream;
		filename_stream << pathname << "/proc." << comm->me;
		std::string filename = filename_stream.str();

		// Open output file for writing.
		std::ofstream stream(filename.c_str());
		if(!stream.is_open())
			error->one(FLERR,"Failed to open DXA output file for writing.");
		writeOutput(stream);

		// Receive data from other processors.
		std::vector<char> receiveBuffer;
		for(int proc = comm->me + 1; proc < comm->me + fileper && proc < comm->nprocs; proc++) {
			// Receive number of bytes to be received.
			MPI_Status status;
			int numBytesToReceive;
			MPI_Recv(&numBytesToReceive, 1, MPI_INT, proc, 0, world, &status);
			// Allocate receive buffer.
			receiveBuffer.resize(numBytesToReceive);
			// Receive actual data.
			MPI_Recv(&receiveBuffer.front(), numBytesToReceive, MPI_CHAR, proc, 0, world, &status);
			// Write to output file.
			stream.write(&receiveBuffer.front(), receiveBuffer.size());
		}
    }
    else {

    	// Write output to a byte buffer and send it to I/O processor.
        std::ostringstream bufferStream;
        writeOutput(bufferStream);
        std::string buffer = bufferStream.str();

        // Closest processor that writes data to a file.
        int dest_proc = (comm->me / fileper) * fileper;

		// First, send number of bytes to be transmitted.
		int numBytes = buffer.size();
		MPI_Send(&numBytes, 1, MPI_INT, dest_proc, 0, world);

    	// Then send data.
		MPI_Send((void*)buffer.c_str(), numBytes, MPI_CHAR, dest_proc, 0, world);
    }

#endif
}

/*********************************************************************
 * Writes the mesh of the local processor to the given output stream.
 *********************************************************************/
void DislocationIdentificationFixOrig::writeOutput(std::ostream& stream)
{
    // List of six edges of a tetrahedron.
    // Each edge is defined in terms of two cell vertex indices.
    static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

    // Write file header.
    stream << "# fix disloc intermediate file\n";
    stream << "# format version 1\n";

    // Save simulation timestep.
    stream << "simulation timestep:\n";
    stream << update->ntimestep << "\n";

    // Save simulation cell geometry and boundary conditions.
    stream << "simulation cell:\n";
    stream << domain->h[0] << " 0 0 " << (domain->periodicity[0] ? "pp" : "ff") << "\n";
    stream << domain->h[5] << " " << domain->h[1] << " 0 " << (domain->periodicity[1] ? "pp" : "ff") << "\n";
    stream << domain->h[4] << " " << domain->h[3] << " " << domain->h[2] << " " << (domain->periodicity[2] ? "pp" : "ff") << "\n";
    stream << domain->boxlo[0] << " " << domain->boxlo[1] << " " << domain->boxlo[2] << "\n";

    // Save processor grid information.
    stream << "processor grid:\n";
    stream << comm->procgrid[0] << " " << comm->procgrid[1] << " " << comm->procgrid[2] << "\n";
    stream << "processor id:\n";
    stream << comm->me << "\n";
    stream << "processor location:\n";
    stream << comm->myloc[0] << " " << comm->myloc[1] << " " << comm->myloc[2] << "\n";
    stream << "processor neighbors:\n";
    stream << comm->procneigh[0][0] << " " << comm->procneigh[0][1] << "\n";
    stream << comm->procneigh[1][0] << " " << comm->procneigh[1][1] << "\n";
    stream << comm->procneigh[2][0] << " " << comm->procneigh[2][1] << "\n";

    // Save lattice structure information.
    stream << "lattice structure:\n";
    stream << latticeStructure << "\n";

    // Save list of ideal lattice vectors.
    stream << "lattice vectors:\n";
    stream << latticeVectors.size() << "\n";
    VectorList::const_iterator sv = scaledLatticeVectors.begin();
    for(VectorList::const_iterator lv = latticeVectors.begin(); lv != latticeVectors.end(); ++lv, ++sv)
        stream << lv->x() << " " << lv->y() << " " << lv->z() << " -> " << sv->x() << " " << sv->y() << " " << sv->z() << "\n";

    // Lists the mesh vertices (i.e. atoms) that need to be output.
    // Will be determined while writing out the triangular facets.
    // A value of -1 indicates that the corresponding atom should not be output. 
    std::vector<int> vertexOutputMap(atom->nlocal + atom->nghost, -1);
    int numOutputVertices = 0;

    // Write out triangular facets of the interface mesh.
    // The interface mesh is formed by all tessellation facets that have a good tetrahedron on one side
    // and a bad tetrahedron on the other.
    stream << "facets:\n";
    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {
        // We want to visit the good cells.
        if(cellInfo[cell].isBad())
            continue;

        // Iterate over the four faces of the good tetrahedron cell.
        for(int f = 0; f < 4; f++) {
        
            // Test if the adjacent tetrahedron belongs to the bad region.
        	GEO::index_t adjacentCell = mirrorFacet(cell, f).first;
            if(cellInfo[adjacentCell].isGood())
                continue;   // It's also a good tet. That means we don't create an interface facet here.

            // Determine if the facet is a local facet (i.e. belongs to the local processor).
            // Look for the vertex atom with the highest ID.
            // If it is a local atom, the facet is local.
            tagint highestAtomId = 0;
            bool isLocalFacet;
            for(int v = 0; v < 3; v++) {
            	GEO::index_t vertex = tessellation->cell_vertex(cell, cellFacetVertexIndex(f, v));
                tagint atomId = atom->tag[vertex];
                if(atomId > highestAtomId) {
                    highestAtomId = atomId;
                    isLocalFacet = (vertex < (GEO::index_t)atom->nlocal);
                }
            }

            // Skip non-local facets which belong to neighboring processors.
            if(!isLocalFacet)
                continue;

            // Write out facet.
            for(int v = 0; v < 3; v++) {
                int vertexIndex1 = cellFacetVertexIndex(f, v);
                int vertexIndex2 = cellFacetVertexIndex(f, (v+1) % 3);
                GEO::index_t vertex1 = tessellation->cell_vertex(cell, vertexIndex1);
                GEO::index_t vertex2 = tessellation->cell_vertex(cell, vertexIndex2);

                int atomIndex = vertex1;
                if(vertexOutputMap[atomIndex] == -1) {
                    vertexOutputMap[atomIndex] = numOutputVertices++;
                }
                stream << "[" << atom->tag[atomIndex] << " ";

                // Look up the tessellation edge.
                for(int e = 0; e < 6; e++) {
                    if(edgeVertices[e][0] == vertexIndex1 && edgeVertices[e][1] == vertexIndex2) {
                        stream << cellInfo[cell].edges[e]->latticeVector << " ";
                        break;
                    }
                    else if(edgeVertices[e][0] == vertexIndex2 && edgeVertices[e][1] == vertexIndex1) {
                        stream << cellInfo[cell].edges[e]->oppositeEdge->latticeVector << " ";
                        break;
                    }
                }

                // Determine the adjacent facet of the interface mesh which shares an edge with the current facet.
                FacetCirculator circulator_start = incident_facets(cell, vertexIndex2, vertexIndex1, cell, f);
                FacetCirculator circulator = circulator_start;
                --circulator;
                do {
                    // Look for the first bad cell while going around the edge.
                    if(cellInfo[circulator.cell()].isBad())
                        break;

                    --circulator;
                }
                while(circulator != circulator_start);

                // Get the adjacent cell, which must be good.
                Facet mirrorFacet = this->mirrorFacet(circulator.cell(), circulator.facet());
                assert(cellInfo[mirrorFacet.first].isGood());

                // Iterate over the three vertices of the neighbor facets to find the third vertex,
                // which is not shared by the two adjacent facets.
                for(int vneigh = 0; vneigh < 3; vneigh++) {
                	GEO::index_t neigh_vertex = tessellation->cell_vertex(mirrorFacet.first, cellFacetVertexIndex(mirrorFacet.second, vneigh));
                    if(neigh_vertex != vertex1 && neigh_vertex != vertex2) {
                        // Write out the third vertex, which will be used to reconstruct the adjacency information.
                        int atomIndex = neigh_vertex;
                        if(vertexOutputMap[atomIndex] == -1) {
                            vertexOutputMap[atomIndex] = numOutputVertices++;
                        }
                        stream << atom->tag[atomIndex] << "]";
                    }
                }
            }
            stream << "\n";
        }
    }

    // Write out vertices.
    stream << "vertices:\n";
    stream << numOutputVertices << "\n";
    const tagint* t = atom->tag;
    double** x = atom->x;
    for(std::vector<int>::const_iterator v = vertexOutputMap.begin(); v != vertexOutputMap.end(); ++v, ++t, ++x) {
        if(*v != -1) {
            stream << (*t) << " " << (*x)[0] << " " << (*x)[1] << " " << (*x)[2] << "\n";
        }
    }

    // End of file marker.
    stream << "eof\n";
}

/*********************************************************************
 * Exports the 'bad' tessellation cells to a VTK file for debugging
 * purposes.
 *********************************************************************/
void DislocationIdentificationFixOrig::dumpBadCellsToVTKFile(const std::string& filename)
{
#if 0
    // Gather vertices that need to be written to the output file.
    std::set<DT::Vertex_handle> outputVertices;
    int numCells = 0;
    for(DT::Finite_cells_iterator cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
        if(cell->info().isBad()) {
            for(int i = 0; i < 4; i++)
                outputVertices.insert(cell->vertex(i));
            numCells++;
        }
    }

    // Write VTK output file.
    std::ofstream outputStream(filename.c_str());
    outputStream << "# vtk DataFile Version 3.0\n";
    outputStream << "# Bad tessellation cells\n";
    outputStream << "ASCII\n";
    outputStream << "DATASET UNSTRUCTURED_GRID\n";
    outputStream << "POINTS " << outputVertices.size() << " double\n";
    int index = 0;
    for(std::set<DT::Vertex_handle>::const_iterator v = outputVertices.begin(); v != outputVertices.end(); ++v) {
        outputStream << (*v)->point().x() << " " << (*v)->point().y() << " " << (*v)->point().z() << "\n";
        (*v)->info().index = index++;
    }
    outputStream << "\nCELLS " << numCells << " " << (numCells * 5) << "\n";
    for(DT::Finite_cells_iterator cell = tessellation.finite_cells_begin(); cell != tessellation.finite_cells_end(); ++cell) {
        if(cell->info().isBad()) {
            outputStream << "4";
            for(int i = 0; i < 4; i++) {
                outputStream << " " << cell->vertex(i)->info().index;
            }
            outputStream << "\n";
        }
    }
    outputStream << "\nCELL_TYPES " << numCells << "\n";
    for(int i = 0; i < numCells; i++)
        outputStream << "10\n";
#endif
}
