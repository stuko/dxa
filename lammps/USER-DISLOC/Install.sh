# Install/uninstall package files in LAMMPS
# edit two Makefile.package files to include/exclude package info

if (test $1 = 1) then

  if (test -e ../Makefile.package) then
    sed -i -e 's/[^ \t]*disloc[^ \t]* //' ../Makefile.package
    sed -i -e 's|^PKG_SYSINC =[ \t]*|&$(disloc_SYSINC) |' ../Makefile.package
    sed -i -e 's|^PKG_SYSLIB =[ \t]*|&$(disloc_SYSLIB) |' ../Makefile.package
    sed -i -e 's|^PKG_SYSPATH =[ \t]*|&$(disloc_SYSPATH) |' ../Makefile.package
  fi

  if (test -e ../Makefile.package.settings) then
    sed -i -e '/^include.*USER-DISLOC.*$/d' ../Makefile.package.settings
    # multiline form needed for BSD sed on Macs
    sed -i -e '4 i \
include ..\/USER-DISLOC\/Makefile.lammps
' ../Makefile.package.settings
  fi

  ln -s USER-DISLOC/fix_disloc_orig.cpp ..
  ln -s USER-DISLOC/fix_disloc_orig.h ..

elif (test $1 = 0) then

  if (test -e ../Makefile.package) then
    sed -i -e 's/[^ \t]*disloc[^ \t]* //' ../Makefile.package
  fi

  if (test -e ../Makefile.package.settings) then
    sed -i -e '/^include.*USER-DISLOC.*$/d' ../Makefile.package.settings
  fi

  rm -f ../fix_disloc_orig.cpp
  rm -f ../fix_disloc_orig.h

fi
