#include "string.h"
#include "stdlib.h"
#include "atom.h"
#include "update.h"
#include "modify.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "force.h"
#include "pair.h"
#include "comm.h"
#include "compute_misorientation.h"
#include "memory.h"
#include "error.h"

namespace LAMMPS_NS {

/* ---------------------------------------------------------------------- */

ComputeMisorientation::ComputeMisorientation(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg)
{
  int iarg = 3;
  if (narg < 4) error->all(FLERR,"Illegal compute misorientation/atom command");

  // Select lattice type and initialize ideal lattice vectors.
  if(strcmp(arg[iarg], "bcc") == 0) {
	  // Nothing to do.
  }
  else error->all(FLERR,"Invalid lattice type in compute misorientation/atom command.");
  iarg++;

  // The lattice orientation within the global simulation coordinate system.
  // Assume identity matrix by default, i.e., Bravais cell is aligned with coordinate axes.
  double orientation[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

  // Parse extra keyword parameters.
  while(iarg < narg) {
      if(strcmp(arg[iarg], "orient") == 0) {
          if(iarg + 5 > narg) error->all(FLERR,"Illegal 'orient' keyword");
          int dim = -1;
          if(strcmp(arg[iarg+1],"x") == 0) dim = 0;
          else if(strcmp(arg[iarg+1],"y") == 0) dim = 1;
          else if(strcmp(arg[iarg+1],"z") == 0) dim = 2;
          else error->all(FLERR,"Invalid axis in 'orient' keyword");
          orientation[dim][0] = force->inumeric(FLERR,arg[iarg+2]);
          orientation[dim][1] = force->inumeric(FLERR,arg[iarg+3]);
          orientation[dim][2] = force->inumeric(FLERR,arg[iarg+4]);
          iarg += 5;
      }
      else error->all(FLERR,"Unknown optional parameter in compute misorientation/atom command.");
  }
  prepareLatticeVectors(orientation);

  peratom_flag = 1;
  size_peratom_cols = 2;
  comm_forward = 1;

  output = NULL;
  nmax = 0;
}

/* ---------------------------------------------------------------------- */

ComputeMisorientation::~ComputeMisorientation()
{
  memory->destroy(output);
}

/*********************************************************************
 * Precomputes the ideal lattice vectors.
 *********************************************************************/
void ComputeMisorientation::prepareLatticeVectors(double orientation[3][3])
{
    // Check if coordinate axes are orthogonal.
    if(orientation[0][0]*orientation[1][0] + orientation[0][1]*orientation[1][1] + orientation[0][2]*orientation[1][2] ||
        orientation[1][0]*orientation[2][0] + orientation[1][1]*orientation[2][1] + orientation[1][2]*orientation[2][2] ||
        orientation[0][0]*orientation[2][0] + orientation[0][1]*orientation[2][1] + orientation[0][2]*orientation[2][2])
        error->all(FLERR,"Lattice orient vectors are not orthogonal.");

    // Check righthandedness of orientation vectors. x cross y must be in same direction as z.
    int xy0 = orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1];
    int xy1 = orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2];
    int xy2 = orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0];
    if(xy0*orientation[2][0] + xy1*orientation[2][1] + xy2*orientation[2][2] <= 0)
        error->all(FLERR,"Lattice orient vectors are not right-handed.");

    // Normalize orient vectors.
    for(int i = 0; i < 3; i++) {
        double len = sqrt(orientation[i][0]*orientation[i][0] + orientation[i][1]*orientation[i][1] + orientation[i][2]*orientation[i][2]);
        for(int j = 0; j < 3; j++) orientation[i][j] /= len;
    }

	static const double input_ideal_vectors[8][3] = {
			{ 0.5, 0.5, 0.5 }, {-0.5, -0.5, -0.5 },
			{ -0.5, 0.5, 0.5 }, {0.5, -0.5, -0.5 },
			{ 0.5, -0.5, 0.5 }, {-0.5, 0.5, -0.5 },
			{ 0.5, 0.5, -0.5 }, {-0.5, -0.5, 0.5 }
	};

	for(int i = 0; i < 8; i++) {
		ideal_vectors[i][0] = input_ideal_vectors[i][0]*orientation[0][0] + input_ideal_vectors[i][1]*orientation[1][0] + input_ideal_vectors[i][2]*orientation[2][0];
		ideal_vectors[i][1] = input_ideal_vectors[i][0]*orientation[0][1] + input_ideal_vectors[i][1]*orientation[1][1] + input_ideal_vectors[i][2]*orientation[2][1];
		ideal_vectors[i][2] = input_ideal_vectors[i][0]*orientation[0][2] + input_ideal_vectors[i][1]*orientation[1][2] + input_ideal_vectors[i][2]*orientation[2][2];
	}
}

/* ---------------------------------------------------------------------- */

void ComputeMisorientation::init()
{
  if (force->pair == NULL)
    error->all(FLERR,"Compute misorientation/atom requires a pair style be defined");

  // need an occasional full neighbor list

  int irequest = neighbor->request(this,instance_me);
  neighbor->requests[irequest]->pair = 0;
  neighbor->requests[irequest]->compute = 1;
  neighbor->requests[irequest]->half = 0;
  neighbor->requests[irequest]->full = 1;
  neighbor->requests[irequest]->occasional = 1;
}

/* ---------------------------------------------------------------------- */

void ComputeMisorientation::init_list(int id, NeighList *ptr)
{
  list = ptr;
}

// Maximum number of neighbors in coordination structures recognized by the CNA.
#define CNA_MAX_PATTERN_NEIGHBORS 14

// Pair of neighbor atoms that form a bond (bit-wise storage, i.e., two bits are set).
typedef unsigned int CNAPairBond;

/*
  A bit-flag array that stores which pairs of neighbors are bonded
  and which are not (using bit-wise storage).
 */
struct NeighborBondArray
{
	/// Default constructor.
	NeighborBondArray() { memset(neighborArray, 0, sizeof(neighborArray)); }

	/// Two-dimensional bit array that stores the bonds between neighbors.
	unsigned int neighborArray[CNA_MAX_PATTERN_NEIGHBORS];

	/// Returns whether two nearest neighbors have a bond between them.
	bool neighborBond(int neighborIndex1, int neighborIndex2) const {
		return (neighborArray[neighborIndex1] & (1<<neighborIndex2));
	}

	/// Sets whether two nearest neighbors have a bond between them.
	void setNeighborBond(int neighborIndex1, int neighborIndex2, bool bonded) {
		if(bonded) {
			neighborArray[neighborIndex1] |= (1<<neighborIndex2);
			neighborArray[neighborIndex2] |= (1<<neighborIndex1);
		}
		else {
			neighborArray[neighborIndex1] &= ~(1<<neighborIndex2);
			neighborArray[neighborIndex2] &= ~(1<<neighborIndex1);
		}
	}
};

/*
 A nearest neighbor of the central atom.
 Stores vector and squared distance.
 */
struct AdaptiveCNANeighbor {
	double del[3];
	double distSq;

	// Used for sorting of neighbor list.
	bool operator<(const AdaptiveCNANeighbor& o) const { return distSq < o.distSq; }
	bool operator>(const AdaptiveCNANeighbor& o) const { return distSq > o.distSq; }
};

// Used for sorting of neighbor list.
inline int AdaptiveCNANeighborComp(const void* a, const void* b) {
	return (static_cast<const AdaptiveCNANeighbor*>(a)->distSq < static_cast<const AdaptiveCNANeighbor*>(b)->distSq) ? -1 : +1;
}

static inline int findCommonNeighbors(const NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors);
static inline int findNeighborBonds(const NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds);
static inline int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds);

template<typename T>
inline void my_swap(T& a, T& b) { T tmp(a); a = b; b = tmp; }

// Find k smallest values in array of length n.
// This is based on the ComputeAcklandAtom::select() routine.
// Before exiting, it sorts the partial list using qsort().
static void my_partial_sort(AdaptiveCNANeighbor* arr, int k, int n)
{
	arr--;
	int l = 1;
	int ir = n;
	for (;;) {
		if (ir <= l+1) {
			if (ir == l+1 && arr[ir] < arr[l])
				my_swap(arr[l],arr[ir]);
			qsort(arr, k, sizeof(AdaptiveCNANeighbor), AdaptiveCNANeighborComp);
			return;
		} else {
			int mid=(l+ir) >> 1;
			my_swap(arr[mid],arr[l+1]);
			if (arr[l] > arr[ir])
				my_swap(arr[l],arr[ir]);
			if (arr[l+1] > arr[ir])
				my_swap(arr[l+1],arr[ir]);
			if (arr[l] > arr[l+1])
				my_swap(arr[l],arr[l+1]);
			int i = l+1;
			int j = ir;
			AdaptiveCNANeighbor a = arr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				my_swap(arr[i],arr[j]);
			}
			arr[l+1] = arr[j];
			arr[j] = a;
			if (j >= k) ir = j-1;
			if (j <= k) l = i;
		}
	}
}

/* ---------------------------------------------------------------------- */

void ComputeMisorientation::compute_peratom()
{
  int inum;
  int *ilist,*numneigh,**firstneigh;

  invoked_peratom = update->ntimestep;

  // grow centro array if necessary

  if(atom->nmax > nmax) {
    memory->destroy(output);
    nmax = atom->nmax;
    memory->create(output,nmax,2,"misorientation/atom:output");
    array_atom = output;
  }

  // invoke full neighbor list (will copy or build if necessary)

  neighbor->build_one(list);

  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  double **x = atom->x;
  int *mask = atom->mask;

  double localAverageNeighborDist = 0, totalAverageNeighborDist;
  int localNeighborDistCount = 0, totalNeighborDistCount;

  // Perform adaptive CNA.
  AdaptiveCNANeighbor* neighborVectors = NULL;
  int maxneigh = 0;
  for (int ii = 0; ii < inum; ii++) {
	int i = ilist[ii];

	if (!(mask[i] & groupbit)) {
	  output[i][0] = 0;
	  output[i][1] = 0;
	  continue;
	}

	double xtmp = x[i][0];
	double ytmp = x[i][1];
	double ztmp = x[i][2];
	int* jlist = firstneigh[i];
	int jnum = numneigh[i];

	if(jnum > maxneigh) {
		maxneigh = jnum * 2;
		memory->destroy(neighborVectors);
		memory->create(neighborVectors,maxneigh,"compute/cna/atom:neighvectors");
	}

	// Generate full list of bond vectors.
	for (int jj = 0; jj < jnum; jj++) {
	  int j = jlist[jj];
	  j &= NEIGHMASK;

	  AdaptiveCNANeighbor& n = neighborVectors[jj];
	  n.del[0] = xtmp - x[j][0];
	  n.del[1] = ytmp - x[j][1];
	  n.del[2] = ztmp - x[j][2];
	  n.distSq = n.del[0]*n.del[0] + n.del[1]*n.del[1] + n.del[2]*n.del[2];
	}

	// Select N nearest neighbors from full list and sort them.
	int N = CNA_MAX_PATTERN_NEIGHBORS;
	if(jnum < CNA_MAX_PATTERN_NEIGHBORS) N = jnum;
	my_partial_sort(neighborVectors, N, jnum);

	if(N != 0) {
		localAverageNeighborDist += sqrt(neighborVectors[0].distSq);
		localNeighborDistCount++;
	}

	// Analyze coordination structure.
	output[i][0] = calculateMisorientation(neighborVectors, N);
	output[i][1] = (output[i][0] != -1) ? 1 : 0;
  }
  memory->destroy(neighborVectors);

  // Determine average nearest neighbor distance in the system.
  MPI_Allreduce(&localAverageNeighborDist, &totalAverageNeighborDist, 1, MPI_DOUBLE, MPI_SUM, lmp->world);
  MPI_Allreduce(&localNeighborDistCount, &totalNeighborDistCount, 1, MPI_INT, MPI_SUM, lmp->world);
  if(totalNeighborDistCount == 0)
	  return;
  totalAverageNeighborDist /= totalNeighborDistCount;
  double smoothingCutoff = totalAverageNeighborDist * 1.207106781;
  double smoothingCutoffSq = smoothingCutoff * smoothingCutoff;

  for(int i = 0; i < 1; i++)
	  smoothField(false, smoothingCutoffSq);

  while(smoothField(true, smoothingCutoffSq) != 0);
}

int ComputeMisorientation::smoothField(bool grow, double smoothingCutoffSq)
{
  int inum;
  int *ilist,*numneigh,**firstneigh;
  inum = list->inum;
  ilist = list->ilist;
  numneigh = list->numneigh;
  firstneigh = list->firstneigh;

  double **x = atom->x;
  int *mask = atom->mask;

  double* newOutput = NULL;
  memory->create(newOutput,atom->nlocal,"misorientation/atom:newoutput");

  comm->forward_comm_compute(this);
  int localNewAtoms = 0;
  for (int ii = 0; ii < inum; ii++) {
	int i = ilist[ii];
	if (!(mask[i] & groupbit)) continue;

	double xtmp = x[i][0];
	double ytmp = x[i][1];
	double ztmp = x[i][2];
	int* jlist = firstneigh[i];
	int jnum = numneigh[i];

	double sum;
	int count;
	newOutput[i] = output[i][0];
	if(newOutput[i] >= 0) {
		if(grow) continue;
		sum = newOutput[i];
		count = 1;
	}
	else {
		if(!grow) continue;
		sum = 0;
		count = 0;
	}

	for (int jj = 0; jj < jnum; jj++) {
	  int j = jlist[jj];
	  j &= NEIGHMASK;

		if(output[j][0] == -1)
			continue;

	  double delx = xtmp - x[j][0];
	  double dely = ytmp - x[j][1];
	  double delz = ztmp - x[j][2];
	  double dist = delx*delx + dely*dely + delz*delz;
	  if(dist < smoothingCutoffSq) {
		sum += output[j][0];
		count++;
	  }
	}

	if(count != 0) {
		newOutput[i] = sum / count;
		localNewAtoms++;
	}
  }

  for (int ii = 0; ii < inum; ii++) {
    int i = ilist[ii];
    output[i][0] = newOutput[i];
  }
  memory->destroy(newOutput);

  int totalNewAtoms;
  MPI_Allreduce(&localNewAtoms, &totalNewAtoms, 1, MPI_INT, MPI_SUM, lmp->world);
  return totalNewAtoms;
}

int ComputeMisorientation::pack_forward_comm(int n, int* list, double* buf, int /*pbc_flag*/, int* /*pbc*/)
{
	int m = 0;
	for(int i = 0; i < n; i++) {
		buf[m++] = output[list[i]][0];
	}
	return m;
}

void ComputeMisorientation::unpack_forward_comm(int n, int first, double* buf)
{
	int last = first + n;
	for(int i = first; i < last; i++) {
		output[i][0] = *buf++;
	}
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based array
------------------------------------------------------------------------- */

double ComputeMisorientation::memory_usage()
{
  double bytes = nmax * sizeof(double) * 2;
  return bytes;
}

/* ----------------------------------------------------------------------
  Find all atoms that are nearest neighbors of the given pair of atoms.
------------------------------------------------------------------------- */
static int findCommonNeighbors(const NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors)
{
	commonNeighbors = neighborArray.neighborArray[neighborIndex];

#ifdef __GNUC__
	// Count the number of bits set in neighbor bit-field.
	return __builtin_popcount(commonNeighbors);
#else
	// Count the number of bits set in neighbor bit-field.
	unsigned int v = commonNeighbors - ((commonNeighbors >> 1) & 0x55555555);
	v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
	return (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
#endif
}

/* ----------------------------------------------------------------------
   Finds all bonds between common nearest neighbors.
------------------------------------------------------------------------- */
static int findNeighborBonds(const NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds)
{
	int numBonds = 0;

	unsigned int nib[CNA_MAX_PATTERN_NEIGHBORS];
	int nibn = 0;
	unsigned int ni1b = 1;
	for(int ni1 = 0; ni1 < numNeighbors; ni1++, ni1b <<= 1) {
		if(commonNeighbors & ni1b) {
			unsigned int b = commonNeighbors & neighborArray.neighborArray[ni1];
			for(int n = 0; n < nibn; n++) {
				if(b & nib[n])
					neighborBonds[numBonds++] = ni1b | nib[n];
			}

			nib[nibn++] = ni1b;
		}
	}
	return numBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds.
------------------------------------------------------------------------- */
static int getAdjacentBonds(unsigned int atom, CNAPairBond* bondsToProcess, int& numBonds, unsigned int& atomsToProcess, unsigned int& atomsProcessed)
{
    int adjacentBonds = 0;
	for(int b = numBonds - 1; b >= 0; b--) {
		if(atom & *bondsToProcess) {
            ++adjacentBonds;
   			atomsToProcess |= *bondsToProcess & (~atomsProcessed);
   			memmove(bondsToProcess, bondsToProcess + 1, sizeof(CNAPairBond) * b);
   			numBonds--;
		}
		else ++bondsToProcess;
	}
	return adjacentBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds between common neighbors and determine the length
   of the longest continuous chain.
------------------------------------------------------------------------- */
static int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds)
{
    // Group the common bonds into clusters.
	int maxChainLength = 0;
	while(numBonds) {
        // Make a new cluster, starting with the first remaining bond to be processed.
		numBonds--;
        unsigned int atomsToProcess = neighborBonds[numBonds];
        unsigned int atomsProcessed = 0;
		int clusterSize = 1;
        do {
        	// Determine the number of trailing 0-bits in atomsToProcess,
        	// starting at the least significant bit position.

#ifdef __GNUC__
			int nextAtomIndex = __builtin_ctz(atomsToProcess);
#else
			// Algorithm is from Bit Twiddling Hacks website.
			static const int MultiplyDeBruijnBitPosition[32] =
			{
			  0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
			  31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
			};
			int nextAtomIndex = MultiplyDeBruijnBitPosition[((unsigned int)((atomsToProcess & -atomsToProcess) * 0x077CB531U)) >> 27];
#endif
			unsigned int nextAtom = 1 << nextAtomIndex;
        	atomsProcessed |= nextAtom;
			atomsToProcess &= ~nextAtom;
			clusterSize += getAdjacentBonds(nextAtom, neighborBonds, numBonds, atomsToProcess, atomsProcessed);
		}
        while(atomsToProcess);
        if(clusterSize > maxChainLength)
        	maxChainLength = clusterSize;
	}
	return maxChainLength;
}

double ComputeMisorientation::calculateMisorientation(const AdaptiveCNANeighbor* neighborList, int numNeighbors)
{
	// Number of neighbors to analyze.
	int nn = 14; // For BCC atoms

	// Early rejection of under-coordinated atoms:
	if(numNeighbors < nn)
		return -1;

	// Compute scaling factor.
	double localScaling = 0;
	for(int n = 0; n < 8; n++)
		localScaling += sqrt(neighborList[n].distSq) / 0.866025404;
	for(int n = 8; n < 14; n++)
		localScaling += sqrt(neighborList[n].distSq);
	localScaling /= nn;
	double localCutoffSquared =  localScaling * localScaling * (1.207106781 * 1.207106781);

	// Compute bond bit-flag array.
	NeighborBondArray neighborArray;
	for(int ni1 = 0; ni1 < nn; ni1++) {
		neighborArray.setNeighborBond(ni1, ni1, false);
		for(int ni2 = ni1+1; ni2 < nn; ni2++) {
			double delx = neighborList[ni1].del[0] - neighborList[ni2].del[0];
			double dely = neighborList[ni1].del[1] - neighborList[ni2].del[1];
			double delz = neighborList[ni1].del[2] - neighborList[ni2].del[2];
			double d = delx*delx + dely*dely + delz*delz;
			neighborArray.setNeighborBond(ni1, ni2, d <= localCutoffSquared);
		}
	}

	int n444 = 0;
	int n666 = 0;
//	int cnaSignatures[CNA_MAX_PATTERN_NEIGHBORS];
	double nearestNeighborVectors[14][3];
	int nnc = 0;
	for(int ni = 0; ni < nn; ni++) {

		// Determine number of neighbors the two atoms have in common.
		unsigned int commonNeighbors;
		int numCommonNeighbors = findCommonNeighbors(neighborArray, ni, commonNeighbors, nn);
		if(numCommonNeighbors != 4 && numCommonNeighbors != 6)
			break;

		// Determine the number of bonds among the common neighbors.
		CNAPairBond neighborBonds[CNA_MAX_PATTERN_NEIGHBORS*CNA_MAX_PATTERN_NEIGHBORS];
		int numNeighborBonds = findNeighborBonds(neighborArray, commonNeighbors, nn, neighborBonds);
		if(numNeighborBonds != 4 && numNeighborBonds != 6)
			break;

		// Determine the number of bonds in the longest continuous chain.
		int maxChainLength = calcMaxChainLength(neighborBonds, numNeighborBonds);
		if(numCommonNeighbors == 4 && numNeighborBonds == 4 && maxChainLength == 4) {
			n444++;
		//	cnaSignatures[ni] = 1;
		}
		else if(numCommonNeighbors == 6 && numNeighborBonds == 6 && maxChainLength == 6) {
			n666++;
			//cnaSignatures[ni] = 0;
			nearestNeighborVectors[nnc][0] = neighborList[ni].del[0] / localScaling;
			nearestNeighborVectors[nnc][1] = neighborList[ni].del[1] / localScaling;
			nearestNeighborVectors[nnc][2] = neighborList[ni].del[2] / localScaling;
			nnc++;
		}
		else break;
	}
	if(n666 != 8 || n444 != 6)
		return -1;

	double totaldev = 0;
	for(int ni = 0; ni < 8; ni++) {
		double mindev = 1e8;
		for(int ni2 = 0; ni2 < 8; ni2++) {
			double deltax = nearestNeighborVectors[ni][0] - ideal_vectors[ni2][0];
			double deltay = nearestNeighborVectors[ni][1] - ideal_vectors[ni2][1];
			double deltaz = nearestNeighborVectors[ni][2] - ideal_vectors[ni2][2];
			double dev = deltax*deltax + deltay*deltay + deltaz*deltaz;
			if(dev < mindev) mindev = dev;
		}
		totaldev += mindev;
	}

	return totaldev / 8;
}

}

