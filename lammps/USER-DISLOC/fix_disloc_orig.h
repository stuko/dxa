#ifdef FIX_CLASS

FixStyle(disloc/orig, DislocationIdentificationFixOrig)

#else

#ifndef __FIX_DISLOC_ORIG_LAMMPS_H
#define __FIX_DISLOC_ORIG_LAMMPS_H

#include "fix.h"
#include "memory.h"

#include <geogram/delaunay/delaunay_3d.h>

using namespace LAMMPS_NS;

class DislocationIdentificationFixOrig : public Fix
{
public:

	struct Vector3 {
		double _data[3];
		Vector3() {}
		Vector3(double x, double y, double z) { _data[0] = x; _data[1]= y; _data[2] = z; }
		double x() const { return _data[0]; }
		double y() const { return _data[1]; }
		double z() const { return _data[2]; }
		double operator[](size_t pos) const { return _data[pos]; }
		double& operator[](size_t pos) { return _data[pos]; }
		double squared_length() const { return x()*x() + y()*y() + z()*z(); }
		Vector3 operator-(const Vector3& v) const { return Vector3( x() - v.x(), y() - v.y(), z() - v.z() ); }
		Vector3 operator+(const Vector3& v) const { return Vector3( x() + v.x(), y() + v.y(), z() + v.z() ); }
		Vector3 operator-() const { return Vector3(-x(), -y(), -z()); }
	};

    /// Helper class that stores a list of vectors.
    class VectorList : public std::vector<Vector3>
    {
    public:

        /// Returns the index of the vector from the list that is closest to the given vector.
        /// Returns -1 if no vector in the list is close enough.
        int closestVector(const Vector3& v, double maxDeviation) const {
            int result = -1;
            for(size_t i = 0; i < size(); i++) {
                double d = (v - (*this)[i]).squared_length();
                if(d < maxDeviation) {
                    maxDeviation = d;
                    result = i;
                }
            }
            return result;
        }

        /// Finds the index of a certain vector in this list.
        /// Returns -1 if no such vector exists in the list.
        int find(const Vector3& v, double maxDeviation = 1e-6) const {
            for(size_t i = 0; i < size(); i++) {
                if((v - (*this)[i]).squared_length() <= maxDeviation)
                    return i;
            }
            return -1;
        }
    };

    typedef std::pair<GEO::index_t, int> Facet;

    struct Edge;

    // Data structure for tetrahedral cells of the tessellation.
    struct Cell {

        Edge* edges[6]; // Each tetrahedral cell has six edges.

        bool isGood() const { return edges[0] != NULL; }
        bool isBad() const { return edges[0] == NULL; }
        void markBad() { edges[0] = NULL; }
    };

    // Data structure for edges of the tessellation.
    struct Edge {
        Edge(int lv, GEO::index_t v2) : latticeVector(lv), initialLatticeVector(lv), vertex2(v2), isFlagged(true) {}
        int latticeVector;        // The index of the ideal lattice vector assigned to this edge.
        int initialLatticeVector; // A copy of the value above, kept for reference.
        GEO::index_t vertex2;     // The vertex this edge points to.
        Edge* oppositeEdge;       // The opposite edge, pointing to the vertex this edge originates from.
        Edge* nextLeavingEdge;    // The next edge in the vertex' linked list of edges.
        bool isFlagged;           // Working variable.
        void flagEdge() { oppositeEdge->isFlagged = this->isFlagged = true; }
    };

    // Simple memory pool class for allocating 'Edge' structures.
    class EdgeMemoryPool : protected Pointers {
    public:
        EdgeMemoryPool(LAMMPS* ptr) : Pointers(ptr), _pageSize(16384), _numAllocated(0) {}
        ~EdgeMemoryPool() {
            // Release memory pages.
            for(std::vector<Edge*>::iterator p = _pages.begin(); p != _pages.end(); ++p)
                memory->sfree(*p);
        }
        Edge* construct(int lv, GEO::index_t v2) {
            if(_numAllocated == _pages.size() * _pageSize) {
                // Allocate a new memory page.
                _pages.push_back(static_cast<Edge*>(memory->smalloc(_pageSize * sizeof(Edge), "DislocationIdentificationFix::EdgeMemoryPool")));
            }
            Edge* edge = &_pages[_numAllocated / _pageSize][_numAllocated % _pageSize];
            // Call placement new operator to initialize Edge structure.
            new (edge)Edge(lv, v2);
            _numAllocated++;
            return edge;
        }
        void clear() {
            // Throw away objects, but hold on to allocated memory.
            _numAllocated = 0;
        }
        size_t memoryUsage() const { 
            return sizeof(Edge) * _pages.size() * _pageSize; 
        }
    private:
        std::vector<Edge*> _pages;
        size_t _pageSize;
        size_t _numAllocated;
        std::allocator<Edge> _alloc;
    };

	class FacetCirculator {
	public:
		FacetCirculator(const DislocationIdentificationFixOrig& tess, GEO::index_t cell, int s, int t, GEO::index_t start, int f) :
			_tess(tess), _s(tess.tessellation->cell_vertex(cell, s)), _t(tess.tessellation->cell_vertex(cell, t)) {
		    int i = tess.tessellation->index(start, _s);
		    int j = tess.tessellation->index(start, _t);
		    if(f == next_around_edge(i,j))
		    	_pos = start;
		    else
		    	_pos = tess.tessellation->cell_adjacent(start, f); // other cell with same facet
		}
		FacetCirculator& operator--() {
			_pos = _tess.tessellation->cell_adjacent(_pos, next_around_edge(_tess.tessellation->index(_pos, _t), _tess.tessellation->index(_pos, _s)));
			return *this;
		}
		FacetCirculator operator--(int) {
			FacetCirculator tmp(*this);
			--(*this);
			return tmp;
		}
		FacetCirculator & operator++() {
			_pos = _tess.tessellation->cell_adjacent(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
			return *this;
		}
		FacetCirculator operator++(int) {
			FacetCirculator tmp(*this);
			++(*this);
			return tmp;
		}
		Facet operator*() const {
			return Facet(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
		}
		Facet operator->() const {
			return Facet(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
		}
		GEO::index_t cell() const { return _pos; }
		int facet() const { return next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)); }
		bool operator==(const FacetCirculator& ccir) const
		{
			return _pos == ccir._pos && _s == ccir._s && _t == ccir._t;
		}
		bool operator!=(const FacetCirculator& ccir) const
		{
			return ! (*this == ccir);
		}

	private:
		const DislocationIdentificationFixOrig& _tess;
		GEO::index_t _s;
		GEO::index_t _t;
		GEO::index_t _pos;
		static int next_around_edge(int i, int j) {
			static const int tab_next_around_edge[4][4] = {
			      {5, 2, 3, 1},
			      {3, 5, 0, 2},
			      {1, 3, 5, 0},
			      {2, 0, 1, 5} };
			return tab_next_around_edge[i][j];
		}
	};

public:

    /// Constructor.
    DislocationIdentificationFixOrig(class LAMMPS*, int, char **);

    /// Destructor.
    virtual ~DislocationIdentificationFixOrig();

    /// The return value of this method specifies at which points the fix is invoked during the simulation.
    virtual int setmask();

    /// This gets called by the system before the simulation starts.
    virtual void init();

    /// This is called by LAMMPS at the beginning of a run.
    virtual void setup(int);

    /// Called at the end of MD time integration steps.
    virtual void end_of_step();

    /// Reports the memory usage of this fix to LAMMPS.
    virtual double memory_usage();

private:

    /// Sends a formatted string to the log file and stdout.
    void printLog(const char* format, ...);

    /// Precomputes the ideal lattice vectors.
    void prepareLatticeVectors(double latticeParameter, double orientation[3][3]);

    /// Calls the CGAL library to compute the Delaunay tessellation.
    /// The atomic coordinates serve as input vertices for the tessellation.
    void generateTessellation();

    /// Assigns ideal lattice vectors to edges of the tessellation.
    void assignLatticeVectors();

    /// Classifies tessellation cells as either 'good' or 'bad'.
    void classifyCells();

    /// Returns the edge connecting two vertices of the tessellation,
    /// or NULL if no such edge exists.
    Edge* findEdge(GEO::index_t v1, GEO::index_t v2) const {
		for(Edge* e = vertexEdges[v1]; e != NULL; e = e->nextLeavingEdge)
			if(e->vertex2 == v2) return e;
		return NULL;
    }

    /// Computes the sum of two lattice vectors.
    /// Returns -1 if the summed vector is not indexed because it is too long.
    int vectorSum(int v1, int v2) const {
        return latticeVectorSums[v1][v2];
    }

	/// Returns whether the given tessellation cell connects four physical vertices.
	/// Returns false if one of the four vertices is the infinite vertex.
	bool isValidCell(GEO::index_t cell) const {
		return tessellation->cell_is_finite(cell);
	}

	Facet mirrorFacet(GEO::index_t cell, int face) const {
		GEO::signed_index_t adjacentCell = tessellation->cell_adjacent(cell, face);
		return Facet(adjacentCell, tessellation->adjacent_index(adjacentCell, cell));
	}

	FacetCirculator incident_facets(GEO::index_t cell, int i, int j, GEO::index_t start, int f) const {
		return FacetCirculator(*this, cell, i, j, start, f);
	}

	/// Returns the cell vertex for the given triangle vertex of the given cell facet.
	static inline int cellFacetVertexIndex(int cellFacetIndex, int facetVertexIndex) {
		static const int tab_vertex_triple_index[4][3] = {
		 {1, 3, 2},
		 {0, 2, 3},
		 {0, 3, 1},
		 {0, 1, 2}
		};
		return tab_vertex_triple_index[cellFacetIndex][facetVertexIndex];
	}

    /// Generates the intermediate output file, which contains the interface mesh facets of the local processor.
    void writeOutputFile(const std::string& directory);

    /// Writes the mesh of the local processor to the given output stream.
    void writeOutput(std::ostream& stream);

    /// Exports the 'bad' tessellation cells to a VTK file for debugging purposes.
    void dumpBadCellsToVTKFile(const std::string& filename);

protected:

    /// Base name of the output files.
    std::string outputBasename;

    /// The number of partial outputs that are combined to a file when running in parallel.
    int fileper;

    /// The name of the input lattice structure, e.g. "bcc".
    std::string latticeStructure;

    /// The list of ideal lattice vectors.
    VectorList latticeVectors;

    /// List of lattice vectors, which have been scaled by the lattice constant.
    VectorList scaledLatticeVectors;

    /// The lookup table for reverse vectors.
    std::vector<int> reverseLatticeVectors;

    /// Lookup table for vector sums.
    /// For two lattice vectors a and b it gives (a+b).
    std::vector< std::vector<int> > latticeVectorSums;

    /// Used for efficient allocation of Edge data structures.
    EdgeMemoryPool edgePool;

	/// Stores the heads of the linked lists of edges leaving each vertex.
	std::vector<Edge*> vertexEdges;

	/// Stores per-cell auxiliary data.
	std::vector<Cell> cellInfo;

	/// The internal Delaunay generator object.
	GEO::SmartPointer<GEO::Delaunay3d> tessellation;
};

#endif // __FIX_DISLOC_ORIG_LAMMPS_H

#endif // FIX_CLASS
