#ifdef COMPUTE_CLASS

ComputeStyle(misorientation/atom,ComputeMisorientation)

#else

#ifndef COMPUTE_ORIENTATION_BCC_H
#define COMPUTE_ORIENTATION_BCC_H

#include "compute.h"

namespace LAMMPS_NS {

struct AdaptiveCNANeighbor;

class ComputeMisorientation : public Compute {
 public:
	ComputeMisorientation(class LAMMPS *, int, char **);
  ~ComputeMisorientation();
  void init();
  void init_list(int, class NeighList *);
  void compute_peratom();
  double memory_usage();
  virtual int pack_forward_comm(int, int *, double *, int, int *);
  virtual void unpack_forward_comm(int, int, double *);

 private:

  /// Precomputes the ideal lattice vectors.
  void prepareLatticeVectors(double orientation[3][3]);

  int smoothField(bool grow, double smoothingCutoffSq);

  double calculateMisorientation(const AdaptiveCNANeighbor* neighborList, int numNeighbors);

  int nmax;
  class NeighList *list;
  double **output;
  double ideal_vectors[8][3];
};

}

#endif
#endif
