#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.cm as cm 
import numpy as np
import math

#t = -0.9*math.pi
#print(t/math.pi, (math.fmod(t + math.pi, 2.0*math.pi)-math.pi)/math.pi)

#theta0 = np.arange(0.0, math.pi*2.0, 0.01)
#x = np.cos(theta0)
#y = np.sin(theta0)

def displacement_field(rx,ry,burgers,cut_dir):
    b = np.linalg.norm(burgers)
    nu = 0.0
    
    bnorm = burgers / b
    bortho = np.array((bnorm[1],-bnorm[0]))
    xt = rx*bnorm[0] + ry*bnorm[1]
    yt = rx*bortho[0] + ry*bortho[1]
    
    theta = np.arctan2(yt,xt)
    
    perp_dot = cut_dir[0] * bnorm[1] - cut_dir[1] * bnorm[0]
    omega = np.arctan2(perp_dot, -np.dot(bnorm, cut_dir))
    theta2 = np.fmod(theta + omega + 5*math.pi, 2.0*math.pi) - math.pi
    
    epsilon = 0.1 * b**2
    l = xt*xt+yt*yt
    ux2 = b * (theta2 + np.cos(theta) * np.sin(theta) / (2*(1 - nu))) / (2 * math.pi) 
    uy2 = b / (2 * math.pi * 4 * (1 - nu)) * ((1-2*nu)*np.log(l+epsilon) + np.cos(2*theta))
    ux_final = bnorm[0] * ux2 - bortho[0] * uy2
    uy_final = bnorm[1] * ux2 - bortho[1] * uy2
    return uy_final

n = 50
xx = np.linspace(-3., 3., n) 
yy = np.linspace(-3., 3., n) 
rx,ry = np.meshgrid(xx, yy)

burgers = np.array((0.0,1.0))
cut_dir = np.array((1.0,2.0))

cut_dir /= np.linalg.norm(cut_dir)

ux_final1 = displacement_field(rx-1,ry-2,burgers,cut_dir)
ux_final2 = displacement_field(rx+1,ry+2,-burgers,cut_dir)

fig, axes = plt.subplots(3)

ux_final = ux_final1 + ux_final2

vmin = min(np.min(ux_final1), np.min(ux_final2))
vmax = max(np.max(ux_final1), np.max(ux_final2))

#vmin = np.min(ux_final)
#vmax = np.max(ux_final)

mag = max(abs(vmax),abs(vmin))
vmax= mag
vmin = -mag

axes[0].pcolormesh(rx, ry, ux_final1, vmin=vmin, vmax=vmax, cmap='bwr')
axes[1].pcolormesh(rx, ry, ux_final2, vmin=vmin, vmax=vmax, cmap='bwr')
im = axes[2].pcolormesh(rx, ry, ux_final, vmin=vmin, vmax=vmax, cmap='bwr')

fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
fig.colorbar(im, cax=cbar_ax, cmap='bwr')

plt.show()