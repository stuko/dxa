#!/usr/bin/env python

from crystalgen import *
from triangulation import *
import random

crystal_size = 8

dislocations = [((-5.51, 0.0), (1.0, 0.0)),
                (( 5.51, 0.0), (-1.0, 0.0)),
                ((0.0, -5.51), (0.0, 0.0)),
                ((0.0,  5.51), (0.0, 0.0))]

dt = compute_delaunay_tessellation(dislocations)

assign_jump_vectors(dt, [(0,1,(1.0,0.0))])

determine_displacement_vectors(dt)

atoms = []
atoms = fill_domain_with_atoms(dt, 0, crystal_size)
for index, cell in enumerate(dt.finite_cells):
    atoms += fill_cell_with_atoms(dt, cell, index+1)

for gap_region in dt.gap_regions:
    atoms += fill_gap_region_with_atoms(dt, gap_region)
    
print("Number of atoms: %i" % len(atoms))

plot_tessellation("output/tessellation.pdf", dt, atoms, size=crystal_size)
