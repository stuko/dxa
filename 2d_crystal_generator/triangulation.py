import numpy as np
import itertools

class Triangulation:

    class Vertex:
        
        def __init__(self, pos):
            if pos != None:
                self.pos = np.asarray(pos)
            else:
                self.pos = None
            self.burgers_vector = np.zeros(2)
            self.cell = None

        def __repr__(self):
            if self.is_infinite():
                return "Vertex(infinite)"
            else:
                return "Vertex(%g,%g)" % (self.pos[0], self.pos[1])

        def is_infinite(self):
            return self.pos is None
        
        def incident_facets(self):
            start_i = self.cell.vertices.index(self)
            assert(start_i >= 0)
            start_facet = (self.cell, (start_i+2)%3)
            assert(self == start_facet[0].vertices[(start_facet[1]+1)%3])
            facet = start_facet
            while True:
                yield facet
                neighbor_cell = facet[0].neighbors[facet[1]]
                i = neighbor_cell.vertices.index(self)
                assert(i >= 0)
                facet = (neighbor_cell, (i+2)%3)
                if facet == start_facet: 
                    break

    class Cell:
        
        def __init__(self, verts):
            self.vertices = verts
            self.jump_vectors = np.zeros((3,2))
            self.neighbors = [None, None, None]
            self.displacement_vector = None
            for v in verts:
                if v.cell is None: 
                    v.cell = self
        
        def __repr__(self):
            return "Cell(%s,%s,%s)" % self.vertices
        
        def is_infinite(self):
            for v in self.vertices:
                if v.is_infinite(): return True
            return False
        
        def bounding_box(self):
            assert(not self.is_infinite())
            xmin = min(self.vertices[0].pos[0], self.vertices[1].pos[0], self.vertices[2].pos[0])
            xmax = max(self.vertices[0].pos[0], self.vertices[1].pos[0], self.vertices[2].pos[0])
            ymin = min(self.vertices[0].pos[1], self.vertices[1].pos[1], self.vertices[2].pos[1])
            ymax = max(self.vertices[0].pos[1], self.vertices[1].pos[1], self.vertices[2].pos[1])
            return ((xmin,ymin),(xmax,ymax))
        
        def contains_point(self, p):
            denominator = ((self.vertices[1].pos[1] - self.vertices[2].pos[1])*(self.vertices[0].pos[0] - self.vertices[2].pos[0]) + (self.vertices[2].pos[0] - self.vertices[1].pos[0])*(self.vertices[0].pos[1] - self.vertices[2].pos[1]));
            a = ((self.vertices[1].pos[1] - self.vertices[2].pos[1])*(p[0] - self.vertices[2].pos[0]) + (self.vertices[2].pos[0] - self.vertices[1].pos[0])*(p[1] - self.vertices[2].pos[1])) / denominator
            b = ((self.vertices[2].pos[1] - self.vertices[0].pos[1])*(p[0] - self.vertices[2].pos[0]) + (self.vertices[0].pos[0] - self.vertices[2].pos[0])*(p[1] - self.vertices[2].pos[1])) / denominator
            c = 1.0 - a - b
            return 0.0 <= a and a <= 1.0 and 0.0 <= b and b <= 1.0 and 0.0 <= c and c <= 1.0
        
        def facets(self):
            yield (self, 0)
            yield (self, 1)
            yield (self, 2)
            
    class IncompatibilityRegion:

        def __init__(self, base_pos, tangent_vector, jump_vector):
            self.base_pos = base_pos
            self.tangent_vector = tangent_vector
            self.jump_vector = jump_vector
    
            shift_vector = 0.5 * jump_vector
            xmin = min(base_pos[0] + shift_vector[0], base_pos[0] - shift_vector[0], base_pos[0] + tangent_vector[0] + shift_vector[0], base_pos[0] + tangent_vector[0] - shift_vector[0])
            xmax = max(base_pos[0] + shift_vector[0], base_pos[0] - shift_vector[0], base_pos[0] + tangent_vector[0] + shift_vector[0], base_pos[0] + tangent_vector[0] - shift_vector[0])
            ymin = min(base_pos[1] + shift_vector[1], base_pos[1] - shift_vector[1], base_pos[1] + tangent_vector[1] + shift_vector[1], base_pos[1] + tangent_vector[1] - shift_vector[1])
            ymax = max(base_pos[1] + shift_vector[1], base_pos[1] - shift_vector[1], base_pos[1] + tangent_vector[1] + shift_vector[1], base_pos[1] + tangent_vector[1] - shift_vector[1])
            self.bb = ((xmin,ymin),(xmax,ymax))
            tm = np.array([tangent_vector,jump_vector])
            self.tminv = np.linalg.inv(tm.T)
            
        def contains_point(self, p):
            rv = np.dot(self.tminv, p - self.base_pos)
            #if np.linalg.norm(p - [0.5,0.5]) < 1e-1:
            #    print(str(self), p, rv)
            if rv[0] < 0.0 or rv[0] >= 1.0 or rv[1] < -0.5 or rv[1] >= 0.5:
                return False
            return True
        
        def shift_vector(self, p):
            if np.dot(self.tminv[1], p - self.base_pos) >= 0.0:
                return 0.5 * self.jump_vector
            else:
                return -0.5 * self.jump_vector
            
        def __str__(self):
            return "base=%s tangent=%s jump=%s" % (self.base_pos, self.tangent_vector, self.jump_vector)
    
    # Constructor that converts a matplotlib triangulation object.
    def __init__(self, dt):
    
        # Build list of vertices.
        self.vertices = []
        for x,y in zip(dt.x, dt.y):
            vertex = Triangulation.Vertex((x,y))
            self.vertices.append(vertex)
            
        # Create infinite vertex.
        self.infinite_vertex = Triangulation.Vertex(None)
        self.infinite_vertex.cell = None
        
        # Build list of cells.
        self.cells = []
        self.finite_cells = []
        for tri in dt.triangles:
            cell = Triangulation.Cell((self.vertices[tri[0]], self.vertices[tri[1]], self.vertices[tri[2]]))
            self.cells.append(cell)
            self.finite_cells.append(cell)
            
        # Generate infinite cells.
        for cell in self.cells:
            for n1 in range(3):
                found_neighbor = False
                for cell2 in self.cells:
                    for n2 in range(3):
                        if cell.vertices[n1] == cell2.vertices[(n2+1)%3] and cell.vertices[(n1+1)%3] == cell2.vertices[n2]:
                            found_neighbor = True
                            break
                if not found_neighbor:
                    inf_cell = Triangulation.Cell((cell.vertices[(n1+1)%3], cell.vertices[n1], self.infinite_vertex))
                    self.cells.append(inf_cell)
            
        # Connect adjacent cells.
        for cell in self.cells:
            for n1 in range(3):
                for cell2 in self.cells:
                    for n2 in range(3):
                        if cell.vertices[(n1+1)%3] == cell2.vertices[(n2+1)%3] and cell.vertices[(n1+2)%3] == cell2.vertices[n2]:
                            cell.neighbors[n1] = cell2
                            break
                assert(cell.neighbors[n1] != None)
                
        self.gap_regions = []
        self.overlap_regions = []
                
    def is_finite_facet(self, facet):
        return (not facet[0].vertices[(facet[1]+1)%3].is_infinite()) and (not facet[0].vertices[(facet[1]+2)%3].is_infinite())
    
    def mirror_facet(self, facet):
        mirror_cell = facet[0].neighbors[facet[1]]
        mirror_facet = (mirror_cell.vertices.index(facet[0].vertices[(facet[1]+1)%3])+1)%3
        return (mirror_cell, mirror_facet)
    
    def finite_facets(self, skip_half = False):
        for cell in self.cells:
            for i in range(3):
                facet = (cell, i)
                if not self.is_finite_facet(facet):
                    continue
                if skip_half:
                    v1 = self.facet_vertex(facet, 0)
                    v2 = self.facet_vertex(facet, 1)
                    #v1,v2 = v2,v1
                    if v1.pos[0] < v2.pos[0]-1e-6: continue
                    if v1.pos[0] < v2.pos[0]+1e-6:
                        if v1.pos[1] < v2.pos[1]: continue
                yield facet
                
    def facet_vertex(self, facet, j):
        assert(j >= 0 and j <= 1)
        return facet[0].vertices[(facet[1]+1+j)%3]
