#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.colors as colors
import numpy as np
import math
import collections
import matplotlib
import matplotlib.tri
from triangulation import *

def compute_delaunay_tessellation(dislocations):
	# Compute Delaunay tessellation
	xc = np.asarray([d[0][0] for d in dislocations])
	yc = np.asarray([d[0][1] for d in dislocations])
	mdt = matplotlib.tri.Triangulation(xc,yc)
	
	# Convert to our internal triangulation structure.
	dt = Triangulation(mdt)
	dt.matplottri = mdt
	
	# Assign dislocations to vertices.
	for disloc in dislocations:
		for v in dt.vertices:
			if np.all(v.pos == disloc[0]):
				v.burgers_vector = np.asarray(disloc[1])
				break

	return dt

def determine_jump_vectors2(dt):
	for vertex in dt.vertices:
		if np.all(vertex.burgers_vector == np.zeros(2)): continue
		for facet in vertex.incident_facets():
			assert(vertex == dt.facet_vertex(facet, 0))
			if dt.is_finite_facet(facet):
				vertex2 = dt.facet_vertex(facet, 1)
				if not np.all(vertex2.burgers_vector == np.zeros(2)): continue
				if not np.all(vertex2.pos == np.zeros(2)): continue
				j = vertex.burgers_vector
				facet[0].jump_vectors[facet[1],:] = j
				mirror_facet = dt.mirror_facet(facet)
				mirror_facet[0].jump_vectors[mirror_facet[1],:] = -j
				break
			
def assign_jump_vectors(dt, jump_vectors):
	for jv in jump_vectors:
		vertex1 = dt.vertices[jv[0]]
		vertex2 = dt.vertices[jv[1]]
		for facet in vertex1.incident_facets():
			if dt.facet_vertex(facet, 1) == vertex2:
				facet[0].jump_vectors[facet[1],:] = -np.asarray(jv[2])
				mirror_facet = dt.mirror_facet(facet)
				mirror_facet[0].jump_vectors[mirror_facet[1],:] = np.asarray(jv[2])
				break

def determine_jump_vectors(dt):
	# Gather facets
	facet_set = set()
	for vertex in dt.vertices:
		for facet in vertex.incident_facets():
			if dt.is_finite_facet(facet):
				mirror_facet = dt.mirror_facet(facet)
				if id(facet[0]) < id(mirror_facet[0]):
					facet_set.add(facet)
				else:
					facet_set.add(mirror_facet)
	print("Number of facets: %i" % len(facet_set))
	print("Number of vertices: %i" % len(dt.vertices))
	
	facet_list = list(facet_set)
	bvec = np.zeros(len(dt.vertices) * 2);
	amat = np.zeros((len(dt.vertices) * 2, len(facet_list) * 2))
	
	for vindex,vertex in enumerate(dt.vertices):		
		b = vertex.burgers_vector
		bvec[vindex * 2 + 0] = -b[0]
		bvec[vindex * 2 + 1] = -b[1]
		
		for facet in vertex.incident_facets():
			if dt.is_finite_facet(facet):
				mirror_facet = dt.mirror_facet(facet)
				if id(facet[0]) < id(mirror_facet[0]):
					facet_index = facet_list.index(facet)
					coefficient = 1.0
				else:
					facet_index = facet_list.index(mirror_facet)
					coefficient = -1.0
				assert(facet_index >= 0)
				amat[vindex * 2 + 0, facet_index * 2 + 0] = coefficient
				amat[vindex * 2 + 1, facet_index * 2 + 1] = coefficient
	print(bvec)
	print(amat)
	
	jvec = np.linalg.lstsq(amat, bvec)[0]
	print(jvec)
	
	#namat = np.dot(amat.T, amat)
	#print(namat)
	#nbvec = np.dot(amat.T, bvec)
	#jvec2 = np.linalg.solve(namat, nbvec)

	x = np.dot(amat, jvec)
	print("Relative error: %g" % (np.linalg.norm(x - bvec) / np.linalg.norm(bvec)))
	
	# Assign jump vectors to facets
	for facet_index,facet in enumerate(facet_list):
		j = np.array((jvec[facet_index*2+0],jvec[facet_index*2+1]))
		facet[0].jump_vectors[facet[1],:] = j
		mirror_facet = dt.mirror_facet(facet)
		mirror_facet[0].jump_vectors[mirror_facet[1],:] = -j

def determine_displacement_vectors(dt):
	# Find a start cell that is adjacent to an infinite cell.
	seed_facet = None
	for cell in dt.cells:
		if not cell.is_infinite(): continue
		for facet in cell.facets():
			if dt.is_finite_facet(facet):
				seed_facet = facet
				break
		if seed_facet: break
	print("Seed facet: %s" % str(seed_facet))
	seed_cell = dt.mirror_facet(seed_facet)[0]
	seed_cell.displacement_vector = seed_facet[0].jump_vectors[seed_facet[1]]
	# Recursive walk over all cells.
	to_be_visited = collections.deque([seed_cell])
	while len(to_be_visited) != 0:
		current_cell = to_be_visited.popleft()
		for n in range(3):
			 neighbor_cell = current_cell.neighbors[n]
			 if neighbor_cell.displacement_vector is not None: continue
			 neighbor_cell.displacement_vector = current_cell.displacement_vector + current_cell.jump_vectors[n]
			 to_be_visited.append(neighbor_cell)
			 
	# Identify overlay/gap regions
	for facet in dt.finite_facets(True):
		v1 = dt.facet_vertex(facet, 0)
		v2 = dt.facet_vertex(facet, 1)
		tangent = v2.pos - v1.pos
		jv = facet[0].jump_vectors[facet[1]]
		perp_dot = (jv[0] * tangent[1] - jv[1] * tangent[0]) / np.linalg.norm(tangent)
		if abs(perp_dot) > 1e-6:
			if perp_dot > 0.0:
				dt.gap_regions.append(Triangulation.IncompatibilityRegion(v1.pos, tangent, jv))				
			else:
				dt.overlap_regions.append(Triangulation.IncompatibilityRegion(v1.pos, tangent, jv))

def dislocation_displacement_field(r, origin, burgers, cut_dir = (1.0,0.0), nu = 0.0):
	r = r - origin
	b = np.linalg.norm(burgers)
	if b == 0.0:
		return np.zeros(2)
	
	bnorm = burgers / b
	bortho = np.array((bnorm[1],-bnorm[0]))
	x = np.dot(r, bnorm)
	y = np.dot(r, bortho)
	theta = math.atan2(y,x)
	
	perp_dot = cut_dir[0] * bnorm[1] - cut_dir[1] * bnorm[0]
	omega = math.atan2(perp_dot, -np.dot(bnorm, cut_dir))
	theta2 = math.fmod(theta + omega + 5*math.pi, 2.0*math.pi) - math.pi
	epsilon = 0.1 * b**2
	l = x*x+y*y
	ux = b / (2 * math.pi) * (theta2 + math.cos(theta) * math.sin(theta) / (2*(1 - nu)))
	uy = b / (2 * math.pi * 4 * (1 - nu)) * ((1-2*nu)*math.log(l+epsilon) + math.cos(2*theta))	
	return bnorm * ux - bortho * uy

def dipole_displacement_field(r, d1, d2, burgers, nu = 0.0):
	cut_dir = (d2 - d1)
	cut_dir /= np.linalg.norm(cut_dir)		
	return dislocation_displacement_field(r, d1, burgers, cut_dir, nu = nu) + dislocation_displacement_field(r, d2, -burgers, cut_dir, nu = nu)

def compute_displacement(dt, p):
	u = np.zeros((2,))
	for facet in dt.finite_facets(True):
		v1 = dt.facet_vertex(facet, 0)
		v2 = dt.facet_vertex(facet, 1)
		jump_vector = facet[0].jump_vectors[facet[1]]
		u += dipole_displacement_field(p, v1.pos, v2.pos, -jump_vector)
	return u

def fill_cell_with_atoms(dt, cell, atom_type = 1):
	atoms = []
	bb = cell.bounding_box()
	for y in range(int(math.floor(bb[0][1])-1), int(math.ceil(bb[1][1]))+1):
		for x in range(int(math.floor(bb[0][0]))-1, int(math.ceil(bb[1][0]))+1):
			p = np.array((x + 1e-8 + 0.5, y + 2e-8 + 0.5))
			if cell.contains_point(p):
				#overlap_point = False
				#for overlap_region in dt.overlap_regions:
				#	if overlap_region.contains_point(p):
				#		overlap_point = True
				#		print("Skipping atom in overlap region: %s" % p)
				#		break
				#if overlap_point: continue
				p += compute_displacement(dt, p)
				atoms.append((p,atom_type))
	return atoms

def fill_gap_region_with_atoms(dt, gap_region, atom_type = 3):
	atoms = []
	for y in range(int(math.floor(gap_region.bb[0][1])-1), int(math.ceil(gap_region.bb[1][1]))+1):
		for x in range(int(math.floor(gap_region.bb[0][0]))-1, int(math.ceil(gap_region.bb[1][0]))+1):
			p = np.array((x + 1e-8 + 0.5, y + 2e-8 + 0.5))
			if not gap_region.contains_point(p):
				continue
			
			#overlap_point = False
			#for overlap_region in dt.overlap_regions:
			#	if overlap_region.contains_point(p):
			#		overlap_point = True
			#		break
			#print('overlap_point=',overlap_point)
			#if overlap_point: continue
			
			print("Filling atom in gap region: %s" % p)
			p += compute_displacement(dt, p - gap_region.shift_vector(p))
			atoms.append((p,atom_type))
	return atoms
	

def fill_domain_with_atoms(dt, atom_type = 0, size = 10):
	atoms = []
	for y in range(-size, size):
		for x in range(-size, size):
			p = np.array((x + 1e-8 + 0.5, y + 2e-8 + 0.5))
			is_outside = True
			for cell in dt.finite_cells:
				if cell.contains_point(p):
					is_outside = False
					break
			for overlap_region in dt.overlap_regions:
				if overlap_region.contains_point(p):
					is_outside = False
					break
			if is_outside:
				p += compute_displacement(dt, p)
				atoms.append((p,atom_type))
	return atoms

def detect_overlapping_atoms(atoms):
	overlapping_atoms = []
	for a in atoms:
		for b in atoms:
			if a is b: continue
			if np.linalg.norm(a[0]-b[0]) < 1e-2:
				overlapping_atoms.append(a)
	return overlapping_atoms

def plot_tessellation(filename, dt, atoms = None, size = 10):
	fig = plt.figure()
	plt.gca().set_aspect('equal')
	plt.axis('off')
	plt.triplot(dt.matplottri)
	
	plt.gca().set_xlim((-size,size))
	plt.gca().set_ylim((-size,size))

	if atoms != None:
		atom_radius = 0.1
		atom_colors = ['b','g','r','c','m']
		for a in atoms:
			color = '0.3'
			if a[1] > 0: color = atom_colors[(a[1]-1) % len(atom_colors)]
			circle = plt.Circle(a[0],atom_radius,color=color, zorder=3)
			fig.gca().add_artist(circle)
		
		# Mark overlapping atoms
		for index,a1 in enumerate(atoms):
			for a2 in atoms[index+1:]:
				if np.allclose(a1[0], a2[0], atol = 0.4):
					print("Found overlap at ", a1[0])
					color = '0.3'
					if a[1] > 0: color = atom_colors[(a[1]-1) % len(atom_colors)]
					circle = plt.Circle(a1[0],atom_radius*2,color=color, zorder=4)
					fig.gca().add_artist(circle)
					break

	awidth = 0.1
		
	for facet in dt.finite_facets():
		j = facet[0].jump_vectors[facet[1]]
		if np.all(j == 0.0): continue
		v1 = dt.facet_vertex(facet, 0)
		v2 = dt.facet_vertex(facet, 1)
		center = 0.5 * (v1.pos + v2.pos)
		delta = v2.pos - v1.pos
		center[0] += delta[1] / np.linalg.norm(delta) * 0.5 - j[0] * 0.5
		center[1] -= delta[0] / np.linalg.norm(delta) * 0.5 + j[1] * 0.5
		plt.arrow(center[0], center[1], j[0], j[1], width=0.1, head_width=awidth*3.0,
					head_length=awidth*2.0, color=(0.2,0.2,0.2), length_includes_head=True, zorder=3)
		
#	for cell in dt.finite_cells:
#		u = cell.displacement_vector
#		if np.all(u == 0.0): continue
#		center = (cell.vertices[0].pos + cell.vertices[1].pos + cell.vertices[2].pos) / 3
#		plt.arrow(center[0], center[1], u[0], u[1], width=0.1, head_width=awidth*3.0,
#					head_length=awidth*2.0, color=(0.2,0.2,0.6), length_includes_head=True, zorder=3)

	for v in dt.vertices:
		if np.any(v.burgers_vector != 0.0):
			plt.arrow(v.pos[0], v.pos[1], v.burgers_vector[0], v.burgers_vector[1], width=0.1, head_width=awidth*3.0,
					head_length=awidth*2.0, color=(0.8,0.2,0.2), length_includes_head=True, zorder=3)
			
	# Plot gap regions and overlap regions.
	for region in dt.overlap_regions:
		verts = []
		verts.append(region.base_pos + region.jump_vector * 0.5)
		verts.append(region.base_pos - region.jump_vector * 0.5)
		verts.append(region.base_pos - region.jump_vector * 0.5 + region.tangent_vector)
		verts.append(region.base_pos + region.jump_vector * 0.5 + region.tangent_vector)
		poly = matplotlib.patches.Polygon(verts, lw=1, fill=True, ec='black', fc='green', alpha=0.5)
		plt.gca().add_patch(poly)		
	for region in dt.gap_regions:
		verts = []
		verts.append(region.base_pos + region.jump_vector * 0.5)
		verts.append(region.base_pos - region.jump_vector * 0.5)
		verts.append(region.base_pos - region.jump_vector * 0.5 + region.tangent_vector)
		verts.append(region.base_pos + region.jump_vector * 0.5 + region.tangent_vector)
		poly = matplotlib.patches.Polygon(verts, lw=1, fill=True, ec='black', fc='yellow', alpha=0.5)
		plt.gca().add_patch(poly)		
	
	#fig.savefig(filename, format='PDF', bbox_inches='tight')
	plt.show()
